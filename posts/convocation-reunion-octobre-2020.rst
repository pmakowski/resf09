.. title: Convocation réunion octobre 2020
.. slug: convocation-reunion-octobre-2020
.. date: 2020-09-24 16:03:08 UTC+02:00
.. tags: convocation
.. category: convocation
.. link: 
.. description: RESF09 convocation réunion octobre 2020
.. type: text

Bonjour,  quelques nouvelles…

Tout d’abord vous êtes invité-e-s, le premier lundi du mois, donc le 5 Octobre à 17h30 à notre réunion mensuelle à la Mairie de FOIX - Salle Jean Jaures - masqués bien entendu… Quant à s’en laver les mains… Bref, Yannick est notre « responsable  Covid ».

Nous pourrions aborder les points suivants (et d’autres si vous le souhaitez) :

1) - Les Mineurs isolés

Nous sommes toujours à la recherche d’hébergements « vacances et week-ends » pour deux jeunes scolarisés, Ibrahim au Lycée Jacquard de Lavelanet et Amdadul à l’EREA de Pamiers.

La dizaine d’Obligations de quitter le territoire (OQTF) prises à l’encontre de mineurs isolés ayant atteint leur majorité a pour première conséquence la perte de leur emploi. Proprement scandaleux ! De concert la Préfecture et la Direction du travail rappellent aux employeurs que des étrangers dépourvus de titre de séjour ne sont pas autorisés à travailler. Et ce après qu’ils aient suivi des formations au Lycée ou en apprentissage et qu’ils aient obtenu des diplômes professionnels pour la plupart. Comment qualifier une telle politique de casse et de mépris, un tel gaspillage de compétences ? Inutile d’écouter la démagogie des discours ministériels sur le sujet… il suffit de constater sur le terrain l’application qui en est faite… Alors au travail !

2) - Maman, les petits bateaux…

… Qui chavirent en Manche ou en Méditerranée et ceux qui ne trouvent pas de port pour accoster. Quant à Lesbos, quelle idée de faire des grillades quand on a rien à manger…

La France, l’Europe et les autres brillent par leur cécité, par leur volonté de rejet, par leur incapacité à penser « humain »… Il n’en fut pas toujours ainsi quand les industries cherchaient de la main d’œuvre. Certes, il faut dénoncer ces politiques scélérates, mais il faut désobéir et accueillir qu’ils le veuillent ou non… Alors au travail !

3) - Allons à l’école « pour tous »

Nous avons repris avec le Conseil départemental, la ségrégation dont sont victimes les enfants des familles étrangères en situation précaire à qui la CAF refuse le versement de l’allocation de rentrée scolaire. Pourtant, faut-il le rappeler : l’école est obligatoire. Les quelques Euros que nous avons distribués ne sauraient suppléer. Nous pouvons œuvrer à imposer l’Egalité et la Solidarité… Alors au travail !

4) - Dans les permanences hebdomadaires, le travail ne manque pas !

Nouveauté de la semaine : après l’avalanche d’OQTF de l’été, est venu le temps pour la Préfecture de « réparer ses oublis ». Depuis la Loi Collomb et la circulaire de Castaner, il était vivement conseillé  aux Préfets d’agrémenter leurs arrêtés d’expulsion d’un appendice dit « interdiction de retour » habituellement pour 12 mois et généralisé à l’espace Schengen… Donc nous avons enregistré, en une seule semaine, une dizaine de nouveaux arrêtés venant réparer cette négligence. Nous repartons au Tribunal Administratif… Dix procédures de plus… Alors au travail !

Ils nous fatiguent mais nous résistons… à Lundi donc… 

Amicalement Christian

