.. title: Compte rendu août 2022
.. slug: compte-rendu-aout-2022
.. date: 2022-08-17 10:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 Août 2022
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse

**Cas difficiles**

Plusieurs familles avec enfants font encore et toujours l'objet d'OQTF et/ou d'interdiction de retour en France ou encore d'assignation à résidence obligeant les intéressés -le plus souvent sans moyen de transport- à aller signer quotidiennement à la police ou la gendarmerie.

Parfois, certains (des hommes en règle générale) sont même envoyés en centre de rétention (CRA) pour leur faire quitter le pays, ce qui se traduit par un stress accru pour les femmes et enfants.
Les jeunes de moins de 18 ans ne sont pas mieux traités même quand ils suivent une scolarité normale voire sont déjà bien intégrés professionnellement.

Quant aux mères d'enfants français, elles éprouvent bien des difficultés pour obtenir un renouvellement de leur titre de séjour, lequel est de droit dans leur cas.
RESF, avec l'appui d'avocat(e)s spécialisé(e)s, entreprend systématiquement les démarches administratives et judiciaires appropriées. Régulièrement, nous demandons des jugements en référé afin d'obtenir des décisions plus rapides, les délais « normaux » pouvant  nécessiter plusieurs mois !

A noter aussi une grande difficulté pour obtenir des regroupements familiaux, hormis pour les réfugiés.
Autre problème pérenne : l'hébergement d'urgence pour les demandeurs d'asile. Les structures existantes n'ont quasiment pas de marge de manoeuvre et des familles sortent des structures CADA sans solution.

**Suivi personnalisé (y compris financier) des jeunes scolarisés**

Le plus souvent, cela passe par une démarche auprès du juge des enfants (JDE) qui semble éprouver des difficultés à leur donner des rendez-vous et qui vient de relancer les tests osseux alors que leur fiabilité est largement sujette à caution.

A l’heure actuelle, RESF en prend cinq à sa charge, ce qui représente plus de 7000 euros annuels, uniquement pour l’internat, les fournitures scolaires et les vêtements.

S’y ajoute la question de leur hébergement pendant les vacances scolaires et les week ends. Avis aux bonnes volontés !

**Rentrée scolaire**

L’aide aux familles ne bénéficiant pas du soutien de la CAF porte sur les fournitures et les vêtements. RESF envisage un concours de 50 euros par enfant comme l’an passé, ce qui avait représenté un budget de 4000 euros.


Le Secrétaire, Alain Lacoste
