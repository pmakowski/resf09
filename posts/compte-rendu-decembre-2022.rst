.. title: Compte rendu décembre 2022
.. slug: compte-rendu-decembre-2022
.. date: 2022-12-16 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 Décembre 2022
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse

Réunion mensuelle du 5 décembre 2022


**Questions diverses**

Fixation de l'Asssemblée Générale annuelle : elle aura lieu le lundi 9 janvier prochain à 17 h 30 à la mairie de Foix. Les adhérents sont invités en amont à réfléchir et à transmettre à Christian les questions et/ou observations relatives aux orientations et au fonctionnement de RESF 09 afin de pouvoir en débattre lors de l'AG.

Mouvement pour la paix : rassemblement le 14 décembre à Foix (18 h) 

Journée mondiale des migrations au Carla Bayle le 18 décembre (18 h également).

**Circulaire Darmanin aux Préfets**

Le ministre de l'intérieur vient de mettre la pression aux préfets leur enjoignant de s'impliquer personnellement dans l'application rigoureuse des OQTF. Parallèlement, il leur est demandé de réduire au minimum (15 jours voire moins) les délais de recours devant le tribunal administratif et de lier aux OQTF les assignations à résidence ainsi que l'interdiction de retour dans l’espace Schengen.

Une circulaire très politique donc et qui augure de la volonté du gouvernement avant la présentation du projet de loi au Parlement.

Autre signe inquiétant : la création d'un nouveau et unique fichier relatif aux migrants sous OQTF et aux délinquants de Droit commun !

**Mineurs isolés**

Confirmation de la chute du nombre d'arrivées en Ariège.

Pour autant, les jeunes résidant dans notre département pourraient constituer une cible préférentielle en matière de renvoi dans leur pays d'origine ; d'où la nécessité d'assurer de notre part un suivi régulier et personnalisé.

Quant à la juge des enfants, qui tarde toujours autant à les convoquer, la raison tiendrait à une « surcharge de travail ».

**Hébergement en période hivernale**

Il semblerait qu'on n'ait à déplorer pour l'instant aucune situation « dans la rue ». La solution des campings a permis de résoudre la plupart des situations.

Deux catégories de familles doivent être distinguées : celles étant officiellement en situation difficile sous la responsabilité de l'administration et celles qui sont hébergées par des bénévoles.

Mais tout cela reste très précaire...

Le Secrétaire, Alain Lacoste
