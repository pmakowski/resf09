.. title: Compte rendu novembre 2024
.. slug: compte-rendu-novembre-2024
.. date: 2024-11-05 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 Novembre 2024
.. type: text



**La circulaire Retailleau**

Circulaire Retailleau (sept pages !) envoyée aux Préfets, demande aux représentants de l'Etat une "complète mobilisation" afin d'obtenir des "résultats".

Pourquoi le service des étrangers de la préfecture étaient-ils fermés ces derniers temps ? Pour que les personnels (et le Préfet qui a été « invité » par le ministre de l’Intérieur) se forme à travailler de manière plus « efficace » ! Bruno Retailleau résumait sa conception de l’accueil dans une circulaire « musclée » pour rappeler « qu’on n’est plus là pour rigoler et qu’il convient dorénavant d’être encore un peu plus « méchant » avec l’étranger … comme le demande le bon peuple « d’extrême droite » et ses caciques.

Clairement et concrètement, il convient maintenant :

1. D’exploiter la notion de « suspicion de trouble à l’ordre public » : il ne sera plus nécessaire d’effectuer une action pouvant troubler l’ordre public… mais prouver qu’on n’a pas l’intention de troubler l’ordre public … ça sent vraiment le moisi …
2. Chaque Préfet devra s’assurer que les services vont « au bout » des procédures « quel qu’en soit le prix » (crédits « ouverts »). D’où, reprise des obligations de quitter le territoire français (OQTF) jusqu'à 3 ans + interdiction de retour sur le territoire français (IRTF), etc. …
3. Dans chaque département le Préfet présidera des commissions de suivi des étrangers en situations irrégulières. Les forces de police effectueront des visites domiciliaires chez tout individu qui ne présenterait pas de papiers d’identification de sa nationalité avec à la clé un placement possible et un maintien en centre de rétention jusqu'à 90 jours (210 jours demain si les rêves de Retailleau se réalisent…)

Pour faire court, il faut « être de plus en plus méchant » et le faire savoir, donc faire peur en créant un climat permanent d’incertitude et de précarité pour les personnes déjà fragilisées… 

Face à ce déferlement plusieurs associations regroupées dans l’ANAFÉ (dont la LDH est membre), réagissent et se mobilisent pour dénoncer ce nouveau « tout de vis » (L'ANAFE (Association nationale d'assistance aux frontières pour les étrangers) est une structure qui collabore avec plusieurs ONG et associations pour mener à bien sa mission. Parmi les organisations partenaires, on peut retrouver des groupes tels que la LDH, la Cimade, France Terre d'Asile, et le Gisti (Groupe d'information et de soutien des immigrés), entre autres. Ces ONG partagent des objectifs similaires en matière de défense des droits des migrants et de soutien aux personnes en situation de vulnérabilité.)

Pour aller plus loin :

- `CIMADE - Ariège/Foix – Exposition – La fabrique des sans papier - Festival Migrantscene <https://www.migrantscene.org/ariege-foix-exposition-la-fabrique-des-sans-papier/>`_
- `Bienvenue ⋅ GISTI <https://www.gisti.org/spip.php?article28)>`_
- `France terre d'asile - Au service des demandeurs d'asile <https://www.france-terre-asile.org/>`_
- `Agir avec l'Anafé <http://www.anafe.org/spip.php?article10#:~:text=e.s%2C%20contactez-nous%20par%20mail,dans%20l%27objet%20du%20mail.>`_
- `MRAP - Mouvement contre le racisme et pour l'amitié entre les peuples <https://mrap.fr/>`_

**Les mineurs isolés**

Christian M. écrivait dans l’invitation à la réunion : *« De nouveaux arrivants jetés par les « évaluations », le Procureur et le Conseil général, se voient balancés à la rue. On ne saurait mieux dire, actuellement plusieurs d’entre-eux victimes de la pratique « des nuits de carence » du 115, se retrouvent à la gare ou sous un hangar pour des nuits plus longues, plus froides et plus angoissantes… Pas si grave : les décideurs dorment au chaud et ne font même pas de cauchemars… »*

Présentée dans le détail, la situation est plus que préoccupante : Manque de capacité pour accompagner la scolarisation de tous ceux qui devraient l’être… 
  
- Manque de finances pour couvrir l’augmentation des frais prévisibles
- Manque de personnes ressources pour accueillir et héberger les jeunes les week-ends et périodes de vacances scolaires

Dans le détail :

1. notre responsabilité nous oblige à ne pas nous engager au-delà de nos possibilités financières (frais d’internat et de restauration) pour assurer à ces jeunes la garantie de les accompagner « jusqu’au bout » de leur période d’installation dans une vie professionnelle autonome. Ce ne sont pas les établissements scolaires qui bloquent… généralement ils « aiment bien » ces jeunes motivés et vivaces. Mais nous devons prévoir sur plusieurs années l’accompagnement pour ne pas devoir les abandonner demain faute de moyens…
2. nous ne pouvons pas accepter de laisser à la rue des jeunes scolarisés en semaine mais sans foyer dimanches, jours fériés et vacances scolaires.

Côté finances :

Constat : le nombre de contributeurs n’est pas en baisse, mais le constat est une réduction nette de chaque contribution.

Solutions ? 

- Augmenter le nombre de contributeurs. Si chaque adhérent au RESF sollicite 1 nouvel adhérent … c’est mieux que rien ! (Rappel : on devient adhérent à partir d’1 € de cotisation annuelle. Ce n’est pas grave si le nouvel adhérent donne plus !)
- Utiliser autour de vous le bulletin d’adhésion joint à chaque invitation aux réunions ou compte rendu. Ceux qui souhaiteraient faire un virement mensuel (donc moins lourd) peuvent (et doivent) utiliser le RIB joint à chaque envoi.

Rappel : pour un don de 100 €, 66 € peuvent vous être déduits de vos impôts pour l’année suivante de celle de votre don ce qui ramène à 34 € votre don.

- Le 8 décembre au cinéma Le Casino à Lavelanet, « La Sauce du Casino » organise une vente d’affiches de cinéma (format A3 et grand format) toute la journée au profit de RESF. Merci de faire circuler l’info autour de vous !
- Organiser des événements « rentables » … A la réunion de décembre nous « soupesons » la volonté (et la capacité) à créer un petit groupe de travail sur cette thématique…

Côté foyers d’accueil :

L’idéal est la stabilité. Mais l’idéal est un luxe… donc même si ce n’est que pour une période limitée faîtes nous connaître (06 70 94 08 48 – christian.morisse@nordnet.fr) votre envie d’essayer… essayer c’est parfois l’adopter… 


**Période hivernale : tout le monde à l’abri !e**

Christian M. écrivait dans l’invitation à la réunion : *« Sauf les déboutés de l’asile, sortis des CADA qui devraient aussi rejoindre le 115. Il leur est systématiquement répondu qu’aucune place n’est disponible – Faux – en fait la réponse polie cache les consignes préfectorales « pas question   de mettre à l’abri des familles (souvent avec enfants) qui ont vocation à rentrer au pays ». Traduire en termes humanitaires : « plus on les fera souffrir, plus ils seront enclins à déguerpir »*

Certes, les associations locales tentent d’accueillir cette misère mais elles sont vite saturées et ne sauraient s’inscrire dans un système de substitution aux responsabilités des institutionnels.
Tout est dit. Rien à ajouter…


**Questions diverses : Des événements de fin d’année…**

Les NATIONS UNIES organisent le 18 décembre la Journée internationale des migrants : pour considérer et célébrer les contributions de millions de migrants à travers le monde.

*« Les personnes en déplacement sont des moteurs puissants pour le développement à la fois dans leur pays d’origine et dans celui de destination en tant que travailleurs, étudiants, entrepreneurs, membres de la famille, artistes et bien plus encore. Les migrants maintiennent souvent des liens importants avec leur pays d’origine tout en s’intégrant dans leur nouvelle communauté, où ils apportent une myriade de connaissances, expériences et compétences. »*

Et nous ? 

Première difficulté : trouver une salle à cette date proche des fêtes de fin d’année. 

Deuxième difficulté : Trouver sa place et un sens dans un événement porté par un collectif aux contours « incertains ». Dit plus simplement : quoi faire et avec qui faire pour que ce ne soit pas 1) de « l’entre soi », 2) du « que des mots », 3) par contre, que ce soit un moment d’explication et d’approfondissement d’une réflexion partagée sur les thèmes qui nous animent ?

Le contact est engagé avec la CGT. D’un commun accord, il apparaît plus sage de reporter en janvier l’organisation de « quelque chose » ( ?) pour se donner le temps d’échanger et de dégager des lignes de forces partagées sur cette question qui, même si elle nous anime tous… ne nous accapare peut-être pas tous de la même manière. A travailler pour affiner le projet…


Le Secrétaire, Francis Lavergne 

