.. title: Convocation réunion janvier 2024
.. slug: convocation-reunion-janvier-2024
.. date: 2023-12-28 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion janvier 2024
.. type: text

Bonjour,

Nous avons le plaisir de vous rappeler que le premier lundi de chaque mois nous tenons une réunion de RESF

REUNION MENSUELLE du Réseau Education Sans Frontière

Assemblée Générale Statutaire

Le Lundi 8 janvier 2024  de 17H30 à 20H.

Salle F.Soulie – Mairie de FOIX

Tout d’abord, meilleurs vœux à toutes et à tous.

**ASSEMBLEE GENERALE STATUTAIRE**

nnexé à cette invitation : Le projet de texte soumis au vote de l’Assemblée Générale statutaire à discuter, à amender, à adopter en l’état ou modifié. Envoyez vos remarques.

Nous envisagerons les améliorations nécessaires au fonctionnement, notamment de l’équipe d’animation, des permanences et des besoins en formation sur « les Droits des Etrangers » existent-ils  encore !

Nous présenterons aussi à cette AG le rapport financier pour 2023 et déciderons des choix prioritaires à faire pour l’année 2024.

L’A.G. est un moment fort de la vie de l’association, plus nous serons nombreux, plus elle se portera bien.    

**Si nous avons du temps : Nouvelle Loi sur l’immigration…**

Retour sur les conditions lamentables de son existence, de son élaboration, des positionnements et des conditions de son « adoption »… Perdant, perdant… comme on dit en langage politique. L’heure n’est plus à pleurnicher mais à se retrousser les manches.

Perdant surtout pour celles et ceux qui vont en faire les frais, les étrangers ! Perdant surtout pour l’universalité des Droits et des Libertés. 

**Questions diverses :** qui sont les vôtres mais, avec un tel ordre du jour, le temps sera compté…   

Bien amicalement à toutes et tous.

Montoulieu le 28 décembre 2023

Christian

