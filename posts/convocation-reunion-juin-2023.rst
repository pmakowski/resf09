.. title: Convocation réunion juin 2023
.. slug: convocation-reunion-juin-2023
.. date: 2023-05-31 19:53:37 UTC+02:00
.. tags: convocation
.. category: convocation
.. link:
.. description: RESF09 convocation réunion juin 2023
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier lundi de chaque mois nous tenons une réunion  de RESF :

RÉUNION MENSUELLE

Du Réseau Éducation Sans Frontière

**Le lundi 5 juin 2023  de 17H30 à 20H.**

**Salle de réunion – Maison des associations - avenuede l'Ariège FOIX**


**1 – Les bonnes et les mauvaises nouvelles du mois :**  

Le lot quotidien d’OQTF/IRTF, de refus de séjour, de tentatives d’expulsion, de mise en rétention et d’assignations à résidence  alimente les permanences hebdomadaires et les recours juridiques avec plus ou moins de succès. Nous donnerons quelques exemples récents.

**2 – Penser rentrée scolaire et orientation :**

Nous avions abordé la question des Aides de rentrée scolaire versées par RESF. Soyons concrets, il faudra d’abord faire un point sur les finances, puis faire des choix concernant les aides et leurs modalités.

**3 – Des mineurs/majeurs pas si « isolés »**

C’est la période des examens et nos « protégés » planchent, qui pour un CAP, qui pour un Bac pro ou un Delf. C’est encourageant de constater que les efforts de toutes et tous, des intéressés en premier lieu, portent leurs fruits. Il en va de même pour les recherches d’emploi, les stages et les contrats d’apprentissage. Nous cherchons des solutions d’hébergement ou de logement autonome (recherche, financements et cautions…) 

**4 – Projet de loi « Immigration » :**

Du tout et n’importe quoi, mais du méchant surement ! Comme on pouvait s’y attendre la Droite et l’extrême Droite (quelle différence ?) s’engouffrent dans la surenchère. Les Républicains, n’hésitant pas à enfourcher les positions de Le Pen, essaient d’aboyer plus fort et Darmanin « en fin stratège » va négocier avec eux.

Que peut-il en sortir ? Que des violences discriminatoires, tant en matière de Droits, de santé ou d’expulsions. La France fût-elle quelque-fois le « Pays des Droits de l’Homme » qu’elle prétend ? Elle est sur le chemin de tous les xénophobes et de tous les démagogues. Nous avons l’obligation de leur faire barrage.

**5 - Au printemps on peut coucher sous les ponts… A Mayotte aussi :**

Nous ferons le point sur la « grande opération de nettoyage » lancée par le Sinistre de l’Intérieur qui avait un peu oublié qu’en France il y a des Lois et qu’on peut obliger ce bonhomme à les respecter. Nous reviendrons sur le déroulé des évènements, les « ratés » et les abus de Pouvoir.

**6 – et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 31 mai 2023

Christian
