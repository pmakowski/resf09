.. title: Convocation réunion juillet 2023
.. slug: convocation-reunion-juillet-2023
.. date: 2023-07-01 09:00:37 UTC+02:00
.. tags: convocation
.. category: convocation
.. link:
.. description: RESF09 convocation réunion juillet 2023
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier lundi de chaque mois nous tenons une réunion  de RESF :

RÉUNION MENSUELLE

Du Réseau Éducation Sans Frontière

**Le lundi 3 juillet 2023  de 17H30 à 20H.**

**Salle de réunion – Maison des associations - avenuede l'Ariège FOIX**

Il y sera question surtout des jeunes (MIE) et de la période estivale 

Question de prise en charge des scolarisations à la rentrée 2023/2024 

Question de quelques familles en difficulté

De demandes de formation (permanences) 

De calendrier du FLE 

et plus si.... 


Bien amicalement à toutes et tous,

Christian
