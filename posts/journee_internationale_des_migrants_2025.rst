.. title: Journée Internationale des migrants - 25 janvier 2025 à FOIX
.. slug: journee_internationale_des_migrants_2025
.. date: 2025-01-10 06:55:00 UTC+01:00
.. tags: actions
.. category: 
.. link: 
.. description:Conférence débat ateliers
.. type: text

Journée Internationale des migrants - 25 janvier 2025 à FOIX
-------------------------------------------------------------

Organisée par la Coordination ariégeoise Asile immigration

Le déroulement suivant est validé par la Coordination :

**9h00-10h00** : Installation d’une exposition « Apport des travailleurs immigrés à l’Ariège »
Installation des tables (tenant lieu de stands) des organisations participantes sur les deux salles
Jaurès et Soulié. Les documents présentés par les organisations sont à leur choix, en notant cependant
qu’il est souhaitable que ces documents correspondent à la thématique de la manifestation.

**10h30** : Rassemblement sous la Halle. Prises de parole : Maryline Lambert au nom de la Coordination,
Christian Morisse sur la situation politique concernant l’immigration

**11h30** : Retour à la salle Jaurès. Retour informel sur le rassemblement lui-même et présentation des
activités de l’après-midi avec les 4 ateliers.

**12h30** : Repas pour ceux qui le souhaitent dans les « estaminets – cuisine d’ailleurs» dont nous pourrions
diffuser la liste/invitation (et avec lesquels nous négocions préalablement des tarifs exceptionnels)

**14h00** : retour à la Mairie et début des ateliers

**14h00-15h00** : Deux ateliers (un dans chaque salle) : 1. le droit au travail 2. le droit à la santé

**15h15-16h15** : Deux ateliers (un dans chaque salle) : 3. les droits de la famille 4. le droit d’accès à la
langue, à la culture, à la scolarisation.

- 1 Le Droit au travail pour tous : situation actuelle – avantages et inconvénients – nos revendications et exigences – la formation professionnelle
- 2 Le Droit à la santé pour tous : problème de l’AME et projets du gouvernement. Comment organiser une mobilisation spécifique dans les semaines qui viennent
- 3 Les Droits de la Famille : unions, regroupements familiaux et le droit au logement
- 4 Le Droit d’accès à la langue, à la Culture, à la scolarisation (moyens et accompagnement)

Dans ces 4 ateliers, sera mise en évidence si possible la question transversale du Droit à faire valoir ses
droits et du Droit de participer à la vie citoyenne.

Les 4 ateliers retenus seront animés par une petite équipe de préparation (2 ou 3). Celle-ci sera chargée de
faire une brève introduction à la thématique, n’excédant pas 4 mn, puis d’animer le débat. Il s’agit dans ces
ateliers de mettre en avant les expériences de l’immigration des participants et de faire si possible émerger
des pratiques qui font des participants des acteurs (“moi aussi, je peux faire quelque chose”).

**16h30** : Retour en plénière (Jean Jaurès) avec un résumé rapide des ateliers. Penser à prendre des notes
pendant l’atelier, un compte-rendu écrit sera diffusé après le 25.

**19h00** : Nous devons avoir tout bouclé et rangé (fermeture des salles)
