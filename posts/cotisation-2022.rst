.. title: Cotisation 2022
.. slug: cotisation-2022
.. date: 2022-01-20 08:00:00 UTC+01:00
.. tags: 
.. category: cotisations 
.. link: 
.. description: Cotisation 2022
.. type: text

Aux camarades, aux amies, aux amis,

Les années passent et se ressemblent… de plus en plus dures. Donc pas de trêve pour les militants des Droits et de la Solidarité. Pas de mise en sommeil de RESF.

Aussi comme chaque année nous remettons à jour la liste des sympathisants/adhérents. 

Nous souhaitons donc que vous nous confirmiez votre adhésion à RESF 09 pour 2022. Aucun montant de cotisation n’a été fixé et chacune chacun participe selon son désir et ses moyens. Nous vous adresserons un reçu fiscal vous permettant un allègement d’impôts. 

La totalité des dépenses consiste à venir en aide aux enfants, aux mineurs isolés, aux familles en grande précarité, de plus en plus nombreux, la plupart étrangers. Nous faisons chaque année un rapport complet des activités et des finances lors de l’Assemblée Générale statutaire.

En 2021 - année bien étrange - RESF 09 a redistribué une bonne part des sommes récoltées mais l’exercice comptable est excédentaire pour la seconde fois. Ce qui nous permettra de combler les déficits passés et d’être un plus audacieux dans les engagements à venir.

Chèques à l’ordre du Réseau Education  Sans Frontière 09

RESF 09 se réunit pour faire le point chaque premier lundi du mois à 17h30 - mairie de FOIX
Une permanence accueille sur RDV le vendredi matin - MDA de Pamiers
 
Amicalement la Présidente   Yannick Garcia Poirier             Janvier 2022 



-------------------------------------------------------------------------------------------------------------------------------

Coupon à retourner : RESF 09 - 5 rue du Carrié - Seignaux - 09000 MONTOULIEU
Ou par courrier électronique : christian.morisse@nordnet.fr

Nom et Prénom…………………………………………………..........................................................................

Adresse Postale …………………………………………………........................................................................

Ville ………………………………………………….............. Code postal ………………………………………

Adresse électronique : …………………………………………………..........  Nouvelle     OUI      NON

Téléphone : ……………………………………………..  Portable : ……………………………………………….

Renouvellement : ………………………………….. Nouvelle adhésion : ………………………………………..

Svp : écrivez très lisiblement - merci

