.. title: Compte rendu juillet 2021
.. slug: compte-rendu-juillet-2021
.. date: 2021-07-18 15:54:29 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link: 
.. description: RESF09 Juillet 2021
.. type: text

Présidence : Yannick Garcia
Animation : Christian Morisse

On constate une dureté accrue de la Préfecture y compris vis a vis de familles établies en Ari¢ge
depuis de nombreuses années. Cela entraine bien entendu une profonde déstabilisation des
intéressés, avec parfois -pour les jeunes adultes en particulier- perte du logement et du travail donc
des moyens de (sur)vie.

Les services préfectoraux en arrivent méme a appeler les employeurs pour leur mettre la pression.
On s'interroge aussi (?!) sur la méthode qui consiste a cibler un seul membre d'une méme famille...
Autre embrouille juridique : la saisine normalement suspensive du Tribunal Administratif en cas
d'OQTF, ce que réfute la Préfecture.

En fait, en Ariège comme ailleurs, les Préfectures ont des consignes ministérielles trés strictes qu'il
leur revient d'appliquer fermement.

Face a cette pression permanente, nous faisons encore et toujours le maximum dans la limite de nos
moyens, en particulier pour les jeunes -une quinzaine sous OQTF- afin de leur permettre de
poursuivre leur scolarité et faciliter leur insertion professionnelle.

Au total, l'été va rester « animé » et la rentrée s'annonce lourde.

Aussi est-il décidé d'anticiper certaines démarches administratives : une réunion de travail
relative 4 la constitution de dossiers d'AES (Admission Exceptionnelle au Séjour) en faveur des
jeunes de 18 a 19 ans se tiendra le 19 juillet à 15 h. a la mairie de Foix.

Familles parrainantes

Le Conseil Départemental nous avait signalé son intérét pour accroitre le nombre de ces familles
dont les conditions d'accueil sont moins contraignantes que les familles agréées. A l'heure actuelle,
cinq sont en place. Si cette option vous intéresse, prenez contact avec Ch. Morisse.

Conséquences sur la politique nationale vis à vis des migrants suite aux récentes élections
On peut craindre un « tri» encore plus sélectif des intéressés en fonction de leurs capacités
d'intégration.

De notre côté, il est proposé de prendre contact par écrit avec les syndicats professionnels de la
Chambre de métiers qui sont réguliérement au contact des jeunes migrants lors de leur
apprentissage et qui leur apportent une pleine satisfaction dans la plupart des cas. Ce qui atteste de
la grande détermination de ces jeunes.

Hébergement d'été
Une liste de jeunes demandeurs va étre établie comme les années précédentes avant de contacter les
personnes ou familles prêtes à en accueillir, sous forme de résidence tournante.

Alain LACOSTE
Secrétaire

