.. title: Rassemblement contre la loi sécurité globale
.. slug: rassemblement-contre-la-loi-securite-globale
.. date: 2020-11-27 08:52:29 UTC+02:00
.. tags: rassemblement
.. category: rassemblement
.. link: 
.. description: RESF09 Loi sécurité globale
.. type: text


La LOI «sécurité globale » adoptée en première lecture par l’Assemblée
aurait du plomb dans l’aile, raison de plus pour l’achever…

Les violences policières : ça n’existe pas, dixit Darmanin. Les images
trahissent les flics qui cognent comme ils ont déjà cogné… trop souvent en toute impunité…


La déclaration du projet de rassemblement envisagé à FOIX ce samedi, a été
déposée par LFI dans les formes et n’a pas été interdit. Le rassemblement a donc lieu

**ce Samedi 28 novembre 2020
à partir de 10 heures et jusqu’à 12 h.
sous la Halle à FOIX**

Sous conditions de respect des mesures sanitaires: masque obligatoire et déclaration de circulation en poche…

Suite aux retours/consultation de nos adhérents :

La Ligue des Droits de l’Homme – LDH Ariège –
et 
Le Réseau Education Sans Frontière – RESF 09 –

soutiennent ce rassemblement et appellent à y participer.

Dans le plus grand calme possible !

Christian Morisse


