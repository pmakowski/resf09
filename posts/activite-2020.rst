.. title: Activité 2020
.. slug: activite-2020
.. date: 2021-01-22 08:33:49 UTC+01:00
.. tags: 
.. category: permanences 
.. link: 
.. description: Activités 2020
.. type: text

L’année 2020 avec quelques repères statistiques – Assemblée Générale statutaire

Une année bien particulière avec deux longues périodes de confinement.

Nous avons maintenu les permanences et avons fonctionné par mail,
merci aux travailleurs sociaux qui ont servi de relai indispensable

-	Nous avons tenu 9 réunions mensuelles du Réseau à Foix 
-	Nous avons tenu 1 rencontre de formation (associations amies)
-	Nous avons tenu 2 conférences de presse
-	Nous avons tenu 32 permanences à Pamiers et 4 à Foix (une moyenne de 9 RDV par séance)
-	Au cours desquelles nous avons reçu 321 « dossiers » (RDV individuels ou familles), dont 68 nouveaux pour 2020
-	Soit environ 200 dossiers « actifs » (certains sont venus deux ou trois fois).
-	Nous avons contribué à 13 dossiers de 1ère demande d’asile à l’OFPRA (et traductions)
-	Nous avons contribué à 28  dossiers de recours auprès de la CNDA 
-	Nous avons formulé 9 dossiers de réexamen 
-	Nous avons complété 10 dossiers de naturalisation et visas administratifs
-	Nous avons contribué à établir 6 dossiers médicaux 
-	Nous avons introduit 142 requêtes contre les OQTF au T.A. Toulouse
-	Et 29 appels en Cour d’Appel administrative à Bordeaux
-	Nous avons complété 230 demandes d’Aide Juridictionnelle (4 pages)
-	Nous avons introduit 23 requêtes Transferts et assignations à résidence (à 48 h. de délai)
-	Ces requêtes exigent la constitution de  dossiers juridiques en amont : environ 120 dossiers
-	Nous avons établi 5 dossiers de regroupement familial (mariages, divorces, rapprochements)
-	Nous avons soutenu, hébergé et financé la scolarisation de 10 Mineurs Isolés 
-	Nous en avons accompagné 4 au JE et 2 en Cour d’Appel des mineurs
-	Nous avons aidé 2 demandes de retour volontaire au Pays
-	Nous avons formulé 45 nouvelles demandes  de régularisations discrétionnaires
-	Nous avons rédigé plusieurs courriers. La Préfecture n’a que peu reçu au guichet
-	Nous avons été conviés et présents à 1 seule réunion de travail en Préfecture
-	Nous avons rédigé de nombreux courriers à l’extérieur (services sociaux, scolarité, justice)
-	Nous avons fait procédé à plusieurs traductions officielles (souvent plusieurs documents)
-	Nous avons assuré une centaine de traductions orales et écrites (permanences et dossiers)
-	Les cours de FLE hebdomadaires à Pamiers et à Foix ont partiellement survécu au Covid 
-	Et enfin enregistré sur le cahier une moyenne de 10 à 12 communications téléphoniques journalières (il n’y a ni fêtes, ni dimanches…) et de très nombreux documents par mail, nous permettant ainsi de faire face malgré les confinements.
-	Et des kilomètres et des kilomètres… Merci à toutes celles et ceux qui roulent…

Il peut y avoir quelques oublis. Les relevés sont faits à partir des dossiers en cours et des cahiers.

Même à domicile…
Certaines semaines sont bien chargées…

