.. title: Permanences LDH RESF 09
.. slug: permanences-ldh-resf09-09122020
.. date: 2020-12-09 08:33:49 UTC+01:00
.. tags: 
.. category: permanences 
.. link: 
.. description: point famille « M » Géorgienne
.. type: text

Rapidement le point famille « M » Géorgienne qui se compose de la Mère, le Père, une fille Majeure et deux filles (16 et 17 ans scolarisées) menacée d’expulsion.

Sous le coup d’une OQTF (recours perdu au TA suivi d’un appel à La Cour Administrative d’appel de Bordeaux - recours non suspensif)

Problème de courriers recommandés non distribués suite à la sortie de l'hébergement d’urgence (hivernal/covid) et du changement d’hébergement pour la famille.

Puis deuxième vague d’arrêtés « d’assignation à résidence » recours sous 48 heures - hors délai pour lui qui n’a pas alerté assez tôt - mais décalés pour elles, contestés et annulés partiellement par le Tribunal administratif (elles sont dispensées du pointage à la Police)

Puis troisième vague d’arrêtés : les IRTF (Interdiction sur le territoire Français (en fait interdiction sur l’espace UE) contestés au Tribunal Administratif. L’audience pour les 3 est fixée le 29 Décembre au TA.

Enfin : 3 feuilles de route qui ordonneraient les départs : Le 12 décembre pour lui et le 29 décembre pour elles et les deux filles mineures, c’est-à-dire que, au mépris du Droit, aucun ne pourrait être présent à l’audience du 29 au TA... Déni de Justice.

L'avocate a adressé hier matin un courrier recommandé doublé de mail à la Préfecture demandant :

- Que l’accès au Droit soit respecté notamment pour l’audience du 29/12
- demande l’annulation des départs pour tout le monde,
- le réexamen de la situation après que nous ayons connaissances des décisions d Justice auxquelles la Préfecture ne saurait se soustraire,
- un réexamen de la situation de l’ensemble de la famille à l’issue des décisions de Justice (manifestement courant Janvier)

Pour l'instant : Pas de réponse de la Préfecture

À défaut de réponse, nous conviendrons en fin de semaine des stratégies à développer avec l’avocate et la famille.

