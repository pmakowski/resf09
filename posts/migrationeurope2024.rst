.. title: Migration: une chance pour l’Europe
.. slug: migrationeurope2024
.. date: 2024-03-29 06:55:00 UTC+01:00
.. tags: actions
.. category: 
.. link: 
.. description:Conférence débat
.. type: text

Conférence débat «Migration: une chance pour l’Europe»
------------------------------------------------------

Présentation de Mme Ekrame Boubtane

**Vendredi 19 avril 2024**
**20h30 salle Jean Jaurès Mairie de Foix**

À l’initiative de la coordination ariégeoise contre la loi Asile immigration du 26 janvier 2024.

À l’heure où l’immigration est l’objet de toutes les invectives, de toutes les polémiques politiciennes, voire de toutes les haines, nous avons souhaité reposer le débat à partir des données sérieuses scientifiques et chiffrées.

À l’heure où l’Europe doit faire des choix politiques, économiques et sociaux, les citoyennes et citoyens que nous sommes ont d’abord le besoin d’accéder à des données objectives et plurielles.

Nous pensons en effet, qu’ avant d’avoir une «opinion à proclamer» il valait mieux savoir de quoi nous parlons.

Nous espérons que cette soirée y contribuera.

Madame Ekrame Boubtane est spécialiste des questions liées aux migrations internationales.
Docteure en économie de l’Université Paris I Panthéon Sorbonne, habilitée à diriger la recherche, elle est actuellement chercheuse associée à l’École d’économie de Paris. Elle était auparavant Maîtresse de conférence à l’Université Clermont Auvergne, experte auprès de l’ Agence nationale de la recherche et du Haut Conseil de l’évaluation de la recherche et de l’enseignement supérieur, membre du Conseil National des Universités.

La coordination ariégeoise «contre la loi asile immigration du 26 janvier 2024» réunit les organisations syndicales, politiques et associatives signataires du communiqué commun du 13 février 2024 : CGT – FSU – UNSA – EELV – Génération.s – LFI – NPA – PCF – POI – PS – Association de soutien à la NUPES – ATTAC – LDH 09 – Libre pensée – Mouvement de la paix – RESF 09

`Présentation en pdf <http://ldh-midi-pyrenees.org/wp-content/2024/03/migration_europe.pdf>`_

