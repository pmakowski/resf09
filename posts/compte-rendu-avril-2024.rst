.. title: Compte rendu avril 2024
.. slug: compte-rendu-avril-2024
.. date: 2024-04-09 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 avril 2024
.. type: text


**Mineurs isolés**

La pression est actuellement plus forte (comme ça l’avait été il y a 4/5 ans). + de 14 aux Pupilles à Reins (FOIX) + liste d’attente, 21 (ou plus) à l’hôtel Rocade à Pamiers, + au foyer de Loumet, etc…

Les structure « classiques » ne peuvent plus répondre… l’organisation des évaluations est « débordée » … les structures et services font avec les moyens dont ils disposent (et souvent plus …). Ce qui donne motif à certain Collectif de « stigmatiser les travailleurs sociaux et de brailler » … nous, conscients des difficultés quotidiennes et récurrentes dans lesquelles se débattent les personnels nous préférons agir à leurs côtés et le plus fréquemment, au-delà.

Exemple des dossiers de « mineur isolé » : la juge des enfants est en congé de maternité. Elle n’est pas remplacée… Ce sont les 5 ½ « autres juges » qui doivent se répartir ses dossiers. Bien évidemment ils traiteront en priorité des dossiers « Urgents + » (enfants violentés, etc., etc. …). Normal !

Pour information :

* 5 Jeunes devraient pouvoir rejoindre le dispositif « mission de lutte contre le décrochage scolaire (MLDS) et rejoindre une scolarité «pleine et ordinaire » à la rentrée prochaine. Nous les accompagnerons.

Côté finances :

* Avec satisfaction nous constatons que la mobilisation des « fonds sociaux » lycéen / collégien et Région ont été plutôt bien affectés, allégeant d’autant les frais que nous avions prévus de mobiliser à ce sujet.


**Coordination contre la loi « asile-immigration »**

Le cycle de formation proposé par la LDH vient de réaliser sa 4ème séance. Il y a du monde (+/- 25 personnes par séance). Si peu de membres des associations, syndicats et partis politiques sont présents (il y en peu, mais il y en a !), satisfaction de compter parmi les présents des salariées de quelques services sociaux, fidèles aux séances.


Rappel : Venez et faites venir vos amis et connaissances à la soirée du vendredi 19 avril. (20h30 Mairie de Foix) Mme Ekrame BOUBTANE se déplace spécialement pour nous exposer « Pourquoi : l’immigration est une chance pour l’Europe » Faisons de cette soirée un moment fort !

**Français Langue Etrangère**

Il est rappelé que les bénévoles du Réseau Éducation Sans Frontière s’investissent en priorité pour répondre aux besoins de jeunes et adultes « en difficultés administratives ».  
Il est rappelé que notre raison d’agir, est de répondre en priorité aux besoins de personnes qui n’accèdent pas ou mal aux cours organisés par les structures répondant aux commandes des institutions publiques. Ces structures reçoivent, elles, pour les prestations qu’elles mettent en œuvre, une rémunération. Il apparait dans l’échange de ce soir que nous ne connaissons qu’imparfaitement qui fait quoi et où. D’autant que les choses évoluent d’une année sur l’autre. Par contre la dernière Loi « Asile-Immigration » va créer de nouveaux besoins avec le nouveau Contrat républicain…   
Pour approfondir la question Yannick et Francis se proposent d’aller à la pêche aux informations pour proposer prochainement une « carte scolaire des formations FLE » du département. A suivre …

**Questions diverses**

Il nous est rapporté qu’un incident à caractère raciste vient de secouer le Collège de Seix. Les enseignants se sont immédiatement saisis de la question. Suite à l’échange engagé ce soir, il est dit clairement que si l’établissement en exprime le besoin, la LDH/RESF peut examiner avec les responsables du Collège, les formes les mieux adaptées pour répondre (avec nous ou sans nous) à cette question.

Un conseil de lecture :

Magasine MANIÈRE DE VOIR n °194 : N° consacré au thème de l’IMMIGRATION  
Parution d’Avril Mai 2024 - 8,50 €.  
Pour commander : ` Manière de voir - Le Monde diplomatique <https://www.monde-diplomatique.fr/mav/>`_


Le Secrétaire, Francis Lavergne

