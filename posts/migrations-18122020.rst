.. title: 18 décembre Journée internationale pour les droits des migrant-es
.. slug: journée-internationale-pour-les-droits-des-migrant-es
.. date: 2020-12-14 08:00:00 UTC+01:00
.. tags: actions
.. category: 
.. link: 
.. description: Journée internationale pour les droits des migrant-es
.. type: text


Nous joignons à ce courrier `le texte national commun <https://mrap.fr/manifestons-le-18-decembre-pour-les-droits-des-migrant-es-791.html>`_ que nous faisons nôtre pour cette journée. Depuis son édition d’autres signatures l’ont rejoint.

Sans plus d’illusions sur ces « journées mondiales » qui se multiplient sur tous les thèmes... médiatisations d’un jour... Une année de bonne conscience assurée.

Nous préfèrerons faire appel au travail quotidien de militant-es qui inlassablement, souvent dans l’ombre et discrètement interviennent, accueillent, aident, accompagnent ces femmes, ces enfants, ces hommes jetés, quelles qu’en soient les raisons, sur les routes, sur les mers de l’exil.



**Ce vendredi 18 décembre :**

**D'abord nous reprendrons les permanences hebdomadaires** interrompues par la « crise sanitaire »... Même si nous avons continué de traiter l’essentiel des demandes, un certain nombre de situations exigent des rencontres effectives.

L'administration préfectorale n’a pas non plus suspendu ses mauvais coups. Covid et confinement ou pas, les Obligations de Quitter le Territoire (OQTF), les Interdictions de Retour sur le Territoire (IRTF) et les enfermements en rétention (CRA) n’ont cessé de tomber.

Nous avons systématiquement organisé, même à distance, la défense des victimes de cette sale politique. Parfois avec succès : tel ce Russe arrêté un soir à Pamiers, envoyé le lendemain en rétention à Marseille (CRA) et libéré par le Juge des Libertés deux jours plus tard... Retour à Pamiers.

**Ce vendredi 18 décembre :**

**Puis nous prendrons le temps de témoigner...** En 20 ans d’activité militante, nous avons accumulé dans les cartons pas moins de 1200 dossiers de familles passées ou installées en Ariège... soit quelques milliers de femmes, d’enfants et d’hommes dont nous avons croisé les destinées.

Nous vous proposons d’exhumer ses histoires belles ou tristes, réussies ou ratées, de revenir à la rencontre des intéressé-es, d’écrire avec elles, avec eux, de publier ces témoignages.

Nous pourrions ainsi mettre en perspective les enjeux, les embuches, l’efficacité et les ratés.

Nous pourrions ainsi, à partir de ce vécu bien réel, montrer que tout n’est pas perdu malgré la violence institutionnelle, que l’espoir de réussir ces parcours difficiles est bien là... L’espoir.

**Alors à vos plumes :**

Sous forme d’interview, de récits, de photographies, de rencontres avec une ou des personnes concernées... témoignez... Communiquez-nous le produit de votre implication quotidienne.

Passant de la journée à l’année, nous envisageons la publication de ces travaux sous forme de recueil pour le 18 décembre... 2021 !

