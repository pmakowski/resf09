.. title: Le quotidien des femmes dans les migrations
.. slug: hutmars2024
.. date: 2024-02-20 06:55:00 UTC+01:00
.. tags: actions
.. category: 
.. link: 
.. description:Conférence débat
.. type: text

Le quotidien des femmes dans les migrations
-------------------------------------------

La LDH a choisi de mettre tout particulièrement en lumière le sort quotidien réservé aux femmes dans les migrations : mauvais traitements familiaux et sociétaux, violences physiques, violences sexuelles, violences religieuses, violences intraconjugales, bref toutes les violences que ces femmes
subissent avant de fuir, pendant la fuite et à l’arrivée dans des parcours souvent effrayants.

Parce que nous les avons rencontrées, parce que nous les avons accueillies lors des permanences, parce que nous les avons accompagnées,

Yannick GARCIA POIRIER présentera le 8 Mars un état des lieux dont nous pourrons débattre et rechercher les meilleurs moyens d’être solidaires.


**Conférence – débat**

Journée internationale des Droits des Femmes

La LDH de l’Ariège vous invite

Le vendredi 8 mars 2024 à 20h30

Salle Jean Jaurès – Mairie de Foix

Présentation : Yannick Garcia Poirier

`CONFERENCE-DU-8-MARS.pdf <http://ldh-midi-pyrenees.org/wp-content/2024/02/CONFERENCE-DU-8-MARS-definitive.pdf>`_

