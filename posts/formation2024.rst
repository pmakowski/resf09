.. title: Formations 2024
.. slug: formations-2024
.. date: 2024-01-22 08:55:00 UTC+01:00
.. tags: formation
.. category: 
.. link: 
.. description:Formations 2024 
.. type: text

Planning des formations proposées par la LDH sur le thème:

"Les étrangers ont des droits" oui, encore, mais lesquels ? comment les imposer ?

+-----------------------------------------------+----------------+------------------+
| Thème du jour                                 | date           | lieu             |
+===============================================+================+==================+
| 1 **Asile** : les structures,                 | 14 février 2024| Maison           |
| les procédures, différents status             |                | des associations |
|                                               | de 18h à 20h   | salle des fêtes  |
| SPADA, OFII, Préfecture, OFPRA, CNDA, Dublin  |                | Foix             |
+-----------------------------------------------+----------------+------------------+
| 2 **la famille** : regroupement familial      | 6 mars 2024    | Maison           |
| parents de, enfants de, conjoints de          |                | des associations |
|                                               | de 18h à 20h   | salle des fêtes  |
| Descendants et ascendents                     |                | Foix             |
+-----------------------------------------------+----------------+------------------+
| 3 **le séjour** : différents titres de séjour | 20 mars 2024   | Maison           |
| de plein droit, les AES , les renouvellements |                | des associations |
| leur durée, les naturalisations               | de 18h à 20h   | salle des fêtes  |
|                                               |                | Foix             |
+-----------------------------------------------+----------------+------------------+
| 4 **l'éloignement** : OQTF, IRTF              | 3 avril 2024   | Maison           |
| assignations à résidence, centre de rétention |                | des associations |
| les dossiers et l'aide jurictionnelle         | de 18h à 20h   | 1er étage        |
| les avocats                                   |                | Foix             |
+-----------------------------------------------+----------------+------------------+
| 5 **conduire un entretien** : qui sommes nous | 17 avril 2024  | Maison           |
| accompagnement admistratif et juridique       |                | des associations |
| les premiers renseignements, les récits,      | de 18h à 20h   | 1er étage        |
| les dossiers individuels, les archives        |                | Foix             |
+-----------------------------------------------+----------------+------------------+
| 6 **législations en Europe** :                | 22 mai 2024    | Maison           |
| approche comparée des différents textes et    |                | des associations |
| pratiques                                     | de 18h à 20h   | 1er étage        |
| Frontext et les fichiers Schengen             |                | Foix             |
+-----------------------------------------------+----------------+------------------+

Pour assurer un travail efficace, nous limiterons les inscriptions à 25 personnes par séance.

Si nécessaire, nous dédoublerons les séances de formation.

Nous vous demanons de remplir la `fiche d'inscription <https://resf.ariege.eu.org/inscription.pdf>`_ à nous retourner à morisse.christian AT yahoo.fr

Manifestez votre demande au moins 15 jours à l'avance (que nous puission confirmer la place). À la carte,
donc aucune obligation de tout suivre ...


