.. title: La vie reprend peu à peu un cours normal
.. slug: la-vie-reprend-peu-a-peu-un-cours-normal
.. date: 2020-06-27 14:35:10 UTC+02:00
.. tags: 
.. category: permanences
.. link: 
.. description: Fin de la trêve pour les hébergements
.. type: text

Bonjour,

La vie reprend peu à peu un cours normal... À qui le dites-vous ? le cours normal des arrêtés de reconduite à la frontière, les mises à la porte des hébergements d'urgence, le refus de titres de séjours ou de renouvellement. Faisons le point sur ces temps difficiles.

**Fin de la trêve pour les hébergements :**

Les places exceptionnelles d'hébergement dites de "période hivernale" ont été maintenues jusqu'à ce jour pour éviter l'errance en période de confinement. La plupart des familles concernées étaients logées sur les campings et les hébergements d'urgence, sous gestion du 115. Au plus tard, le 10 juillet, il faudra avoir vidé les lieux, vagues de sorties qui chaque année ont lieu fin mars. Certes, elles ont eu trois mois de répit mais ni le relogement, ni la crise Covid 19 ne sont résolus. Un vent de panique s'est installé parmi les intéressés, on le comprend. Plus dure sera la chute.

**Nous avons repris les permanences :**

Sous conditions strictes notamment de prise de RDV et de respect des mesures de protection, nous avons repris les permanences hebdomadaires d'abord sur Foix puis sur Pamiers. Pendant toute cette triste période nous avions assuré à  distance les suivis juridiques rendus rares du fait de la mise en sourdine des services préfectoraux (qui avaient d'autres chats à fouetter) et de la justice. Nous avons aussi assumé à domicile l'hébergement 24h/24 des jeunes (les MIE)  que nous suivons et dont la scolarité a été perturbée.

Les cours de français langue étrangère ont été suspendus dans les lieux publics, ils ne reprendront qu'en septembre. Néanmoins quelques cours particuliers à domicile ont vu le jour. Merci à celles qui ont donné de leur temps et de leurs compétences, bénévoles comme toujours.

**Reprise accélérée des procédures d'éloignement (le terme est plus chic que "expulsion") :**

Dans les 3 dernières semaines, une quinzaine d'OQTF (obligation de quitter le territoire) sont tombées obligeant officiellement les étrangers concernés  à partir dans les 30 jours, alors que la plupart des frontières extérieures à l'Europe restent fermées et les vols impossibles. Une logique administrative qui nous échappe !

De même les refus de titres de séjour ou de leur renouvellement, notamment pour raison de santé tombent à nouveau de manière systématique pour ne pas dire mécanique. Nous reprenons le chemin des tribunaux sans enthousiasme mais avec détermination. Les avocates de chôment pas, voire un peu débordées, comme nous : un dossier c'est plusieures heures de boulot et des RDV parfois à domicile pour ceux qui en ont un.

**Préparer la rentrée scolaire et prendre en charge de nouveaux pensionnaires :**

Malgré les pertubations actuelles, nous enregistrons les bons, voir excellents résultats scolaires des mineurs isolés que nous aidons. CAP ou Bacs Pro en poche, suivi des apprentissages, c'est le résultat de 2 ou 3 ans d'accompagnement. Il faut rappeler que RESF 09 prend en charge tous les frais de scolarité, internat compris, et qu'une dizaine de ces jeunes sont hébergés bénévolement chez des militants du Réseau. La tâche est lourde.

Nous avons envisagé d'accompagner pour l'année scolaire 2020/2021 au moins trois petits nouveaux. Après passage au CIO (Centre d'Information et d'Orientation) deux seraient en LEP à Saint-girons et un à l'EREA de Pamiers. Outre les soucis de budgétisation, nous sommes confrontés à l'hébergement (week-end et vacances scolaires) et nous sommes à la recherche de propositions, pour l'instant sans succès. C'est pourtant une condition sine qua non.

Déplorable Préfecture encore qui, en janvier/février a pris plusieurs arrêtés d'expulsion à l'encontre de jeunes, jadis confiés à l'ASE (Aide Sociale à l'Enfance) et suivant des formations en apprentissage, donc en bonne voie d'autonomie et d'intégration dans des secteurs professionnels en tension. Là aussi quelle logique administrative ? Nous reprenons le chemin du Tribunal Administratif et, parfois nous gagnons, enfin les jeunes gagnent et l'État gaspille le fric des contribuables.

Plusieures procédures de reconnaissance de minorité sont encours qui, si elles aboutissent devraient permettre la rpise en charge des intéressés par le Conseil Départemental (ASE).

**La grogne encore :**

RESF 09 participait à  la signature de la pétition large s'opposant aux expulsions. Elle a recueilli d'assez nombreuses signatures mais fut interrompue par un virus inopiné. Une audience est néanmoins demandée pour la remettre en Préfecture. Nous avons aussi porté la signature de RESF 09 à l'appel national pour la régularisation de tous les sans-papiers. On a bien le droit de rêver à une société un peu plus humaine !

**Les rencontres de RESF :**

Nous avons dû suspendre nos réunions mensuelles et nous avons fonctionné en groupe restreint (l'équipe des permanences). Dès que possible nous reprendrons ces rencontres mensuelles indispensables. En attendant, nous proposons des réunions décentralisées dont nous confions l'organisation aux "locaux". Il y a un vrai besoin de se retrouver sur le terrain. N'hésitez pas à faire remonter vos demandes et vos inquiétudes.

**Les finances :**

Bien qu'ayant quelques maigres réserves, nous dirons que les fonds sont en baisse et que les participations de soutien sont plus timides que les années précédentes. Affaire de confinement ? Il semblerait que malgré les frémissements de solidarités diverses, ce fléchissement soit général à toutes les associations. Nos principaux postes de dépense restent les accompagnements scolaires et les soutiens juridiques et financiers aux familles.

Soyons optimistes, avec le soleil revenu et le grand air, les bourses vont se délier. Merci.

Amicalement.



