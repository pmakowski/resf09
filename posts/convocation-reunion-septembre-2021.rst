.. title: Convocation réunion septembre 2021
.. slug: convocation-reunion-septembre-2021
.. date: 2021-08-23 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion septembre 2021
.. type: text

Bonjour à toutes et tous,

Il faudra vous y faire les vacances sont finies (la pandémie, non). J'espère que nous pourrons nous rencontrer plus régulièrement que l’année passée.

Plusieurs informations rapides :

1) Nous tiendrons une réunion mensuelle de RESF 09 le Lundi 30 Août 2021 à FOIX, salle des fêtes de la Maison des Associations (Avenue de l'Ariège). Les sujets ne manqueront pas:

- Poursuite des refus de séjour, des expulsions et des procédures qui s'imposent
- mineurs isolés balancés par la préfecture
- problème des hébergements
- orientation et prises en charges de nouveaux Mineurs

Nous ferons aussi le point sur l'Afghanistan et les autres verrues du monde de la guerre, sur les déclarations et les actes des gouvernants, sur les solidarités possibles, sur d'éventuelles conditions d'accueil...

2) Pour la rentrée scolaire, nous avons reconduit (réunion du 19 juillet) le principe d’une aide financière aux enfants (petits ou grands) refusés à la « prime de rentrée scolaire » par la CAF. Le budget le permet même si c’est lourd.

Nous avons listé les familles concernées mais informez aussi autour de vous, notamment sur le Couserans et les Pays d’Olmes.

Pour organiser la « distribution » nous avons prévu des RDV particuliers mais aussi deux permanences « spéciales rentrée ».

- À PAMIERS LE VENDREDI 27 AOÛT entre 9 h. et 11 h à la maison des associations (7bis rue Saint Vincent)
- À FOIX LE LUNDI 30 AOÛT entre 9h30 et 12 h à la maison des associations (Salle des fêtes dans la cour) Avenue de l'Ariège

Nous avons besoin d’une adresse d'hébergement (obligation de scolarisation pour les mairies), d’une affectation scolaire 2021/2022 et si possible d’un document reprenant la composition familiale.

La vaccination ou un test PCR sont vivement conseillés mais non imposés, pour accéder aux permanences.

3) Les permanences juridiques du vendredi se poursuivent normalement exclusivement sur RDV (parce que nous apportons les dossiers des gens concernés et que sans dossier, il n’est pas possible de travailler correctement).

Donc à bientôt et bien amicalement.

Le 23 août 2021

Christian

