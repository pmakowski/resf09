.. title: Convocation réunion avril 2024
.. slug: convocation-reunion-avril-2024
.. date: 2024-04-07 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion avril 2024
.. type: text

Bonjour,

Nous avons le plaisir de vous rappeler que le premier lundi de chaque mois nous tenons une réunion de RESF

REUNION MENSUELLE du Réseau Education Sans Frontière

Assemblée Générale Statutaire

Le Lundi 8 avril 2024  de 17H30 à 20H.

Salle F.Soulie – Mairie de FOIX


**Mineurs isolés**

Les arrivées en hausse, les évaluations et l’hébergement sont saturés. Nous avons un peu avancé sur la scolarisation de quelques nouveaux candidats. Mais nous sommes en Avril et les solutions proposées doivent plutôt être mises en perspective de la rentrée 2024 et sa préparation. Nous sommes toujours à la recherche de capacités de prise en charge pour les weekends et les vacances.

Nous reviendrons également sur les audiences chez la Juge des enfants et les 7 nouvelles demandes pour lesquelles nous avons été sollicités suite aux sorties/refus du Conseil Départemental, une liste qui s’allonge…

**La coordination contre la Loi « Asile-Immigration »**

Le planning de formation proposé par la LDH a réuni sa quatrième séance. Le public (nombreux) semble y trouver « son compte ». Nous espérons susciter de nouvelles vocations. Nous poursuivons donc ce cycle. Dommage que les organisations membres de la coordination à l’initiative de ce programme ne se soient pas plus investies, ça leur était d’abord destiné.

La conférence envisagée par la coordination sur le thème « l’immigration, une chance pour l’Europe » prend forme. Elle aura lieu le Vendredi 19 Avril à 20h30  à la Salle Jean Jaurès – Mairie de Foix et sera présentée par Mme Ekrame BOUBTANE cf `ici <https://resf.ariege.eu.org/posts/migrationeurope2024/>`_.

**Le Français Langue Etrangère (FLE)**

A lire les échanges qui circulent entre les intervenants, il semblerait que la demande est en forte hausse. Si nous avons plus d’éléments lors de la réunion de Lundi, nous verrons ce qu’il faut envisager pour consolider les équipes et répondre au mieux à l’arrivée massive de « jeunes » 

**Questions diverses :** qui sont les vôtres.

Bien amicalement à toutes et tous.

Montoulieu le 7 avril 2024

Christian

