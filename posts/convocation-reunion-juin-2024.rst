.. title: Convocation réunion juin 2024
.. slug: convocation-reunion-juin-2024
.. date: 2024-05-30 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion juin 2024
.. type: text

Bonjour,

Nous avons le plaisir de vous rappeler que le premier lundi de chaque mois nous tenons une réunion de RESF

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 3 juin 2024  de 17H30 à 20H.

Salle F.Soulie – Mairie de FOIX


**Mineurs isolés**

C’est l’hôtel des courants d’air. Toujours aussi nombreux sur le seuil des évaluations et toujours aussi nombreux à être déclarés « majeurs », nous avions décidé d’une permanence spéciale pour préparer d’éventuels recours annoncés près de la Juge des Enfants.

L’idée était louable mais le résultat nul. Les intéressés ont, semble-t-il repris le chemin « de la grande ville » malgré les RDV qui avaient été pris. Merci à celles et ceux d’entre nous qui s’étaient déplacés.

Concernant les 7 derniers présentés dernièrement  chez la Juge des Enfants, six ont vu leur recours rejeté après avoir été reçus, très mal reçus et surtout pas écoutés, pas entendus… bref un drôle de tribunal… Les 6 dossiers rejetés font l’objet d’une requête auprès de la Cour d’Appel des mineurs de Toulouse… Nous verrons bien.  

Nous travaillons à leur scolarisation éventuelle en Septembre 2024. En attendant ils suivent les cours de FLE avec assiduité. Nous sommes toujours à la recherche de capacités de prise en charge pour les weekends et les vacances.  Michèle a remis en service son « planning de l’été 2024 ».

**Les premiers ravages de la Loi « Asile-Immigration »**

Contre tout respect du principe de non rétroactivité de la Loi, principe fondamental en Droit français, l’administration l’applique rétroactivement concernant des OQTF anciennes, portant leur « validité » à 3 ans contre une seule année lors de leur édition. Donc des OQTF qui étaient caduques sont ressorties du placard comme étant toujours applicables. Un recours est déposé en Conseil d’Etat.

**Ainsi la famille « M » en fait-elle les frais cette semaine :**

Nous reviendrons sur cette situation en détail. Une requête est déposée au TA afin de tenter de faire annuler les assignations à Résidence prononcées à l’égard du couple « M »

**Formation pour les intervenants F.L.E.**

Rdv pour une demi-journée de rencontre/ formation le Samedi 15 Juin de 9 h à 12h à la MDA (voir Claudie)


**Questions diverses :** qui sont les vôtres.

Bien amicalement à toutes et tous.

Montoulieu le 30 mai 2024

Christian

