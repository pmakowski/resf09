.. title: Informations novembre 2020
.. slug: informations-novembre-2020
.. date: 2020-11-24 11:52:29 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link: 
.. description: RESF09 Novembre 2020
.. type: text


En espérant pouvoir vous retrouver rapidement à la Mairie de FOIX (en décembre : on vous
invitera...) Un point rapide :

Moussa à sa sortie de la Cour d’Appel des Mineurs de Toulouse est... mineur ! Et confié à l’ASE de
l'Ariège. Rappelons que nous avions accompagné sa scolarité au LEP Camel (2°"° année)

Amdadoul à sa sortie d'audience chez la Juge des Enfants de Foix (où il était accompagné par
Fabienne qui jouait l’interprète et par Ventzi dans le rôle de l’avocate) donc Amdadoul est lui aussi
mineur, confié à l’ASE de l’Ariège. Nous l’avions scolarisé à l’EREA où il est interne.

Nous avions obtenu la même prise en charge dès la rentrée pour trois autres mineurs.
Reste Abdou pour lequel nous attendons une audience à Toulouse. Il est aussi scolarisé à l‘'EREA et
prépare un CAP.

Là où ça fait mal : ce sont 9 OQTF pendantes pour des mineurs devenus majeurs à qui la Préfecture a
refusé un titre de séjour dont nous attendons impatiemment les dates d’audience au Tribunal
Administratif. Leurs droits étant suspendus... on passe à la caisse.

Bonne nouvelle : Nune et son fils Habet sont autorisés à reprendre le boulot et devraient être crédités
d’une AES par la Préfecture. Ça n’a pas été simple.

Bonne nouvelle encore : deux asiles subsidiaires ont été accordées récemment par la Cour nationale
du droit d’Asile.

Mauvaises nouvelles : nous enregistrons chaque semaine quelques nouvelles OQTF ou IRTF (dont 6
viennent d’être cassées par le TA maïs cinq ont aussi été confirmées).

Le reste, bon ou mauvais, vous connaissez déjà... période hivernale + covid + prolongation de
période hivernale (intermède aôut/septembre) puis re-covid et nouvelle période hivernale + les
hébergements militants et solidaires. il n’y a pas beaucoup de gens à la rue.

Côté « vie de l’Association » : nous fonctionnons toujours à distance mais avec d’excellents relais,
qu’ils en soient remerciés…. et le travail est un peu rôdé.

Côté finances : nous tenons à peu près la route mais nous allons clore les comptes 2020, alors si
vous voulez contribuer 2020 (reçu fiscal) faites-le maintenant. Merci.

C’est le trésorier qui vous parle et qui vous écrit.

Amicalement
Christian

Dernière nouvelle : ça vient de tomber, Sergo s’est fait choper au volant hier soir à Pamiers. Sous le
coup d’une OQTF, il doit être amené en rétention « à Marseille » ... Sa compagne et la gamine sont
restées ici. On fait le nécessaire pour fournir un dossier juridique au CRA de Marseille et assurer sa
représentation devant le Juge des Libertés dans les 48 heures... Malo, Malo !



