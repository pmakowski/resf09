.. title: Convocation réunion novembre 2024
.. slug: convocation-reunion-novembre-2024
.. date: 2024-10-30 09:00:37 UTC+02:00
.. tags: convocation
.. category: convocation
.. link:
.. description: RESF09 convocation réunion novembre 2024
.. type: text


**REUNION MENSUELLE du Réseau Education Sans Frontière** 

**Le Lundi 4 novembre 2024 de 17h30 à 20H.** 

Salle Soulié - Mairie de Foix

**circulaire du Ministère de l’intérieur aux Préfets**

Après les avoir « invités » à Paris pour les mettre au pas, la circulaire du Ministre vient rappeler aux Préfets qu’on n’est pas là pour rigoler et qu’il convient d’être « méchant » avec l’étranger comme le demande le bon peuple « d’extrême droite » et ses caciques. A servitude, servitude et demi…

**Mineurs isolés**

De nouveaux arrivants jetés par les « évaluations », le Procureur et le Conseil général, se voient balancés à la rue. On ne saurait mieux dire, actuellement plusieurs d’entre-eux victimes de la pratique « des nuits de carence » du 115, se retrouvent à la gare ou sous un hangar pour des nuits plus longues, plus froides et plus angoissantes… Pas si grave : les décideurs dorment au chaud et ne font même pas de cauchemars…

**Période hivernale : tout le monde à l’abri !**

Sauf les déboutés de l’asile, sortis des CADA qui devraient aussi rejoindre le 115. Il leur est systématiquement répondu qu’aucune place n’est disponible – Faux – en fait la réponse polie cache les consignes préfectorales « pas question   de mettre à l’abri des familles (souvent avec enfants) qui  ont vocation à rentrer au pays ». Traduire en termes humanitaires : « plus on les fera souffrir, plus ils seront enclins à déguerpir »

Certes, les associations locales tentent d’accueillir cette misère mais elles sont vite saturées et ne sauraient s’inscrire dans un système de substitution aux responsabilités des institutionnels. 

**Les finances en berne**

Nous approchons de la fin de l’année et les contributions semblent en forte baisse… Inquiétant. Bien sûr, RESF subit, comme toutes les associations humanitaires, une chute importante des participations même si le nombre d’adhésions reste constant.

Nous comprenons bien que ce sont toujours les mêmes qui contribuent et que les sollicitations sont de plus en plus nombreuses, que les perspectives économiques (pouvoir d’achat, chômage et retraites) pèsent lourdement.

Bref, nous devrons faire avec cette situation, peut-être revoir à la baisse, nous aussi, nos engagements. Ce serait dommage. Programmée sur deux ou trois ans,  la scolarité des jeunes que nous suivons est porteuse d’espoir et d’avenir pour eux… et pour nous tous.

**Questions diverses :** qui sont les vôtres.


Bien amicalement à toutes et tous,

Christian
