.. title: Compte rendu septembre 2020
.. slug: compte-rendu-septembre-2020
.. date: 2020-09-24 15:54:29 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link: 
.. description: RESF09 Septembre 2020
.. type: text

Présidence : Yannick Poirier
Animation : Christian Morisse

**Rencontre avec les élus et techniciens du Conseil Départemental**

(en présence de la Présidente Christine Téqui).
Le compte-rendu plus précis est disponible auprès de Yannick 
                                                                                                                             
Principaux sujets évoqués à propos des jeunes proches de la majorité : quoique déjà bien intégrés notamment sur le plan scolaire voire majeurs et bénéficiant d'un contrat qui devraient leur permettre de poursuivre leur cursus, certains font l'objet d'OQTF.

Les responsables du CD mettront ce sujet sur la table lors de leurs rencontres avec la Préfecture.

Par ailleurs, la délégation de « RESF » a rappelé la « délibération Massat » prise par le Conseil Général voici quelques années.  Elle a souhaité que les aides attribuées par le CD à l'occasion de la rentrée scolaire puissent bénéficier aux enfants des familles étrangères en situation précaire auxquelles la CAF les refuse.

À également été abordée la situation des familles qui acceptent de « parrainer » un (ou plusieurs) jeunes. Différente du statut de famille d'accueil, cette option permettrait d'être reconnue officiellement et faciliterait l'hébergement lors des week-ends et vacances scolaires. Le CD s'est déclaré très intéressé par une démarche commune avec RESF pour développer le nombre de ces familles (qui pourraient  être indemnisées).

**Requalification « jeunes mineurs » par la Juge des enfants (JDE)**

Trois situations ont été réglées dernièrement de façon positive. Ils sont tous trois scolarisés (internes) et donc pris en charge par l' Aide Sociale à l'Enfance (ASE), sous la responsabilité du Conseil Départemental.

En revanche, 7 ont fait l'objet d'une OQTF, décision que nous avons attaquée chaque fois devant le Tribunal Administratif.

S'y ajoutent deux cas en Appel, Cour d’Appel des mineurs - TGI de Toulouse, ainsi qu'une nouvelle demande auprès de la JDE.

**Permanences (le vendredi à Pamiers) pour les démarches administratives et juridiques**

Nous rencontrons des difficultés régulières de transcription des récits personnels, les demandeurs ne maîtrisant pas notre langue la plupart du temps. On constate aussi qu'avant leur venue, leur dossier a souvent déjà été ouvert par une autre structure (PADA, Forum réfugiés...) mais il convient en règle générale de repréciser et détailler leur périple et les arguments susceptibles d‘être retenus lors des entretiens à l‘OFPRA.

Bien entendu, nous devons appliquer une rigueur et une vigilance renforcées lors des permanences, COVID oblige.

**Aide rentrée scolaire**

Nous concrétisons notre engagement d'octroyer une somme de 50 euros par enfant en primaire et secondaire. Globalement, cela va représenter un budget de l'ordre de 5000 euros.

**Cours de FLE**

À Pamiers (autour de Claudie) : reprise le 15 septembre sous contraintes sanitaires.

À Foix (Yannick) : à partir du 14/09 avec les mêmes règles (groupes limités en nombre)

**Marche nationale des sans papiers (et ceux qui les soutiennent)**

Relais médiatique localement pour signaler et appuyer l'initiative. Toutefois, il reste ardu de rendre visible ce genre d‘initiative dans un contexte de rentrée difficile et animée sur bien des « fronts ».

**Mouvement de la paix**

Rassemblement prévu à Foix le 19/09 dans le cadre de la journée nationale.

Le 16 septembre 2020
Alain Lacoste
Secrétaire de RESF

