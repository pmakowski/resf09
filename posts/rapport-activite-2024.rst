.. title: Rapport d’activité  pour 2024 – Assemblée Générale statutaire
.. slug: rapport-activite-2024
.. date: 2025-01-03 08:33:49 UTC+01:00
.. tags: rapport d'activité
.. category: rapport d'activité 
.. link: 
.. description: Activités 2024
.. type: text

**Rapport d’activité pour 2024 – Assemblée Générale statutaire**

Préambule : Pour celles et ceux qui l’auraient conservé, ce document est tout à fait semblable à celui de 2023
Et pour cause, la situation faite aux « étrangers » ne cesse de se détériorer, victimes des mêmes… On tourne en rond et on recommence… Donc on se répète…

« L’immigration est plus que jamais au cœur des débats sociaux et politiques. Faire connaître au grand public nos expériences ne  peut qu’assainir ces débats et remettre de l’humain face au déferlement de haine auquel nous assistons »

L’immigration est plus que jamais l’objet de toutes les ambitions, le fonds de commerce électoral des plus moches sur l’échiquier politique… Si loin des difficultés réelles de la vie quotidienne des populations concernées. La tache brune envahit l’Europe et les surenchères d’extrême droite semblent payer électoralement. Ne nous faisons pas d’illusions, derrière cet affligeant constat, il y a des électrices et des électeurs… des gens à qui nous n’avons pas su « faire connaître notre travail »  On ne saurait tout faire !

**Sur la brèche 365 jours par an**

Le rythme de fonctionnement normal de RESF09 s’est donc maintenu toute l’année 2024, une activité essentiellement concentrée autour ou à partir des permanences hebdomadaires. Nous avons essayé de répondre à toutes les situations qui nous ont été soumises. Le tableau statistique joint est un bon baromètre. 

Les ordres tombant du Palais princier et de ses annexes ministérielles se succèdent : circulaires, injonctions, mutations, pratiques toujours plus violentes et acharnées à l’égard des plus vulnérables, les « crève la faim », les SDF sous leurs tentes et les étrangers en situation précaire… Toute la misère du Monde est désormais pourchassée par des hordes policières devenues l’instrument privilégié des gens au pouvoir.  

Nous recevons ces victimes, ces laissés pour compte  des méandres administratifs, à qui nous tentons de donner des repères dans les démarches (et les déconvenues) qui les attendent. Après les galères de la Méditerranée et les débarquements/embarquements dans la Manche ou un petit tour aux Canaries,  commence la navigation à vue dans les couloirs préfectoraux ou judiciaires.

Nous nous sommes appliqués à ce que les compétences de nos équipes sachent répondre, expliquer, guider, permettre de choisir les possibles, dans le respect des publics concernés. Au-delà des aides apportées, il s’agit de permettre à chacun  de rester l’acteur principal de sa vie personnelle et le garant de ses secrets intimes.

**Toujours présents dans la durée**

Le Réseau vient d’achever sa onzième année d’existence légale. Il est sur le terrain depuis plus de 20 ans, sous diverses formes. Le réseau est un vrai réseau où les militants et les travailleurs sociaux des différents secteurs constituent un précieux relais. 

Les permanences  que nous tenons avec suffisamment d’intervenants sont un peu la plaque tournante des décisions à prendre. Nos activités n’ont pas changé d’orientation même si le juridique occupe beaucoup plus de place. La future nomenclature du code CESEDA (Code de l’Entrée et du Séjour des Etrangers et des Demandeurs d’Asile) version 2024 nous a contraints à quelques révisions, réadaptations et formations. Les discours ministériels actuels risquent fort de nous astreindre à de nouvelles mobilisations et à de nouvelles mises à jour.

En 2023, Nous avons enregistré 116 contributions financières (cotisations et dons) pour un montant total de 21 509 € (légère hausse) en comparaison : 7 634 € en 2016 - 12 460 € en 2017 – 15 470 € en 2018 - 17 656 en 2019 – 20 925 en 2020 – 22 255 € en 2021 – 22 737 en 2022 – 20 121 en 2023. Le nombre d’adhérents reste stable, environ 150/180. La participation associative et syndicale est, elle aussi, stable. C’est rassurant. Globalement les comptes vont bien. Nous vous présenterons le bilan financier et les principaux postes de dépense. Le Réseau ne vit que de ses fonds propres, vos dons et cotisations, ce qui lui assure une totale indépendance, salutaire par les temps qui courent… 

Le site du réseau «resf.ariège.eu.org » est régulièrement pourvu (merci à notre spécialiste) et trop peu fréquenté. Il faudra voir comment en améliorer le fonctionnement et l’utilisation.
 
**Un public variant peu**

L’essentiel de nos interventions s’adresse aux personnes d’origine étrangère, surtout aux familles avec enfants, qu’elles soient demandeuses d’asile, réfugiées, primo-arrivantes, en cours ou en fin de procédures. La mobilisation en faveur des mineurs/majeurs Isolés s’est largement développée ces dernières années et constitue une part importante de nos travaux.  

Le nombre d’entrées nouvelles dans le département semble relativement stable, voire en baisse. Les familles d’origines géorgiennes, Arméniennes et Albanaises restent les plus nombreuses en Ariège. D’Afrique subsaharienne, nous accueillons surtout des migrants isolés, souvent des jeunes et des femmes seules avec enfants. Ce sont généralement des familles ayant des jeunes enfants qu’il faut mettre à l’abri, souvent soigner, orienter vers de l’alphabétisation et une scolarisation urgente. 

Les conséquences des dernières directives ministérielles malmènent les prises en charge sociales et médicales compromettent  la mise en œuvre des solidarités élémentaires. Les familles que nous accompagnons actuellement seront touchées de plein fouet.

**CESEDA « nouveau » : un arsenal au service du rejet et des expulsions** 

Même si les services préfectoraux persistent à s’appeler « service des migrations et de l’intégration ! » Qu’en termes pudiques, ces choses-là sont dites ! La Loi dite du « 26 janvier 2024 », même rectifiée par le Conseil Constitutionnel, est un outil entièrement orienté vers les refus, les rejets et les expulsions, rabotant un peu plus les maigres Droits des Etrangers. L’année politique au Ministère de l’Intérieur, pour chaotique qu’elle fut n’en reste pas moins l’une des plus acharnée contre les migrants. Nous n’avons pas chômé, les avocats et les tribunaux non plus ! Nous sommes sur le pont « fêtes et dimanches »…

Contrairement aux allégations des xénophobes, globalement, nous constatons dans le monde associatif un élan de solidarité assez général, notamment à l’égard les populations « étrangères ».

Face aux menaces législatives, l’inquiétude s’empare des personnes en situation administrative instable et les poussent à solliciter leur régularisation, donc à formuler de nombreux dossiers d’AES (Admission Exceptionnelle au séjour).

La procédure parfaitement discrétionnaire ouvre la porte à toutes les inégalités de traitement. La réponse, quand réponse il y a, est souvent négative et suivie de la cohorte d’arrêtés d’expulsion (OQTF – IRTF -  Assignation à résidence). Tous les prétextes sont bons : Compléments de dossier, papiers d’Etat civil, passeports, non intégration, remise en cause des « paternités » et tests ADN imposés… Bref un arsenal qui pourrait se résumer à dire : Quel prétexte trouver pour jeter !

En 2024, nous avons ouvert 137 nouveaux dossiers , soit environ 350 personnes nouvelles. . En fait, la file ouverte des situations a encore augmenté cette année essentiellement suite aux refus d’asile et aux sorties de CADA suivies d’OQTF  - 565 dossiers en RDV individuels ou familles. Les origines géographiques de plus en plus diversifiées posent des problèmes d’interprétariat oral et écrit mais nous sommes riches de nos différences et quand on ne sait pas faire, on fait faire et on paie….
    
Nous tentons de ne rien laisser passer et de recourir systématiquement à la Justice. Les avocates sont là, disponibles et pertinentes. Chaque requête gagnée n’est pas seulement gagnée pour les intéressés mais constitue une victoire contre la politique xénophobe du pouvoir… donc une victoire politique pour nous aussi…

Les permanences hebdomadaires à Pamiers suffisent tout juste à répondre à la demande. L’équipe semble bien rodée. Un grand merci à celles et ceux qui se déplacent chaque semaine, sachant qu’elles,  qu’ils rentrent à la maison avec du « boulot » dans le sac… 

Nous savons aussi que nous pouvons nous appuyer  sur de bonnes relations et sur l’efficacité professionnelle de quelques avocates et avocats à qui nous demandons beaucoup, bien souvent dans l’urgence. Nous avons rempli et déposé de nombreux dossiers d’aide juridictionnelle permettant aux plus démunis de faire valoir leurs Droits et d’être défendus devant le Juge.

Les capacités d’accueil à domicile sont maintenues (Familles ou Mineurs Isolés) mais nous manquons cruellement de propositions et, pour la première fois cette année, nous devons laisser quelques familles, quelques jeunes sans solution d’hébergement… La concentration de la demande sur l’axe Tarascon – Foix – Pamiers nous complique un peu les choses. Ces accueils  représentent un investissement militant important qui nécessite de mettre en commun les charges et les difficultés. Nous sommes en relation fréquente avec les lieux plus institutionnels assurant l’accueil (CADA, SAO, MECS, Accueils d’urgence, familles d’accueil, foyers, Emmaüs…). Rappelons que, faisant fi de la Loi et de ses obligations de mise à l’abri en période hivernale, la Préfecture a donné la consigne de n’héberger aucune famille en situation irrégulière.

**Quelques mots concernant notre fonctionnement…** 

Une réunion mensuelle du Réseau assure un minimum de prises collectives d’orientations et de décisions. D’abord destinée à informer, elle est devenue le lieu de rencontre des militants les plus actifs.  

Les permanences hebdomadaires fonctionnant à Pamiers constituent un lourd investissement militant. Une dizaine de camarades accueillent, sur rendez-vous, les personnes en demande. L’équipe, aujourd’hui pleinement fonctionnelle, semble bien stabilisée. De nouvelles venues « apprennent le métier », laissant espérer une relève possible.

Nous devons prendre soin d’expliquer aux intéressés quelles sont les réglementations en vigueur et le fonctionnement des institutions. Nous devons expliquer qui nous sommes… que nous sommes solidaires, que nous sommes bénévoles et que nous ne pouvons pas, ne savons pas  « tout faire »… 

**Que faisons-nous ?** 

* Le dispositif régional (SPADA) devant lequel tout demandeur d’asile doit se faire enregistrer assure un premier accueil et un accompagnement aux premières démarches. Cette année, l’essentiel des nouveaux venus sont arrivés dans le département parce qu’affectés sur les CADA (200 places). Nous n’avons eu que fort peu d’entrées directes. Il nous revient souvent de déposer les demandes de recours à la CNDA (Cour nationale du Droit d’Asile) pour les ressortissants des « Pays sûrs »
* Lorsque, déboutés, les demandeurs d’Asile doivent quitter le CADA, nous assurons la suite inéluctable  sous forme d’OQTF facile à prévoir. Nous complétons les dossiers, assumons si besoin, les traductions des récits d’exil. Nous présentons et expliquons le déroulement des futures démarches. Nous essayons d’orienter vers des hébergements, nous assurons la scolarisation des enfants s’il y a lieu. 
* L’absence de dispositif officiel de domiciliation complique la tâche. Le siège social de RESF et de la Ligue domicilie de plus en plus de dossiers (Asile, Justice ou encore CPAM). C’est une grosse responsabilité qui consiste à réceptionner, redistribuer et expliquer les courriers reçus.
* Nous organisons la défense juridique auprès du Tribunal Administratif et de la Cour Administrative d’Appel, des personnes ou familles menacées d’expulsion (Refus de séjour, OQTF, arrêtés de transfert Dublin et IRTF). Idem pour celles qui sont assignées à résidence ou enfermées au Centre de Rétention Administratif (CRA). 
* Nous avons en 2024, dû faire face à plusieurs tentatives d’expulsion. Nous avons déjà parlé ici de la violence institutionnelle dont l’administration préfectorale fait preuve sans aucun respect de la famille, sans aucune considération humanitaire. La Justice elle-même a sanctionné de telles pratiques (Juge des Libertés) et cassé des arrêtés préfectoraux. 
* Nous transmettons aux avocats, souvent dans l’urgence (procédures à 48h. de préférence pendant les weekends), les dossiers préalablement constitués « à toutes fins utiles » ce qui implique de tenir à jour ces dossiers en «alerte», y compris les demandes d’aide juridictionnelle. 
* Les délais de procédure sont encore plus restreints suite à l’application de la dernière Loi, l’objectif du pouvoir étant, faute de pouvoir supprimer le droit à un recours, de le rendre impossible par les délais imposés..
* C’est un travail d’archivage, de correspondance écrite et téléphonique. C’est un travail d’anticipation. L’expérience nous a appris à envisager tous les possibles y compris les pires et les plus urgents. Nous devons alerter les intéressés de ces éventuelles procédures sans faire « peur » inutilement.
* Nous sommes confrontés aux besoins de traductions… Celles que nous pouvons assurer nous-mêmes (plusieurs langues), celles que nous pouvons transcrire à partir d’une traduction orale, dématérialisée ou aidés d’intervenants de même origine culturelle, discrètes et compétentes (le plus fréquent). Nous commandons et payons aussi les travaux des traducteurs assermentés quand c’est indispensable mais c’est cher. 
* Nous avons constitué de nombreux dossiers d’AES (Admission Exceptionnelle au Séjour) dits discrétionnaires donc au bon vouloir du Prince. C’est long, compliqué, ça demande un travail relationnel avec les intéressés, plusieurs rencontres préalables et des centaines de photocopies de documents déjà connus des services préfectoraux mais néanmoins exigés. 
* Nous avons pris en charge quelques dossiers de demande de naturalisation. Ils sont complexes et exigeants, aux multiples documents à fournir. Depuis deux ans, la concentration administrative  entièrement dématérialisée  rend souvent les procédures inaccessibles… Les RDV dématérialisés sont quasi impossibles à obtenir ou portés à plusieurs mois. Les ratés se multiplient tout comme les refus de naturalisation plus nombreux depuis l’entrée en application de la Loi du 26 Janvier dont les décrets ont été signés en Juillet et Août par des Ministres « démissionnaires ». 
* Les équipes interviennent sur l’apprentissage de la langue, l’une à Foix, l’autre à Pamiers. La fréquentation variable est composée prioritairement de personnes qui, du fait de leur absence de statut, n’accèdent pas au FLE institutionnalisé. Cette activité, tant sur le plan de l’apprentissage que sur le plan relationnel est sans cesse sollicité.
* Les mineurs isolés, cette année encore, ont occupé beaucoup de temps et d’argent. Nous accompagnons les « rejetés du Conseil Départemental» afin qu’ils puissent avoir recours à la Justice, Juge des Enfants et Cour d’appel des mineurs. Certes des  procédures de recours existent mais, malgré nos requêtes, les réponses du Juge des enfants se fait attendre des mois, des mois pendant lesquels les mineurs sont laissés à la dérive, sans hébergement, sans argent et sans occupation. Le Juge des enfants vient de redécouvrir l’usage des tests osseux dont tout le monde dénonce l’absence totale de fiabilité. Autant de temps de perdu… Nous devons et tentons de pallier tous ces abandons sur lesquels les institutionnels, administratifs et judiciaires, ferment les yeux. Les quelques requalifications obtenues sont venues très tard… trop tard… 
* Avec la même discrétion et la même ténacité, nous avons accompagné les mineurs isolés « rejetés » vers la scolarisation et la formation professionnelle en assumant la totalité des charges de scolarité, internat, vie quotidienne, déplacements et congés scolaires. Merci à toutes celles et ceux qui mettent à la main à « la pâte » et à la poche. Idem pour beaucoup d’enfants des familles étrangères en situation précaire orientés vers une scolarité salvatrice, nous avons apporté des aides à la rentrée scolaire  (refusées par la CAF et attribuées au compte-gouttes par le Conseil Départemental). 

Nous  le disions en préambule de ce rapport, « l’immigration est plus que jamais au cœur des débats sociaux et politiques. Faire connaître au grand public nos expériences ne peut qu’assainir ces débats et remettre de l’humain face au déferlement de haine auquel nous assistons. » Nous ajoutons : Oui mais Comment ?
 
