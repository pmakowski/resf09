.. title: Convocation réunion mars 2025
.. slug: convocation-reunion-mars-2025
.. date: 2025-02-27 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion mars 2025
.. type: text

Bonjour, nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion de RESF.

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 3 Mars 2024  de 17h30 à 20H.

Salle de Réunion Maison des Associations avenue de l’Ariège FOIX

**Mineurs isolés**

Nous sommes toujours en attente des décisions du Juge des Enfants qui nous avait imposé des tests osseux. Quoi penser ou comment qualifier des délais si longs pour, rappelons-le, des procédures qui sont sensées mettre à l’abri le plus rapidement possible des mineurs isolés… qui plus est en période hivernale ? Nous verrons comment réagir à de telles carences…

Nous ferons le point sur les dernières scolarisations en cours et sur les nouvelles prises en charge que nous pouvons financièrement assumer…  Nous en avions retenu le principe lors de l’AG statutaire.

Nous tirons la sonnette d’alarme sur la nécessité et l’urgence de trouver des hébergements pérennes.

**RETAILLEAU, pas beau ! les autres non plus.**

Ses petits copains du Sénat (vous savez là où on est élu pour longtemps et où on a si peu de comptes à rendre au bon Peuple) ses copains donc, viennent de voter en avant-première un projet de Loi sur « l’interdiction de Mariage pour les sans-papiers »… On n’arrête pas le progrès ! dans la perspective d’une éventuelle nouvelle Loi « immigration »… Idem pour le Droit du Sol et le Sinistre Darmanin. 

**Journée mondiale contre les discriminations (21 mars)**

Le thème est plus que respectable mais, de journées mondiales en journées mondiales, les militants s’usent et les résultats ne sont pas nécessairement au RDV. Que ferons-nous ? si nous faisons ?   N’oubliez pas le 8 mars et les Droits des Femmes (on vous fera suivre le programme).

**Expulsions, OQTF, Assignations…**

Toute bonne administration étant au garde-à-vous… les applications de la circulaire du ministre de l’Intérieur sont immédiates. Les arrêtés tombent tous les jours et nombreux sont ceux qui « troublent l’ordre public » Ordre derrière lequel il semblerait que l’administration puisse mettre « tout et n’importe quoi ». Nous reviendrons sur quelques cas « type » traités ces jours derniers.

**Des personnes, des familles avec enfants dormiront dehors**

C’est déjà vrai en période hivernale, alors que va-t-il se passer auprès le 31 mars ? Il fera beau !

**Questions diverses :** 

qui sont les vôtres.  

Bien amicalement à toutes et tous,   Montoulieu le 26 février 2024  
Christian   
