.. title: Manifestation contre la loi sécurité globale
.. slug: manifestation-contre-la-loi-securite-globale
.. date: 2021-01-21 08:52:29 UTC+02:00
.. tags: rassemblement
.. category: rassemblement
.. link: 
.. description: RESF09 Loi sécurité globale
.. type: text


**Non à la proposition de loi dite de sécurité globale !**

La coordination #STOPLOISECURITEGLOBALE09appelle à se mobiliser contre la loi de sécurité globale **le samedi 30 janvier 2021, 10h30, Halle de Villote, Foix.**

Que ce soit dans l’affaire Benalla, ou lors de répression de mouvements sociaux, les violences policières envers des manifestants syndicalistes ou citoyens avec l’utilisation de LBD, grenades… celles à l’encontre du campement des migrants de la place de laRépublique à Paris ou d’un producteur de musique, dans toutes ces situations ce sont les images qui ont permis de dévoiler au grand jour des exactions notoires des forces de l’ordre.

Nous apportons notre soutien aux victimes de toutes ces violences et agressions racistes, anti-militantes, dans un contexte où les forces de l’ordresont aussi en difficulté par manque de moyensnotamment. Nous exigeons, au-delà des sanctions qui doivent être prises à l’encontre des auteurs de ces faits, qu’un profond changement soit effectué dans la politique de sécurité du gouvernement et de la doctrine du maintien de l’ordre. La loi dite de sécurité globale, dans la droite ligne du nouveau schéma national du maintien de l’ordre, favoriserait les dérives et conduirait à un sentiment d’impunité pour les policiers coupables de violences. C’est une loi qui prend sa place dans le cadre d’un climat sécuritaire et autoritaire installé par le pouvoir et qui tourne le dos aux principes démocratiques et de la défense des libertés publiques.

La réécriture de l’article 24 n’enlève rien à la dangerosité de cette loi.

**Cette proposition de loi doit être retirée** tout comme les décrets parus en décembre 2020, qui étendent les possibilités de fichage pour opinions politiques ou syndicales.

Premiers signataires : CGT 09, FSU 09, LDH 09, RESF 09, PCF 09, PS 09…

