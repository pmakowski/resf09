.. title: Compte rendu mai 2024
.. slug: compte-rendu-mai-2024
.. date: 2024-05-15 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description: RESF09 mai 2024
.. type: text


**Mineurs isolés**

Nous avons accompagné 7 jeunes dont le cas a été examiné très récemment par la Juge des Enfants. Comme à notre habitude, nous avions réuni les éléments pour tenter de justifier la reconnaissance par la Juge, de la minorité de ces jeunes.  
Sur 7 jeunes 1 seul est reconnu mineur. Ce jour-là, nous avons mesuré l’évolution et la nuisance de la Loi du 26 janvier en attribuant à un Juge unique la capacité à prendre seule la non recevabilité des requêtes sur lesquelles nous avions travaillé… Ce jour a été, pour les jeunes … (et aussi pour nous), un moment difficile. Ce que nous craignions s’est (malheureusement) ce jour-là, vérifié.  
Dès réception de la notification « officielle » (qui nous est annoncée pour fin mai) nous ferons, bien évidemment, appel… et une fois de plus nous mobiliserons nos énergies et de notre temps.  
Par contre, dans le même temps, nous nous réjouissons qu’un jeune que nous suivons et dont nous avons monté le dossier vient d’être pris en charge par l’ASE (Conseil Départemental). Côté positif et très manichéen, cette décision allège d’autant les frais que nous avions budgétés pour ce jeune.  
En cours de réunion un échange s’engage sur les conditions de l’apprentissage pour les jeunes sans papiers.

**Quelques précisions ont été amenées :**

* Dans un lycée professionnel, la formation professionnelle sur le terrain s'exerce pendant des stages : 16 semaines au maximum pendant l'année. Au Centre de Formation des Apprentis, l'alternance est de 28 à 32 semaines de formation en entreprise.
* Par contre, le CFA de Foix n’accepte pas d’apprentis en « situation irrégulière », alors que les Lycées professionnels publics n’exigent pas la possession de titre de séjour.
* Un contrat d’apprentissage n’est pas assujetti à la possession par un jeune de papiers attestant la nationalité française.

**Regard sur nos finances :**

Nos prévisions « prévoyaient le pire » … heureusement le pire n’est pas arrivé.  
Actuellement +/- 40 jeunes sont en attente d’évaluation. En Ariège (comme dans la majorité des autres départements d’Occitanie) ce sont les salariés de l’ANRAS qui assure les bilans. En règle générale le Procureur suit les rapports qui lui sont transmis.  
L’ANRAS travaillant « à son rythme » (qui est fonction du nombre de personnels dédiés…) , les rapports d’évaluation arrivent donc lentement… égrenant d’autant nos actions de suivi… et donc notre mobilisation financière…

**Hébergement :**

Emmaüs Pamiers accueillera cet été 3 jeunes « sous tente » dans un « stage international ». Resteront 3 jeunes qui auront besoin (leur internat étant fermé) d’un accueil/hébergement pour les vacances.  
**APPEL PRESSANT :** N’hésitez pas (même pour une période courte) à nous signaler vos possibilités.

**OQTF**

Fin avril, nous étions assez satisfaits d’être « à jour » des dossiers que nous suivions… c’était sans savoir qu’en +/- une semaine arriveraient 7 OQTF (la dernière ce midi). 2 sont du type « 15 jours ». (ce qui n’impose pas de se présenter 5 fois par semaine mais 2 fois en 15 jours). Elles peuvent être contestées dans les 15 jours et le Tribunal doit répondre en 96 heures. Question : Pourquoi 2 ? Hypothèse : Le mois de mai et la multiplication de jours fériés nécessite de déployer les forces de Police sur le suivi de la circulation routière ?  
Nous constatons qu’actuellement la CNDA (que le gouvernement a annoncé comme devant être régionalisée), semble « gelée ». ???

**Projection du film : "La Spirale" à Montaigut.**

Le film rend fidèlement l’ambiances du CRA de Cornebarrieu. Ça ressemble à une prison. C’est souvent la dernière étape avant l’expulsion forcée. 
Si vous voulez expliquer ce que font la LDH et RESF, projetez ce film. Il est le reflet fidèle de ce nous faisons au quotidien. Christian MORISSE possède un DVD… pour tout emprunt : voyez avec lui !

**Festival RésistanceS**

Les 8 et 9 juillet le thème est : « A court de justice » Philippe M. nous interroge sur la tenue d’un débat après la projection du moyen métrage « Un pays de papier ». Accord de principe est donné. Christian M. est sollicité pour l’animer.  
Accord pour que la LDH conjointement avec RESF tiennent un stand ces 8 et 9 juillet dans le hall de l’Estive. Il faudra des volontaires…

**FLE Pamiers**

Rien de neuf. De 5, nous sommes à +/- 20 personnes, pour 2 encadrants le mardi et de 2 à 4 le jeudi.  
Claudie a demandé (et obtenu) la participation de Bruno DEGREVE à une matinée d’échange sur les « bonnes pratiques »le samedi 15 juin à Pamiers. On en reparlera… début juin.


Pas de questions diverses…


Secrétaire de séance : Francis LAVERGNE

