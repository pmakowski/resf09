.. title: Convocation réunion février 2023
.. slug: convocation-reunion-fevrier-2023
.. date: 2023-01-29 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion février 2023
.. type: text

Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion de RESF

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 6 février 2023  de 17H30 à 20H.

Salle Jean Jaurès – Mairie de FOIX

**Les hoquets de l’intérieur, petit ministre deviendra grand pourvu que…**

Les menaces qui pèsent sur les retraites ne sauraient faire oublier les manigances souterraines concernant le projet de Loi sur L’immigration. Il fallait s’y attendre : l’extrême droite et les Républicaines, si tant est qu’on puisse faire la différence, s’engouffrent dans la surenchère… un petit coup de 49-3 pourrait faire l’affaire, et là, sans risque de censure. Qu’en il s’agit d’être méchants, xénophobes et inhumains, les acteurs sont légion…

**Ukraine : Aussi sale que toutes les guerres**

Mais que deviennent les réfugiés de la première heure, dans le département et dans le pays ? On entend beaucoup plus parler des ventes d’armes (toujours lucratives) que des exilés. Quand fera-t-on un sort « spécial » aux Palestiniens ? à moins qu’ils n’attendent pas grand-chose de la diplomatie française…

**les mineurs isolés**
 
Toujours pas d’audience chez la Juge des enfants pour deux d’entre eux. Les expertises demandées ne sont, parait-il, toujours pas revenues… 9 mois après ! Qu’ils, qu’elles traînent encore un peu et les jeunes seront majeurs.

**Quelques situations alarmantes : au cas par cas.**

L’acharnement pour l’exemple, chacune de ces situations est un cas d’école, certes pour l’administration qui veut montrer ses capacités à obéir au Ministre (tiens : encore lui !) mais aussi pour les militantes et militants qui affinent leurs capacités à venir en aide. 

**Bilan de l’exercice 2022 : que peut-on améliorer en 2023 ?**

L’AG statutaire s’est tenue en Janvier. Après vous avoir proposé des brouillons, nous vous faisons parvenir les textes corrigés et adoptés. Mais la vie continue et nous devons nous interroger sur des améliorations toujours possibles.

**Questions diverses :** qui sont les vôtres

Bien amicalement à toutes et tous.

Montoulieu le 29 Janvier 2023

Christian

