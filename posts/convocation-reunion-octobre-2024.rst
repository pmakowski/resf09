.. title: Convocation réunion octobre 2024
.. slug: convocation-reunion-octobre-2024
.. date: 2024-09-29 09:00:37 UTC+02:00
.. tags: convocation
.. category: convocation
.. link:
.. description: RESF09 convocation réunion octobre 2024
.. type: text


**REUNION MENSUELLE du Réseau Education Sans Frontière** 

**Le Lundi 7 octobre 2024 de 17h30 à 20H.** 

Salle 1er étage - Maison des associations de FOIX

**Ce mauvais vent qui vient de Droite (ou d’extrême droite)**

Le président de la République, empêtré dans ses errements politiques et ses défaites successives a cru bon de désigner un Premier Ministre de Droite/Droite+  et de valider un Gouvernement de la même couleur. A peine installés dans leurs fauteuils (entendez « sièges éjectables »), tous ces promus d’un jour hurlent avec les loups d’une autre droite extrême, contre les immigrés, les étrangers, sources de tous les maux de la Terre et de la France… Le nouveau Ministre de l’intérieur, tout juste sorti du confessionnal sénatorial, clame haut et fort qu’il entend bien rejeter « son prochain » à la mer. L’histoire de France est pavée d’épisodes de ce type où les politiques du bouc émissaire ont produit les effets que nous connaissons… Les vieux démons sont de retour.

**Mineurs isolés**

Tenaces, concrets  et solidaires, nous poursuivons l’accompagnement des jeunes scolarisés l’an passé qui donc, poursuivront leur formation professionnelle. Cinq d’entre eux restent à la charge de RESF. Nous accueillons à cette rentrée sept « petits nouveaux » également scolarisés après les évaluations de rigueur, soit un total de douze jeunes dont une seule fille. Quelques dossiers sont présentés au Juge des enfants pour une « réestimation de minorité », quatre dossiers sont pendants en Cour d’Appel. 

Ce point étant fait en réunion, nous aurons à lancer une campagne de collecte d’adhésions supplémentaires et surtout à répondre aux besoins urgents d’hébergement (weekends et vacances scolaires). Merci à celles et ceux qui s’y collent déjà.

Nous reviendrons aussi sur les besoins en terme de santé et les menaces qui planent sur l’AME. Comment peut-on relayer les protestations émises par plusieurs personnalités dans la presse nationale ?   .


**Rentrée scolaire**

Nous avons reconduit les aides mises en place les années précédentes. Nous en ferons un premier bilan qualitatif et quantitatif, la solidarité a un prix… 

**Des permanences de plus en plus chargées.**

Mais aussi de mieux en mieux assurées… l’expérience aidant et les formations acquises nous garantissent une équipe efficace. Néanmoins elle reste perfectible. Nous en parlerons, toute remarque, critique ou suggestion seront les bienvenues. 


**Questions  diverses  :**  qui  sont  les  vôtres. 


Bien amicalement à toutes et tous,

Christian
