.. title: L'Ariège dit stop aux expulsions
.. slug: lariege-dit-stop-aux-expulsions
.. date: 2020-03-15 17:20:26 UTC+01:00
.. tags: pétition
.. category: 
.. link: 
.. description: 
.. type: text


L’Ariège a connu récemment des expulsions brutales, inhumaines, indignes
touchant notamment de jeunes enfants scolarisés et leurs parents.
Nous exigeons que la France respecte ses engagements internationaux en
matière de droit à l’éducation, de droits des enfants, des droits de l’Homme.
L’Ariège a toujours été une terre d’accueil et doit le rester pour les accueillis
comme pour les Ariégeois.

Nous, signataires, demandons l’arrêt de toutes les expulsions sur le
département.


**Liste des organisations signataires** : Réseau Education Sans Frontières 09, Ligue des Droits de l’Homme 09,
Association Couserannaise pour l’Accueil des Réfugié-es et des Migrant-es, Génération’s Ariège, Parti
Communiste Français 09, Bénédicte Taurine Députée 1 ère circonscription, Michel Larive Député 2 ème
circonscription, SNUipp-FSU 09, Solidaires 09, La France Insoumise 09, Collectif soutien aux réfugiés 09, Comité
contre les OQTF, Conseil Ariégeois des Parents d’Elèves, Fédération des Conseils de Parents d’Elèves 09, Collectif
soutien de Massat, NPA 09, CIMADE 09, CGT 09, FSU 09, Coordination Volvestre pour le développement des
solidarités, PEP 09, Action des Chrétiens pour l’Abolition de la Torture09, Collectif de Montbrun Solidaire avec
les Migrants, Amis de la Fondation pour la Mémoire de la Déportation 09, Monnaie 09, CCFD-Terre Solidaire,
Parti Socialiste 09, Mouvement pour la paix 09, UNSA Education 09, ATTAC 09, SNES-FSU 09, SNEP-FSU 09, FO
09, EELV, la Confédération Paysanne, le Cercle Cœur Colibris Couserans, CNT 09, Ligue de l’enseignement, Cent
pour un toit, Union des Familles Laïques, Association Départementale des Elus Communistes et Républicains

Vous pouvez vous aussi signer la pétition en ligne : `http://chng.it/WjCvxhBQ <http://chng.it/WjCvxhBQ>`_

