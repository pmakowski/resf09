.. title: Lettre ouverte à Emmanuel Macron
.. slug: lettre-ouverte-a-emmanuel-macron
.. date: 2020-04-29 16:41:21 UTC+02:00
.. tags: pétition
.. category: 
.. link: 
.. description: 
.. type: text

RESF 09 et la section LDH Ariège signent cette lettre à l'initiative des États Généraux des Migrations.

Monsieur le président, Nous – Etats Généraux des Migrations, associations, syndicats et collectifs – vous demandons de procéder à la régularisation immédiate, pérenne et inconditionnelle des personnes sans-papiers.

La pandémie à laquelle nous faisons face aujourd’hui révèle au grand jour les inégalités sociales, économiques et sanitaires qui minent notre société. Dans cette période, la situation des personnes sans papiers est des plus inquiétantes. L’absence de titre de séjour les privant de l’accès aux droits sociaux fondamentaux tels que le droit au travail, au logement, aux prestations sociales, l’état d’urgence sanitaire démultiplie leur précarité. Durant le confinement, l’absence de droit à l’assurance chômage, alors que la plupart travaillent, le risque d’être contrôlé·es, placé·es en rétention – car les centres de rétention administrative ne sont pas tous fermés et sont des lieux autant propices à la propagation du Covid-19 que les prisons – et, l’absence de logement à leur nom font des personnes sans papiers des victimes potentielles de l’épidémie davantage que le reste de la population.

Si leur sort a pu réapparaître dans le débat public aujourd’hui, c’est uniquement en termes de santé publique, et les quelques solutions apportées ici ou là pour permettre à certain·es d’obtenir un lieu d’hébergement plus conforme aux exigences sanitaires ne sont qu’un pis-aller, au demeurant très provisoire. La situation de ces personnes auxquelles l’État refuse d’accorder le droit au séjour est le résultat de politiques migratoires toujours plus restrictives et déshumanisantes, d’un choix du non-accueil, du primat de la logique utilitariste sur celle de l’égalité et de la dignité humaine.

Cette situation va perdurer une fois la crise sanitaire passée. Les personnes sans papiers continueront d’être maintenues dans une situation de non droit qui les rend vulnérables à l’exploitation et aux abus de toutes sortes, en sus de la menace du contrôle, du risque de placement en centre de rétention et d’expulsion. Leur traitement comme main d’œuvre flexible et à bas prix contribue depuis de nombreuses années à la dégradation des droits du travail de toutes et tous.

Parce que l’accès à la dignité et aux droits fondamentaux ne peut ni être affaire de circonstances, ni servir des intérêts économiques, mais doit constituer au contraire une exigence non négociable d’égalité, nous vous demandons de régulariser immédiatement, de façon pérenne et inconditionnelle toutes les personnes sans papiers, étape nécessaire du changement radical des politiques migratoires que nous revendiquons toutes et tous depuis de nombreuses années.

