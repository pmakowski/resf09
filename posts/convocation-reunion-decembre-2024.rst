.. title: Convocation réunion décembre 2024
.. slug: convocation-reunion-decembre-2024
.. date: 2024-12-01 09:00:37 UTC+02:00
.. tags: convocation
.. category: convocation
.. link:
.. description: RESF09 convocation réunion décembre 2024
.. type: text


**REUNION MENSUELLE du Réseau Education Sans Frontière**

**Le Lundi 2 décembre 2024 de 17h30 à 20H.**

Salle Soulié - Mairie de Foix

**Ministère de l’intérieur**

La nomination de Patrick Stefanini comme « représentant spécial » de l’Intérieur pour négocier les accords de réadmission de personnes en situation irrégulière n’est pas de bonne augure. Nous reviendrons sur le parcours et le profil de ce bonhomme… Le temps que durera le Gouvernement Barnier…

**Mineurs isolés**

Nous ferons le point sur les dernières audiences auprès du Juge des enfants et de la Cour d’appel des mineurs, sur les scolarisations en cours et sur la perspective de nouvelles prises en charge (affaire d’hébergement et gros sous…)

**Expulsions, OQTF, Assignations…**

La leçon semble avoir été bien entendue chez les préfets et les consignes du Ministère de l’intérieur font fureur dans les rangs. Du zèle, certes, des promotions sûrement ou des mises en « vacances » ? toujours est-il que les arrêtés tombent tous les jours et que les listes de RDV aux permanences s’allongent.

**Journée mondiale des Droits des Étrangers**

D’un commun accord avec la coordination du « 26 janvier » nous avons considéré que la date du 18 décembre est fort malvenue. Nous avons décidé d’organiser cette journée « Droits des étrangers » le Samedi 25 janvier 2025 à FOIX (rassemblement, animations, stands et ateliers thématiques) une date à retenir dès maintenant. Le 18 décembre une marche est prévue à Saint Girons.

**Les finances dernière ligne droite**

La clôture des comptes 2024 est fixée au 31 décembre et il n’est jamais trop tard pour figurer sur la liste, Oh combien précieuse ! des joyeux donateurs/adhérents. A ce jour les contributions semblent en baisse sensible…  Bien sûr, RESF subit, comme toutes les associations humanitaires, une chute importante des participations même si le nombre d’adhésions reste constant.

Bref, nous devrons faire avec cette situation, peut-être revoir à la baisse, nous aussi, nos engagements. Ce serait dommage. Programmée sur deux ou trois ans,  la scolarité des jeunes que nous suivons est porteuse d’espoir et d’avenir pour eux… et pour nous tous.

**Questions diverses :** qui sont les vôtres.


Bien amicalement à toutes et tous,

Christian
