.. title: Compte rendu juin 2021
.. slug: compte-rendu-juin-2021
.. date: 2021-06-15 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Juin 2021
.. type: text

Réunion du 7 juin 2021 (Maison des associations Foix)

En l'absence de Yannick (excusée), Christian Morisse (ré)ouvre cette rencontre à caractère mensuel mais qui avait été interrompue -Covid oblige- depuis septembre 2020.

Pour autant, le travail de traitement et de suivi des dossiers s'est perpétué tout comme les décisions « peu aimables » initiées par la Préfecture les concernant.

On a même noté une accélération zélée des OQTF (Obligation de Quitter le Territoire Français) pendant le(s) confinement(s) ce qui a rendu certaines démarches encore plus difficiles, en particulier sur le plan juridique. 

Heureusement, les avocats se mobilisent toujours fidèlement à nos côtés de façon préventive, y compris le weekend car on doit parfois réagir sous 48 h.

Le plus grave tient au caractère systématique des décisions préfectorales qui ne tiennent en aucun cas compte des situations et/ou difficultés personnelles et familiales. On a même constaté ici ou là la non application de décisions de justice...

Une anecdote lamentable pour clore le constat : l'usurpation d'identité au sein-même du service étrangers de la Préfecture, opérée par un employé qui s'est fait passer au téléphone pour son chef de service ! Heureusement, Christian garde l'ouïe fine et n'a pas manqué d'en référer à qui de droit...

On remarque aussi que les policiers sont de plus en plus sollicités pour contacter directement les personnes et familles concernées dont le moral est souvent très bas.

Côté Conseil Départemental, les relations se sont améliorées, les élus,  notamment la Présidente, s'intéressant de plus près au sort des jeunes qui prennent contact avec leur service spécialisé (ASE).

En ce qui concerne la trêve hivernale, terme officiellement abandonné, les places d'hébergement restent ouvertes mais pas forcément pour les mêmes, ce qui nécessite encore et toujours de trouver de nouvelles solutions.

**Toujours motivés**

Reste que le Réseau continue de fonctionner, techniquement et humainement, en particulier en liaison avec les travailleurs sociaux des différentes structures mais aussi, soulignons-le, les entrepreneurs, artisans le plus souvent, qui jouent le jeu de l'intégration des jeunes.

Notre situation financière révèle une évolution positive : les dons et cotisations sont passés de 7600 € en 2015/2016 à près de 21000 € l'an dernier, réconfortant quant à la volonté de soutien croissant de nos amis. Les charges sont aussi en forte hausse (notamment les taxes administratives en Préfecture)

Pour terminer, un point précis de la situation actuelle touchant 157 adultes et 112 enfants, des familles menacées d'expulsion et ne pouvant donc pas travailler :

- 10 dossiers d’Arméniens
- 28 dossiers de Géorgiens
- 21 dossiers d’Albanais
- 23 dossiers venant du Maghreb et de l'Afrique
- 9 dossiers d'origine plus lointaine

auxquels s'ajoutent 14 mineurs isolés visés par des OQTF.

Donc pas (ou peu) de vacances cet été !

Alain LACOSTE Secrétaire Le 15 juin 2021
