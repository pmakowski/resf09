.. title: Compte rendu mars 2023
.. slug: compte-rendu-mars-2023
.. date: 2023-03-10 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Mars 2023
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse

La délocalisation de la réunion sur Pamiers (faute de salles disponibles à Foix) a vu la participation se réduire sensiblement.

**Tour d'horizon de l'actualité**

Projet de loi « Darmanin » : le texte va être présenté et débattu au Parlement à partir du 17 mars. Le jour-même, RESF organise une réunion de présentation du contenu du texte gouvernemental (rendez-vous à partir de 14 h. à la maison des associations à Pamiers).

Jeunes migrants pris en charge par nos soins : Brice et Makan ont (enfin !) fait l'objet d'une décision de la juge des enfants, sans même avoir été reçus. La décision (de droit divin ?) est tombée : le premier a été déclaré majeur, le second mineur... à quelques semaines de sa majorité, lequel sera pris en charge par l'ASE pour peu de temps.

Un tour de table s'instaure à propos d'une exposition initiée par RESF Hautes Pyrénées sur l'immigration ; est-il opportun de la présenter en Ariège et quand? Il serait alors souhaitable de la coupler avec une (ou des) manifestations à caractère pédagogique. Mais la période à venir s'annonce peu porteuse : examen du projet de loi, législative partielle.

Plus largement, une question de fond se pose : comment informer le grand public et particulièrement les jeunes sur ce sujet soumis aux dérives médiatiques, politiciennes et autres réseaux sociaux ?

Une opportunité se présente : la sortie du nouveau livre de François Héran (sociologue reconnu et spécialiste de l'immigration) intitulé « Immigration : le grand déni ».

**Période hivernale :** une vingtaine de situations incertaines et difficiles se profile dans quelques semaines pour autant de familles démunies ; s'y ajouteront les sorties de CADA.

Quelles priorités va t'on devoir définir selon les cas : situation administrative, présence d'enfants... ?



Alain Lacoste  secrétaire
