.. title: Convocation réunion avril 2021
.. slug: convocation-reunion-avril-2021
.. date: 2021-03-30 16:03:08 UTC+02:00
.. tags: convocation
.. category: convocation
.. link: 
.. description: RESF09 convocation réunion avril 2021
.. type: text

Bonjour,

Avec le Printemps, les ours ressortent de leur tanière…

Nous avons le plaisir de vous annoncer une réunion de RESF (enfin)

Dans des horaires impartis (y compris les trajets)

Assemblée générale statutaire (bilan 2020)

Du Réseau Education Sans Frontière

Le LUNDI 12 AVRIL 2021 de 15H. à 18H.

Salle Jean Jaurès – Mairie de FOIX

Nous commencerons par regarder dans le rétroviseur. Vous avez eu les documents publiés en Janvier et avez 14 jours pour réviser, critiquer et commenter et, espérons-le, adopter.

Ensuite nous pourrons faire un point sur la triste situation faite aux familles migrantes et les procédures disproportionnées commandées par le Sinistre Darmanin et ses copains.

Donc vous n’échapperez pas au compte rendu de l’audience que nous avons eue chez la Préfète le mardi 16 mars et la conférence de presse qui a suivi.

Enfin, nous essaierons de définir quelques priorités pour les mois à venir et arrêter les modes de fonctionnement pour que le Covid ressemble à de la « démocratie par délégation de pouvoir ». A la veille des élections, le mot résonne mal…

Nous avons établi des questionnaires (http://ldh-midi-pyrenees.org/wp-content/2021/03/A-CANTONALES.pdf et http://ldh-midi-pyrenees.org/wp-content/2021/03/A-REGIONALES.pdf) dont vous pourrez vous emparer pour aller poser quelques questions aux candidats (déjà en lisse) pour les élections départementales et régionales. Nous les centraliserons, espérant couvrir l’ensemble du département, ferons quelques statistiques et rendrons publiques les réponses. Le succès de l’opération dépend de vous et de votre capacité à parcourir les routes de montagne (à bicyclette comme Paulette). Par les temps qui courent, il faut s’aérer.

A bientôt donc, espérons-le

Bien amicalement à toutes et tous

Montoulieu le 29 mars 221

Christian 
