.. title: Compte rendu juillet 2024
.. slug: compte-rendu-juillet-2024
.. date: 2024-07-03 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 juillet 2024
.. type: text

**L'actualité**

Cette réunion se tient au lendemain du 1°’ tour des élections législatives. Les résultats, étaient
prévisibles … mais la réalité n'en n'est pas moins crue : le Rassemblement National est le premier
parti de France. Avant d'entrer dans le détail des sujets habituels et locaux, un tour de table improvisé a permis à chacun d'exprimer son émotion, ses craintes et ses espoirs.

Si l'émotion est réelle (et fondée) elle ne doit pas nous paralyser. Les Lois dites « Darmanin » dont
la loi « asile et immigration », étaient déjà là pour répondre à l'émotion plus qu'à la raison de
personnes en colère, plus fâchés que fachos qui croient qu'en votant pour un parti qui n'a jamais
occupé formellement le pouvoir, leur vie va changer. Elles découvriront alors, que ce sont elles
qui sont, entre autres, les victimes de leur propre vote.

**L'avenir ?**

Pour tenter de nous projeter dans un avenir très incertain, nous avons évoqués les premiers
changements qu'entraineraient la prise de pouvoir du RN et consorts. Finalement, tout n'étant
que suppositions, nous convenons de nous recentrer sur les urgences liées aux personnes qui
viennent vers nous pour trouver un appui et un accompagnement dans les jours et semaines à
venir.

**A faire pour juillet/août :**

Les seules pistes qui permettent l'intégration sont la fréquentation scolaire et/ou
l'apprentissage. Nos efforts doivent se concentrer pour trouver dans les quelques possibilités, au
plan local, des solutions de scolarisation et d'apprentissage. En corolaire le réseau doit aussi,
autant que faire se peut, trouver des solutions d'accueil pendant les vacances et les W-Ends pour ces mineurs/majeurs isolés.

S'en suit un examen au cas par cas des pistes envisageables pour chaque jeune entre EREA et
Lycées professionnels pour le versant scolarisation/apprentissage.

Le passage au CIO est également à prévoir dès le 25 août pour déterminer le niveau d'acquis.

Autre tâche est la coordination des possibilités d'accueil dans les familles pour que personne ne
se retrouve à la rue dans cette période estivale. C'est tous les étés, avec une dizaine de familles,
que ce « Rubik's Cube” se joue. Vous avez une possibilité (ne serait-ce qu'un W-End) > vite: un
mail, un SMS, un coup de téléphone … Christian.morisse@nordnet.fr - 06 70 94 08 48. ça peut aider à
résoudre ce casse tête. Merci.


Le Secrétaire, Francis Lavergne

