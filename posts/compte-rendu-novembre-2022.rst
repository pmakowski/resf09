.. title: Compte rendu novembre 2022
.. slug: compte-rendu-novembre-2022
.. date: 2022-12-15 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 Novembre 2022
.. type: text

Présidence : Yannick Garcia

Animation : absence « covidesque » de Christian Morisse

Réunion mensuelle du 7 novembre 2022


**Questions diverses**

(Claudie) : contacts récents avec l'association « Regards de femmes » pour faire bénéficier les personnes qui le souhaitent des cours de FLE.

Possibilités ouvertes par le CESU (chèque emploi service) utilisables pour les offres de travail personnelles ; à noter la nécessité d'opérer la première démarche déclarative par courrier.

Animation solidaire au Carla Bayle : du 11 au 20/11, une cinquantaine d'artistes proposent une exposition vente de 13 à 18 h. hormis le lundi.

Hommage à Gérard Bérail le 3/12 à la mairie de Foix (salle J. Jaurès à 15 h.) avec divers centres d'intérêt : poésie, chansons...

Journée d'information au Fossat sur les femmes migrantes à l'initiative de diverses associations dont l'Adoma, le 20/11.

A noter du côté de l'OFPRA la mise en place d'un nouveau service qui suivra particulièrement les situations de vulnérabilité.

Samedi 12/11 à Massat assemblée générale de l'association « Repoussons les frontières » suivie d'un concert.

Le 25/11 journée consacrée aux violences faites aux femmes: à Lavelanet (20 h. 30) projection du film « Ripostes féminines » ; à Foix, groupe de présence au marché à partir de 11 h. et expo. l'après midi à la mairie (salle J. Jaurès).

Les 19 et 20/11 à Pamiers (salle Espalioux) exposition – films – théâtre à l'initiative de la Cimade avec Migrant'scène.

**Mineurs isolés**

On n'enregistre pratiquement plus aucune arrivée en Ariège. Les deux derniers que nous suivons sont pris en charge comme internes.

Demeure le problème récurrent de leur hébergement le weekend et pour les vacances scolaires (appel aux bonnes volontés)

**AES**

La plupart des dossiers de demande en préfecture entraîne des OQTF. En la matière, on attend le contenu du nouveau projet de loi qui s'annonce encore plus contraignant.

A l'heure actuelle, on constate une forme d'acharnement ciblé sur certaines familles, en particulier les hommes (père, mari) qui doivent quitter leurs proches qui se retrouvent en plein désarroi.

**Période hivernale**

Il convient de rester attentifs vis à vis de la situation des enfants afin que leur nouvelle adresse éventuelle ne contrarie pas leur scolarité. Heureusement, les portes des campings se sont réouvertes et à ce jour aucune famille ne semble se trouver sans solution.

**FLE**

À Pamiers, le problème de manque d'assiduité reste préoccupant. Toutefois, de nouvelles demandes se sont fait jour. Rappelons que les cours ont lieu près de la Croix Rouge les mardis et jeudis de 10 h à 11 h 30.

Le Secrétaire, Alain Lacoste
