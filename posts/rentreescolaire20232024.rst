.. title: Rentrée scolaire 2023/2024 
.. slug: rentree-scolaire-2023-2024
.. date: 2023-08-16 08:55:00 UTC+01:00
.. tags: rentrée
.. category: 
.. link: 
.. description: 
.. type: text


Nous renouvelons notre soutien aux enfants scolarisés, de la maternelle au supérieur, dont les familles ne touchent aucune aide officielle, n’ayant pas accès aux aides de la CAF. 

L'an dernier, une quarantaine de familles ont bénéficié de notre intervention 
pour 80 enfants environ. Cette année, allons organiser une série de rencontres 
avec les familles afin de mesurer aussi précisément que possible leur situation. 

Des permanences spéciales rentrée sont programmées 

**FOIX** : Mercredi 30 août et Mercredi 6 septembre, à partir de 17h. 

(Maison des Associations — Salle des fêtes - avenue de l’Ariège) 

**PAMIERS** : Vendredi 1 septembre et Vendredi 8 septembre le matin de 9h à 11h . 

À la Maison des Associations — 7 bis rue Saint Vincent 

En parallèle avec la permanence habituelle 

Un tarif progressif d'aide est mis en place (primaire, collège, lycée et faculté). 

Il est utile de rappeler que des dispositifs sont mis en place pour préparer au moindre coût cette rentrée. Les associations locales, Emmaüs en particulier, font des offres. 

