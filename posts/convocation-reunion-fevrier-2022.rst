.. title: Convocation réunion février 2022
.. slug: convocation-reunion-fevrier-2022
.. date: 2022-01-19 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion février 2022
.. type: text

Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF

La prochaine tiendra lieu :

d’ASSEMBLEE GENERALE STATUTAIRE ANNUELLE pour 2021

 Du Réseau Education Sans Frontière

Le LUNDI 7 février 2022  de 17H30 à 20H.

Salle Jean Jaurès – Mairie de FOIX

1) L’Assemblée générale statutaire

L’année précédente cette réunion a dû être reportée plusieurs fois pour cause de pandémie. Espérons que cette année nous puissions tenir les dates prévues. Il faudra néanmoins respecter les règles sanitaires en vigueur.

Le rapport moral et le rapport d’activité : un vaste regard dans le rétroviseur d’abord, nous n’avons pas chômé malgré les contraintes de fonctionnement. Le rapport général sera accompagné d’un relevé statistique de nos activités essentiellement mises en œuvre lors des permanences hebdomadaires. Nous le soumettrons à l’œil critique de l’Assemblée et essaierons de définir quelques priorités et lignes de force pour 2022. Vos suggestions sont d’ores et déjà les bienvenues. Nous essaierons de vous adresser un document préparatoire quelques jours avant la réunion afin que vous puissiez formuler d’éventuels amendements à discuter en AG.

Peut-être aurons-nous aussi à revisiter notre mode fonctionnement.

Le rapport financier : le bilan vos sera présenté en Réunion. La participation est plutôt à la hausse. Là aussi, nous ferons le point sur les aides distribuées. Il faudra réexaminer les principaux postes de dépenses et nos priorités.

Et si nous avons le temps,

2) La chasse aux sorcières :

Les relents xénophobes traînent dans les discours des prétendants au trône qui se traduisent par une espèce de dangereuse escalade où toutes les outrances semblent permises. Nous avions, début janvier, fait un point précis sur les familles menacées, la cadence s’accélère. Nous aborderons quelques nouvelles situations.

3) Questions diverses que nous prendrons le temps d’aborder.

Bien amicalement à toutes et tous.

Montoulieu le 19 Janvier 2022

Christian

