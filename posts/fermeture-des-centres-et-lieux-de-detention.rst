.. title: Fermeture des centres et lieux de détention
.. slug: fermeture-des-centres-et-lieux-de-detention
.. date: 2020-08-20 12:43:00 UTC+02:00
.. tags: 
.. category: communiqués
.. link: 
.. description: 
.. type: text

RESF 09 s'associe au communiqué de la Cimade et l'ADE pour demander la fermeture de tous les centres et lieux de rétention.

Le communiqué de la Cimade et de l'ADE :

Un cas de Covid a été déclaré jeudi au sein du CRA de Toulouse. La Cimade et l'ADE s'alarment
des risques encourus notamment par les personnes retenues et demandent la fermeture du
centre.

Une semaine après l'identification d'un cluster au CRA de Mesnil Amelot, le jeudi 13 août l'équipe
de la Cimade intervenant au CRA de Toulouse Cornebarrieu a été informée d'un cas positif de
Covid-19.

Dans le respect des consignes sanitaires, la Cimade a alors interrompu sa présence physique et
mis en place une permanence téléphonique pour répondre aux demandes des personnes
enfermées.

A ce jour il n'est pas possible de connaître l'étendue de la contagion, des tests sont en cours.
Cependant les inquiétudes sont fortes parmi les retenus, plusieurs questionnent la pertinence de
se faire tester si de nouvelles personnes potentiellement porteuses du virus entrent à nouveau au
CRA.

Le secteur femmes a été mis en quarantaine, les retenues doivent prendre leur repas dans leur
secteur. Pour les hommes, tout retenu sortant du secteur doit porter un masque et se laver les
mains. Les visites sont suspendues jusqu'à nouvel ordre.

Cependant, le maintien de plus de 40 personnes en état de promiscuité et d'enfermement
contrevient aux directives de l'OMS et fait peser un risque sanitaire sur les personnes retenues et
les différents personnels travaillant au CRA. Les conditions d'enfermement ne permettent pas de
respecter pleinement les gestes barrières et mettent en danger les personnes étrangères privées
de liberté en rétention administrative. « On mange à deux par table, il n'y a un seul briquet par
secteur et pas de gel hydroalcoolique à côté, on n'a pas de nouveaux masques... » déplorent
plusieurs retenus.

Les juges de la liberté et de la détention toulousains ont décidé de juger les personnes retenues
sans les présenter à l'audience. Après la visio-conférence et les audiences en catimini interdites
au public imposées durant prés de six mois, la crise sanitaire justifie aujourd'hui la mise en place
d'une parodie de justice alors même que la Cour de cassation a déjà censuré des décisions de
JLD prolongeant des rétentions administratives sans comparution des personnes concernées.

Dans ces conditions, le respect des droits des personnes retenues au CRA de Cornebarrieu
impose leur libération immédiate.

Et ce d'autant qu'à de très rares exceptions, les possibilités pour l'administration de reconduire
des personnes dans leurs pays d'origine sont inexistantes et la rétention n'a donc aucun sens.

Aussi, face à cette situation critique, la Cimade et l'ADE demandent la fermeture de tous les
centres et lieux de rétention, seul moyen de conjuguer la préservation des libertés fondamentales
des personnes retenues avec l'impératif constitutionnel de santé publique.

