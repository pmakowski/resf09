.. title: Compte rendu octobre 2021
.. slug: compte-rendu-octobre-2021
.. date: 2021-11-01 12:46:49 UTC+01:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 octobre 2021
.. type: text

Réunion mensuelle du 4 octobre (mairie de Foix)

Présidence : Yannick Garcia

Animation : Christian Morisse

**Soutien aux jeunes**

**Rentrée scolaire** : l'aide de RESF (50 euros par enfant scolarisé) est accordée aux familles en situation irrégulière et exclues à ce titre des allocations de la CAF. L'estimation de la dépense globale était de 5000 euros ; à l'heure actuelle, 3000 euros ont été versés. Aussi nous proposons un soutien aux étudiants en situation précaire (ils seraient 6 ou 7).

Côté Conseil Départemental, les conditions particulières d'octroi aux familles sont parcimonieuses et indirectes (achats ciblés).

**Mineurs** : 4 sont pris en charge par nos soins dans le cadre de leurs études (internat, hébergement pendant les weekend). A titre expérimental, l'EREA envisagerait d’attribuer (sur la base d'une convention et avec une petite participation) un ou plusieurs logements  de fonction vacants.

Par ailleurs, 17 jeunes sont devenus majeurs ces mois derniers et ont fait immédiatement l'objet d'une OQTF suite à leur demande d'un titre de séjour. RESF a bien sûr engagé des recours auprès du Tribunal Administratif : 4 ont été positifs, 1 perdu donc en appel à Bordeaux), les autres sont en attente d'une décision judiciaire.

A noter également que le Ministère de l’Intérieur a pris directement en mains les autorisations de travail, en lieu et place de la direction départementale du même nom. De plus, les demandes sont désormais informatisées et nécessitent un numéro d'étranger, faute de quoi la démarche est stoppée. Quant au contact téléphonique indiqué, il fait l'objet d'attente(s) interminable(s)...

Or, cette autorisation de travail est nécessaire pour l'inscription en apprentissage !

Tout ceci alors que de nombreux artisans manquent de main-d'oeuvre formée et motivée. La préfecture, elle, continue de contacter les employeurs pour les dissuader de prendre ces jeunes. Le CFA est intervenu, les chambres syndicales aussi, sans réponse.

**Nouveaux arrivants**

Depuis le début de l’année, nous avons enregistré peu de nouveaux arrivants comparativement aux années précédentes (Covid, dissuasion... ?). Mais l’essentiel des arrivées passe par la Plateforme régionale de Toulouse. Les dossiers ne nous parviennent souvent qu’à l’issue des demandes d’Asile, ou par les CADA basés en Ariège. Néanmoins, les procédures judiciaires sont en nette augmentation.

**Familles**

Les demandes d'Admission Exceptionnelle au Séjour (AES) sont le plus   souvent rejetées. Les quelques rares décisions positives se heurtent ensuite à des difficultés administratives parfois invraisemblables, notamment l’exigence de passeports alors que les Consulats sont perturbés (Mali, Soudan, Guinée).

**Afghanistan**

Pas de demande semble-t-il en l'Ariège à l'heure actuelle. Nous avions reçu en permanence quelques ressortissants déjà présents en France, inquiets, très inquiets pour leurs familles. Les ambassades des pays voisins font la « sourde oreille, y compris les ambassades de France (malgré les discours !)

**Permanences et formation**

Celles et ceux qui veulent se familiariser avec le code CESEDA  nouvelle   formule sont conviés à la mairie de Foix le 5 décembre à 15 h. sous la houlette de Christian*.

Quant à la prochaine réunion mensuelle, elle aura lieu le 8 novembre à 17 h 30.

Alain Lacoste secrétaire

Octobre 2021

Depuis la rédaction de ce compte-rendu, une seconde réunion de formation sur le même est programmée à Saint Girons le Lundi 15 Novembre, Salle de l’ancienne gare, à la demande d’Acarm et des collectifs du Couserans.
