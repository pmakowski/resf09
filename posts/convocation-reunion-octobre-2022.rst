.. title: Convocation réunion octobre 2022
.. slug: convocation-reunion-octobre-2022
.. date: 2022-09-27 16:00:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link:
.. description: RESF09 convocation réunion octobre 2022
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF.

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 3 octobre 2022  de 18H à 20H. - Salle F Soulié – mairie de Foix - FOIX

**La rentrée scolaire pour les mineurs isolés**

Tout d’abord 2 des 4 dossiers déposés chez la Juge des enfants ont été audiencés : un reconnu mineur, l’autre non. Nous attendons les audiences pour les deux autres.

En attendant une prise en charge effective, nous les avons accueillis et scolarisés. Nous avons assuré leur rentrée scolaire et les orientations professionnelles souhaitées. Nous avons pris en charge sept jeunes mais en avons « perdu » un qui semble préférer aller « travailler ». C’est son choix.

Nous avons fait le point sur les charges financières correspondantes et les moyens dont nous disposons pour 2022/2023. Nous pourrons encore envisager quelques nouvelles demandes.

**Les admissions exceptionnelles au séjour**

De nombreux jeunes (anciens MIE devenus majeurs) ont demandé un titre de séjour, les textes les y obligent. La plupart ont une formation professionnelle et des diplômes, la plupart ont un emploi. Deux sur trois se sont fait jeter. Réponse de la Préfecture : une Obligation de quitter le territoire (OQTF/IRTF). Que de procédures et quel gaspillage !

Pourtant, bonne nouvelle, ce brave Gideon, après toutes les misères subies et l’acharnement de l’administration a reçu un titre de séjour. On finirait par croire aux miracles… 

**Enfants « laissés pour compte de la CAF »**

Dans la limite de nos moyens, cette année encore, nous avons aidé financièrement les familles concernées… Nous nous appelons « Réseau Education Sans Frontière » l’école n’est-elle pas la première planche de salut pour ces gamins ballotés au gré des turpitudes administratives.

**Distribuer, c’est bien mais il faut aussi alimenter la caisse.**

Nous ferons un point financier précis de cette rentrée et des engagements pris pour l’année scolaire. Il semblerait que l’inflation refroidisse un peu les dons et cotisations… Les temps sont durs et les perspectives plutôt mauvaises.

**et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 27 septembre 2022

Christian
