.. title: Convocation réunion septembre 2020
.. slug: convocation-reunion-septembre-2020
.. date: 2020-09-02 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion septembre 2020
.. type: text

Bonjour,

Aucune salle n’étant libre le 7 sur FOIX, nous sommes obligés de déplacer la
réunion mensuelle au LUNDI 14 SEPTEMBRE 2020 de 17h. À 20h. Salle Jean Jaurès à la
Mairie. Nous proposons les points suivants :

1) Les mineurs isolés

- Compte-rendu de la rencontre avec le Conseil Départemental
- la requalification « mineurs » par la Juge (Ebad - Abdoullah et Ibrahim)
- les autres procédures en cours
- l'hébergement problématique pendant les weekends et les vacances pour deux
  jeunes (un sur Lavelanet, lycée Jacquard et un sur Pamiers à l'EREA)... si vous avez
  des disponibilités, faites-nous le savoir...)

2) Les permanences à Pamiers

- les conditions de mise en œuvre
- Le nombre limité de RDV et les priorités à définir

3) Les aides de rentrée

- le principe en a été retenu en Août mais comment l’applique-t-on ?
- La délibération « Massat » du Conseil Départemental, quel Accompagnement
  des demandes aux ADS ?

4) Obligations de Quitter le territoire (OQTF)

- les nouvelles et quelques anciens dossiers
- les audiences et les résultats
- les procédures d’appel à Bordeaux

5) La reprise des cours de F.L.E.

6) Et les questions que vous souhaitez voir aborder

rassemblement pour la Paix,
Marche nationale Septembre/Octobre pour les régularisations... par exemple...

Et il se fera tard et un masque pendant deux heures : c’est la galère.

Amicalement Christian

