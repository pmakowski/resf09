.. title: Compte rendu septembre 2023
.. slug: compte-rendu-septembre-2023
.. date: 2023-09-27 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 Septembre 2023
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse


**Bonnes nouvelles (ce n'est pas si fréquent)**

De « vieux » dossiers ont connu dernièrement une issue positive. D'autres sont semble t'il en bonne voie pour des familles volontaires et bien intégrées.

S'y ajoutent des régularisations en faveur de jeunes adultes formés avec notre soutien, notamment en apprentissage et que des patrons sont prêts à embaucher.

**ou moins bonnes**

La plate-forme nationale d'orientation et d’affectation géographique des demandeurs d'asile a été réactivée afin de désengorger les grandes villes. Cela se traduit par de nouvelles arrivées importantes dans des départements ruraux comme le nôtre...

**Rentrée scolaire**

L'aide que nous apportons pour les enfants aux familles n'ayant pas droit au soutien CAF a été prolongée.

Ces familles sont au nombre de 41 (chiffre stable) pour 94 enfants avec des montants allant de 50 euros en maternelle et primaire à 80 euros en secondaire et 100 à 200 en supérieur selon la situation personnelle.

A noter que le bilan de l'année scolaire 2022 - 2023 s'est traduit par de bons résultats aux examens et à l'entrée dans le monde professionnel.
Cette année encore, nous serons amenés à prendre en charge plusieurs internats.

**Dublin**

Cette appellation, qui concerne le retour des demandeurs d'asile dans le premier pays européen où ils ont été admis, fait l'objet de contestation(s) de notre part devant le Tribunal Administratif pour les personnes, notamment originaires de pays francophones.

Objectif : leur permettre d' effectuer une nouvelle demande d'asile en France.

**FLE (cours de Français)**

La situation diffère selon le lieu.

A Pamiers, de nouvelles familles ou personnes seules se manifestent régulièrement mais leur assiduité en dents de scie ne permet pas un travail et un suivi suffisants.

A Foix, le site du Courbet et l'apport de la CAF, en particulier par l'organisation en différents niveaux se traduisent par une bonne implication, féminine le plus souvent, une bonne ambiance générale ainsi que l'intégration des nouveaux et nouvelles participant(e)s.

**Finances de l'association**
Ces deux dernières années, les comptes de l’exercice annuel étaient en équilibre, voire un peu excédentaires. Les réserves permettent encore de faire face mais nous avons pris de nouveaux engagements auprès des mineurs/majeurs isolés.

Les participations (dont le montant est laissé à l'appréciation des intéressés) ainsi que des dons semblent marquer le pas.

Toutes les Associations se trouvent actuellement dans la même situation dans ce contexte d’inflation galopante… D'où un appel à un coup de pouce -Coluche disait un p'tit geste- qui serait le bienvenu.

A titre d'exemple, la charge annuelle des timbres fiscaux exigés pour l’ouverture de nombreux dossiers ainsi qu’à la remise des cartes de séjour, payantes évidemment, atteint des sommes très importantes.

Alors, merci par avance.

Le Secrétaire, Alain Lacoste

