.. title: Rapport d’activité  pour 2022 – Assemblée Générale statutaire
.. slug: rapport-activite-2022
.. date: 2023-01-09 08:33:49 UTC+01:00
.. tags: rapport d'activité
.. category: rapport d'activité 
.. link: 
.. description: Activités 2022
.. type: text

**Rapport d’activité  pour 2022 – Assemblée Générale statutaire**

Comparé aux deux années précédentes, 2022 nous a permis de retrouver un rythme de fonctionnement normal. Avec la tenue de 52 permanences (une par semaine le Vendredi matin) où nous sommes généralement entre 5 et 8 intervenants,  nous avons répondu présents dans toutes les situations qui nous ont été soumises. Le tableau statistique joint est un bon baromètre. 

Certes, les élections présidentielles et législatives ont ravivé toutes les velléités xénophobes et racistes. Nous avons entendu des propos dont la monstruosité n’a d’égale que la bêtise de leurs auteurs. Depuis, le pouvoir en place, plus spécialement le Ministère de l’intérieur, veut montrer qu’il est efficace et espère être le futur champion de cet électorat-là. Donc se succèdent des circulaires, des injonctions et des pratiques encore un peu plus violentes et acharnées avec les plus vulnérables, les « crève la faim », les SDF sous leurs tentes et les étrangers en situation précaire. 

Une fois encore, les administrations préfectorales, les forces de Police massivement mises à contribution, en petits robots bien dressés, ont été particulièrement zélées, particulièrement sourdes et opiniâtres, agissant souvent à la limite de la légalité. Les têtes qui dépassent n’ont pas leur place, une préfète vient d’en faire les frais. 

Le Télétravail et la dématérialisation facilitent  la tâche de l‘administration. C’est une véritable aubaine où on peut prendre un maximum de décisions inhumaines sans devoir être confrontés aux visages défaits des victimes… bien plus confortable que derrière une vitre… Les furieux de l’expulsion et du mépris des pauvres gens ont « carte blanche » et sont encouragés, par décrets et par circulaires, à « faire du chiffre »  souvent à la limite de la légalité. 

Sachant que les Préfectures ne sauraient s’emparer de tous les dossiers, il semblerait qu’elles aient choisi de « faire des exemples », de semer la peur et la violence institutionnelle, de montrer aux « chefs » qu’ils sont de bons petits soldats. 

Donc les mauvais coups ont continué de pleuvoir et nous avons dû faire face (voir tableau statistique ci-joint). 

**Toujours présents dans la durée**

Le Réseau vient d’achever sa neuvième année d’existence légale. Il est depuis plus de 20 ans, sous diverses formes, sur le terrain. Le réseau est un vrai réseau où les militants et les travailleurs sociaux des différents secteurs constituent un précieux relais. Les permanences  que nous tenons avec suffisamment d’intervenants sont un peu la plaque tournante des décisions à prendre. Nos activités n’ont pas changé d’orientation même si le juridique occupe beaucoup plus de place. La nouvelle nomenclature du code CESEDA (Code de l’Entrée et du Séjour des Etrangers et des Demandeurs d’Asile) édition 2022, nous a contraints à quelques révisions, réadaptations et formations. 

En 2022, Nous avons enregistré 124 contributions financières (cotisations et dons) pour un montant total de 22 737 € : des rentrées stables avec plus de cotisants. (7 634 € en 2016 - 12 460 € en 2017 – 15 470 € en 2018 - 17 656 en 2019 – 20 925 en 2020 – 22 255 € en 2021). Le nombre d’adhérents reste stable, environ 150, les couples participants étant difficiles à comptabiliser (1 ou 2 ?), certain-e-s donnant à plusieurs reprises sur une même année. La participation associative et syndicale est, elle aussi, stable. C’est rassurant. Globalement les comptes vont bien. Nous avions pensé pouvoir dépenser plus cette année, c’est chose faite bien malgré nous. La situation de quelques familles a entraîné des soutiens exceptionnels.


Les listes sympathisants sont difficiles à tenir à jour. Elles fonctionnent avec environ 1000 destinataires (adhérents, hébergements et sympathisants) mais nous avons trop de retours pour changements d’adresse ou de nouvelles situations non signalées. Il faudra en revoir l’utilité, l’élaboration et la gestion. Le site du réseau «resf.ariège.eu.org » est régulièrement pourvu (merci à notre spécialiste). Il mériterait lui aussi d’être mieux alimenté avec des rubriques et des infos plus larges.

L’essentiel de nos interventions s’adresse aux personnes d’origine étrangère, surtout aux familles avec enfants, qu’elles soient demandeuses d’asile, réfugiées, primo-arrivantes, en cours ou en fin de procédures. La mobilisation en faveur des mineurs/majeurs Isolés s’est largement développée ces dernières années.  

Le nombre d’entrées nouvelles dans le département semblerait relativement stable, voire en baisse. Même constat  pour les « Mineurs isolés ».

Les personnes en situation précaire ou irrégulière sont la cible d’une vague d’OQTF qui anticipe sur les contenus (à géométrie variable) d’une Xième Loi sur l’immigration en gestation. La circulaire de l’Intérieur en Novembre 2022 a fouetté les troupes. Les méthodes d’interpellation privilégient les weekends, les vacances scolaires et les jours de fête, l’administration espérant bien que notre vigilance soit en sommeil : RATE ! et les militants et les avocates sont là ! Même le Dimanche. 

Contrairement aux allégations des xénophobes, globalement, nous constatons dans le monde associatif un élan de solidarité assez général y compris à l’égard les populations « étrangères ».

Cette année encore, nous avons surtout eu à gérer des dossiers « anciens » réactivés par une Préfecture incitée et acharnée à faire « du nettoyage ».  Nous n’avions jamais été amenés à compléter autant de  dossiers d’AES (Admission Exceptionnelle au séjour) imposés par la Préfecture et suivis de refus de séjour quasi-systématiques, accompagnés d’OQTF (Obligation de Quitter le Territoire), d’IRTF (Interdiction de Retour sur le Territoire) et d’assignations à résidence. Tous les prétextes sont bons : Papiers d’Etat civil, passeports, non intégration, remise en cause des « paternités » et tests ADN imposés… Bref un arsenal qui pourrait se résumer à dire : Comment emmerder le monde ? 

Les familles d’origines géorgiennes, Arméniennes et Albanaises restent les plus nombreuses en Ariège. D’Afrique subsaharienne, nous accueillons surtout des migrants isolés, souvent mineurs et femmes seules avec enfants. C’est-à-dire généralement des familles ayant des jeunes enfants qu’il faut mettre à l’abri, souvent soigner, orienter vers de l’alphabétisation et une scolarisation urgente. 

Nous avons ouvert 117 nouveaux dossiers (en hausse sensible), soit environ 300 personnes nouvelles. . En fait, la file ouverte des situations a augmenté significativement par la réactivation préfectorale de dossiers anciens et les refus de renouvellement de titre de séjour (569 dossiers en RDV individuels ou familles). Les origines de plus en plus diversifiées posent des problèmes d’interprétariat oral et écrit mais nous sommes riches de nos différences et quand on ne sait pas faire, on fait faire et on paie….

Sur les 20 dernières années d’activité LDH/RESF, nous avons enregistré un total de 1 510 dossiers classés dans les archives, ce qui correspond à  environ 5 000 à 6000 personnes concernées dont plus d’une centaine de MIE/MNA… couvrant pratiquement tout le champ juridique du code CESEDA… Autant dire qu’il vaut mieux se tenir à jour. Une formation spécifique et permanente est nécessaire pour rester efficace et préparer la relève.

Nous tentons de ne rien laisser passer et de recourir systématiquement à la Justice. Les avocates sont là, disponibles et pertinentes. Chaque requête gagnée n’est pas seulement gagnée pour les intéressés mais constitue une victoire contre la politique xénophobe du pouvoir… donc une victoire politique aussi pour nous…

Les permanences hebdomadaires à Pamiers suffisent tout juste à répondre à la demande. L’équipe est rodée et de plus en plus efficace... Un grand merci à celles et ceux qui se déplacent chaque semaine, sachant qu’ils rentrent à la maison avec du « boulot » dans le sac… Nous savons aussi que nous pouvons nous appuyer  sur de bonnes relations et sur l’efficacité professionnelle de quelques avocates et avocats à qui nous demandons beaucoup. Nous avons rempli et déposé pas moins de 150 dossiers d’aide juridictionnelle cette année.

Les capacités d’accueil à domicile sont maintenues (Familles ou Mineurs Isolés)… La concentration de la demande sur l’axe Tarascon – Foix – Pamiers nous complique un peu les choses. Ces accueils  représentent un investissement militant important qui nécessite de mettre en commun les charges et les difficultés. Nous sommes en relation fréquente avec les lieux plus institutionnels assurant l’accueil (CADA, SAO, MECS, Accueils d’urgence, familles d’accueil, foyers, Emmaüs…)


**Quelques mots concernant notre fonctionnement**

Une réunion mensuelle du Réseau assure un minimum de prises collectives d’orientations et de décisions. D’abord destinée à informer, elle est devenue le lieu de rencontre des militants les plus actifs.  

Les permanences hebdomadaires fonctionnant à Pamiers constituent un lourd investissement militant. Une dizaine de camarades accueillent, sur rendez-vous, les personnes en demande. L’équipe, aujourd’hui pleinement fonctionnelle, semble bien stabilisée. De nouvelles venues « apprennent le métier ». Que faisons-nous ? 

* Nous assurons l’accueil des primo-arrivants et des demandes d’asile auprès de l’OFPRA, complétant les dossiers, assumant si besoin est, les traductions des récits d’exil, expliquant le déroulement des démarches futures, essayant d’orienter vers des hébergements, assurant la scolarisation des enfants s’il y a lieu. Le dispositif régional (SPADA) devant lequel tout demandeur d’asile doit se faire enregistrer assure un premier accueil et un accompagnement aux premières démarches. Cette année, l’essentiel des nouveaux venus sont arrivés dans le département parce qu’affectés sur les CADA. Nous n’avons eu que fort peu d’entrées directes.

* L’absence de dispositif officiel de domiciliation complique la tâche. Le siège social de RESF et de la Ligue domicilie de plus en plus de dossiers (Asile, Justice ou encore CPAM). C’est une grosse responsabilité qui consiste à réceptionner, redistribuer et souvent expliquer les courriers reçus.

* Nous assurons la rédaction des recours et des dossiers d’aide juridictionnelle auprès de la Cour Nationale du Droit d’Asile (CNDA). En phase de recours, nous enregistrons un peu moins de demandes, les personnels CADA et les avocats commis d’office auprès de la CNDA les assurant auprès des personnes hébergées (200 places). 

* Nous organisons la défense juridique, le plus souvent auprès du Tribunal Administratif et de la Cour Administrative d’Appel, des personnes ou familles menacées d’expulsion (Refus de séjour, OQTF, arrêtés de transfert Dublin et IRTF). Idem pour celles qui sont assignées à résidence ou enfermées au Centre de Rétention Administratif (CRA). Nous avons en 2022, dû faire face à plusieurs tentatives d’expulsion. Nous avons déjà parlé ici de la violence institutionnelle dont l’administration préfectorale fait preuve sans aucun respect de la famille, sans aucune considération humanitaire. 

* Nous transmettons aux avocats, souvent dans l’urgence (procédures à 48h. de préférence pendant les weekends), les dossiers préalablement constitués « à toutes fins utiles » ce qui implique de tenir à jour ces dossiers en «alerte», y compris les demandes d’aide juridictionnelle. Les délais de procédure sont désormais plus restreints. C’est un travail d’archivage, de correspondance écrite et téléphonique. C’est un travail d’anticipation. L’expérience nous a appris à envisager tous les possibles y compris les pires et les plus urgents.

* Nous sommes confrontés aux besoins de traductions… Celles que nous pouvons assurer nous-mêmes (plusieurs langues), celles que nous pouvons transcrire à partir d’une traduction orale, aidés d’intervenants de même origine culturelle, discrètes et compétentes (le plus fréquent). Nous commandons et payons aussi les travaux des traducteurs assermentés quand c’est indispensable mais c’est cher. 

* Nous avons constitué et rédigé de nombreux dossiers d’AES (Admission Exceptionnelle au Séjour) dits discrétionnaires donc au bon vouloir du Prince. C’est long, compliqué, ça demande un travail relationnel avec les intéressés, plusieurs rencontres préalables et des centaines de photocopies de documents déjà connus des services préfectoraux mais néanmoins exigés. 

* Nous avons pris en charge quelques dossiers de demande de naturalisation. Ils sont complexes et exigeants, aux multiples documents à fournir. La concentration administrative actuelle ; entièrement dématérialisée (malgré la Loi) rend souvent les procédures inaccessibles… Les RDV, eux aussi dématérialisés, sont quasi impossibles à obtenir ou portés à plusieurs mois. Les ratés se multiplient. 

* Nous devons prendre soin d’expliquer aux intéressés quelles sont les réglementations en vigueur et le fonctionnement des institutions. Nous devons expliquer qui nous sommes… que nous sommes solidaires, que nous sommes bénévoles et que nous ne pouvons pas, ne savons pas  « tout faire »…

* Les équipes interviennent sur l’apprentissage de la langue, l’une à Foix, l’autre à Pamiers. Cette activité fortement perturbée par la crise sanitaire a retrouvé son rythme. La fréquentation variable est composée prioritairement de personnes qui, du fait de leur absence de statut, n’accèdent pas au FLE institutionnalisé. Cette activité, surtout sur le plan relationnel, a l’air très appréciée.

* Les mineurs isolés, cette année encore, ont occupé beaucoup de temps et d’argent. Nous accompagnons les « rejetés du Conseil Départemental» afin qu’ils puissent avoir recours à la Justice, Juge des Enfants et Cour d’appel des mineurs. Les quelques requalifications obtenues sont venues rappeler que la législation existe. Encore faut-il que les intéressés puissent y accéder. 

* Avec la même discrétion et la même ténacité, nous avons accompagné presque tous les enfants des familles étrangères en situation précaire vers une scolarité salvatrice, assurant des aides à la rentrée scolaire  (refusées par la CAF et attribuées au compte-gouttes par le Conseil Départemental). 

L’immigration est plus que jamais au cœur des débats sociaux et politiques. Faire connaître au grand public nos expériences ne peut qu’assainir ces débats et remettre de l’humain face au déferlement de haine auquel nous assistons.     

Au-delà des « oublis » que vous nous pardonnerez… en quelques lignes une année d’activité bien remplie, forte d’une équipe consolidée, pleine d’espoir dans une période à venir qui s’annonce plutôt sombre…

**L’année 2022 avec quelques repères statistiques**

merci aux travailleurs sociaux qui ont servi de relai indispensable

Nous avons tenu 12 réunions mensuelles du Réseau à Foix

Nous avons tenu 2 rencontres de formation (associations amies)

Nous avons tenu 50 permanences à Pamiers (avec une moyenne de 11 RDV par séance)

Quelques visites à domicile (exceptionnelles)

Au cours desquelles nous avons reçu 569 « dossiers » (RDV individuels ou familles)

Dont 117 nouveaux dossiers pour 2022 (en hausse sensible)

Soit environ 250 dossiers « actifs » (certains sont venus deux ou trois fois).

Soit un total en archives de 1510 dossiers 

Nous avons contribué à 10 dossiers de 1ère demande d’asile à l’OFPRA (et traductions)*

Nous avons contribué à 7  dossiers de recours auprès de la CNDA*

Nous avons été confrontés à  5  classements Dublin 

Nous avons formulé 7 dossiers de réexamen 

Nous avons complété 22 dossiers de naturalisation et visas administratifs (hausse)

Nous avons contribué à établir 8 dossiers pour des titres de séjour « santé » 

Nous avons introduit 101 requêtes contre les OQTF – IRTF et refus de séjour au T.A.
suivies si nécessaire en Cour d’Appel administrative aujourd’hui  à Toulouse

Nous avons eu connaissance de 4 retours volontaires

Nous avons complété environ 150 demandes d’Aide Juridictionnelle (dont quelques préventives)

Nous avons introduit 24 requêtes sur  assignations à résidence et 7 en CRA  (à 48 h. de délai)
Ces requêtes exigent la constitution de  dossiers juridiques en amont 

Nous avons établi 16 dossiers de regroupement familial (mariages, divorces, rapprochements)

Nous avons soutenu, hébergé et financé la scolarisation de 8 Mineurs Isolés 

Nous avons accompagné financièrement 57 enfants et 3 étudiants pour la rentrée scolaire.

Nous avons (sur les finances de RESF) aidé ponctuellement de nombreuses familles  

Nous avons accompagné  4 mineurs isolés devant la Juge des enfants

Nous avons formulé 54 nouvelles demandes d’Admission Exceptionnelle au Séjour

Nous avons rédigé plusieurs courriers. La Préfecture n’a que peu reçu au guichet

Nous enregistrons 16 Régularisations adultes (toutes procédures confondues)
et 6 réfugiés statutaires (hors accueil des Ukrainiens)

Nous avons rédigé de nombreux courriers à l’extérieur (services sociaux, scolarité, justice)

Nous avons fait procéder à plusieurs traductions officielles (souvent plusieurs documents)

Nous avons assuré une cinquantaine de traductions orales et écrites (permanences et dossiers)

Les cours de FLE hebdomadaires à Pamiers et à Foix ont survécu au Covid 

Enfin nous avons enregistré sur le cahier une moyenne de 10 à 12 communications téléphoniques journalières (il n’y a ni fêtes, ni dimanches…) et de très nombreux documents par mail, nous permettant ainsi de faire face aux précipitations administratives

Et des kilomètres et des kilomètres… Merci à toutes celles et ceux qui roulent…

Il peut y avoir quelques oublis. Les relevés sont faits à partir des dossiers en cours et des cahiers.

Adopté à l’Assemblée Générale statutaire
Du Lundi 9 Janvier 2023 à FOIX
