.. title: Compte rendu décembre 2021
.. slug: compte-rendu-decembre-2021
.. date: 2021-12-06 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Décembre 2021
.. type: text

Réunion du 6 décembre 2021 (Maison des associations de Foix)

Présidence : Yannick Garcia

Animation : Christian Morisse

**Point relatif à Bertrand**

En France depuis 5 ans et travaillant dans un kebab, arrêté puis transféré au CRA de Perpignan après avoir fait l'objet d'une OQTF (à 48 h !), bien soutenu par un couple. RESF a comme d'habitude entrepris les démarches vis à vis du Tribunal Administratif (en appel) à Montpellier, encore en cours à ce jour..

**Mineurs**

On remarque une chute sensible du nombre d'arrivants.

Nous avons à l'heure actuelle trois nouveaux cas déclarés majeurs dont un quasiment analphabète et un dont la minorité sera difficile à démontrer.

Nous envisageons une scolarisation à l'EREA si des places sont disponibles.

Comme d'habitude subsiste pour les pensionnaires le problème d'hébergement lors des weekends et des vacances scolaires (appel aux bonnes volontés).

**OQTF concernant les MIE**

17 sont en cours dont 6 déjà traitées (3 cas perdus renvoyés en appel à Bordeaux par nos soins).

Administrativement, nous faisons face à un problème récurrent, celui des actes de naissance. Pour éviter leur remise en cause par la préfecture, on cherchait à obtenir une carte consulaire (voire un passeport) ce qui passe par le Consulat et donc un déplacement « à risque » lors des trajets vers Paris ou Lyon.

Or voilà que maintenant la préfecture argue du fait que le passeport est un titre de transport, pas une pièce d'identité ! Elle demande donc que les actes d’état civil soient légalisés, ce que nous tentons de contester juridiquement devant le T.A.

**Climat politique**

Tous les observateurs ont noté une dérive droitière très marquée et pour le moins inquiétante en ce début de campagne (Présidentielle et législative).

La date retenue pour la journée nationale des migrants (le 18/12) a été mal choisie par rapport aux vacances scolaires.

En Ariège, une conférence de presse regroupant plusieurs associations et syndicats aura lieu la veille à la ligue de l'enseignement.

**Pour ce qui concerne la LDH et RESF, quatre priorités se dégagent :**

le droit au travail pour tous ,

le respect de l'unité familiale ,

la fermeture des CRA,

des hébergements stables et dignes.


Le 6 décembre 2021

Le Secrétaire , Alain LACOSTE

