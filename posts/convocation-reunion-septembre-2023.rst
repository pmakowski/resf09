.. title: Convocation réunion septembre 2023
.. slug: convocation-reunion-septembre-2023
.. date: 2023-08-22 09:00:37 UTC+02:00
.. tags: convocation
.. category: convocation
.. link:
.. description: RESF09 convocation réunion septembre 2023
.. type: text


Bonjour, 

Nous  avons  le  plaisir  de  vous  rappeler  que  le  premier  Lundi  de  chaque  mois  nous  tenons  une  réunion  de  RESF, 
mais  pour  cause  de  fête  de  FOIX,  tout  est  en  congé  le  Lundi  4  septembre,  donc  :. 

**REUNION  MENSUELLE  du  Réseau  Education  Sans  Frontière** 

**Le  Lundi  11  septembre  2023  de  17h30  à  20H.** 

Salle  Jean  Jaurès  -  Mairie  de  FOIX

**Les  bonnes  et  les  mauvaises  nouvelles  du  mois**

Le  lot  quotidien  d'OQTF/IRTF,  de  refus  de  séjour,  de  tentatives  d'expulsion,  de  mise  en  rétention  et 
d’assignations  à  résidence  alimente  les  permanences  hebdomadaires  et  les  recours  juridiques  avec  plus  ou  moins  de 
succès.  Nous  reviendrons  sur  quelques  exemples  récents. 

**Penser  rentrée  scolaire  et  orientation**

Nous  avons,  en  Août  arrêté  les  modalités  concrètes  et  les  permanences  : 

à  voir `ici <https://resf.ariege.eu.org/posts/rentree-scolaire-2023-2024/>`_ et  faire circuler  l'information  autour  de  vous. 

**Des  mineurs/majeurs  pas  si  «  isolés  »**

Après  une  fin  d'année  scolaire  plutôt  satisfaisante  (de  nombreux  examens  réussis)  nous  mettons  en  place  les 
prises  en  charges  pour  2023/2024  et  les  accompagnements  nécessaires.  Nous  renouvelons  l'appel  aux  hébergements 
(weekend  et  vacances  scolaires). 

**La  reprise  des  cours  de  FLE  :**

Si  nous  pouvons  faire  un  peu  le  tour  des  besoins  et  des  moyens  disponibles  concernant  le  matériel,  les  locaux, 
les  lieux  et  les  intervenants. 

**Les  finances  :**

Dépenser,  c’est  bien  si  c'est  pour  la  bonne  cause,  et  les  occasions  ne  manquent  pas,  nous  sommes  vigilants. 
L'état  des  fonds  disponibles  n'est  pas  alarmant  mais  un  petit  coup  de  pouce  ferait  du  bien.  Pensez  à  votre  contribution 
si  ce  n'est  déjà  fait. 

**Questions  diverses  :**  qui  sont  les  vôtres. 


Bien amicalement à toutes et tous,

Christian
