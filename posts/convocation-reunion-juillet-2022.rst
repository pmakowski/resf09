.. title: Convocation réunion juillet 2022
.. slug: convocation-reunion-juillet-2022
.. date: 2022-06-26 09:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link:
.. description: RESF09 convocation réunion juillet 2022
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier lundi de chaque mois nous tenons une réunion  de RESF,

RÉUNION MENSUELLE

Du Réseau Éducation Sans Frontière

Le lundi 4 juillet 2022  de 17H30 à 20H.

Salle Soulié – Mairie de FOIX

L’ordre du jour sera chargé :

**1 – les mineurs isolés :**

Nous avons abordé le problème des accueils pendant les congés scolaires. Nous avons recensé quelques propositions insuffisamment pour l’instant. Si vous avez quelques possibilités d’hébergement prenez rapidement contact avec  Michèle (agostini.michele09@orange.fr) en précisant les dates et les capacités d’accueil. Merci

Nous ferons le point sur les OQTF/IRTF qui continuent de pleuvoir. Nous reviendrons tout particulièrement sur deux ou trois.

Nous ferons le point sur les scolarisations  qui s’achèvent et sur nos capacités pour la rentrée prochaine.

**2 – Fin de trêve hivernale et nouvelles menaces sur les familles :**

Plusieurs d’entre-elles ont reçu, comme l’an passé, après mise en demeure, des arrêtés d’assignation à résidence à l’hôtel, les obligeant à quitter leur hébergement, à quitter l’école pour les enfants et les privant de repas le midi. Ils ont  obligation d’aller signer au commissariat tous les matins.

Nous avons accompagné des dépôts de recours au Tribunal Administratif (avec succès pour quelques-uns)

**3 – Les femmes « parents d’enfant français »**

Nous reviendrons sur les quelques dossiers évoqués le mois dernier. Où en sommes-nous avec les projets de groupes de suivi ?

**4 – et les menaces que font peser les résultats des élections en matière d’immigration :**

Nous risquons d’assister à de « drôles » d’alliances tirant vers l’extrême Droite les propositions législatives. Les étrangers pourraient bien faire les frais des « compromis nécessaires »

**5 – et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 26 juin 2022

Christian
