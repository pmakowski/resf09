.. title: Compte rendu rencontre Conseil Départemental
.. slug: compte-rendu-rencontre-cd
.. date: 2021-09-01 14:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Compte rendu rencontre Conseil Départemental
.. type: text

Compte rendu de la rencontre avec le Conseil Départemental le mercredi 1er septembre 2021

Y ont participé

pour le CD Mmes Vilaplana (vice Présidente chargée des affaires sociales) et Séverin (Directrice de la solidarité - ASE)
 
pour RESF Christian Morisse et Alain Lacoste

**Difficultés accrues dues à la dégradation des relations avec la préfecture**

(situation constatée aussi dans les autres départements). Le constat est partagé avec le CD qui a « en conséquence » pris ses responsabilités par rapport au sort des jeunes majeurs, en complément du soutien réservé aux mineurs dans le cadre de l'Aide Sociale à l'Enfance (ASE).

Il est navrant de constater la pression (OQTF) mise par les autorités préfectorales sur les mineurs devenus majeurs, même en formation, ainsi que sur leurs employeurs.

**Fonctionnement du suivi des jeunes : Le parcours improbable vers un titre de séjour…**

Nous déplorons que, sur consigne du Ministère de l’intérieur, les Préfectures présentent une seconde fois au contrôle de la PAF (Police des frontières) les documents d’état civil du pays d’origine (acte de naissance et jugement supplétif). Comme par hasard le résultat est très souvent négatif alors que le premier contrôle était favorable… de là à penser que les consignes sont plus fortes que l’analyse…
Reste alors à tenter de se faire envoyer, depuis le pays d’origine, de nouveaux documents. Il faut s’organiser pour savoir qui opère ses demandes et pour garantir l’authenticité.

Le lien permanent et positif de RESF avec les diverses structures d'accueil est essentiel.

Le CD dit avoir refusé de valider « les procédures », accord dont se prévaut la préfecture concernant l’obtention d’un titre séjour à leur majorité.

Il est souhaitable que les référents ASE pour chaque mineur pris en charge, montent les dossiers de demande d’Admission Exceptionnelle au Séjour (AES) avec les intéressés.

Si, généralement les foyers prennent en charge ces démarches, le problème reste posé lorsqu’il s’agit de familles d’accueil ou familles parrainantes moins rompues à ce genre de démarches.

L’obligation de détenir un titre de séjour s’impose aux jeunes majeurs dans leur dix-huitième année. Faut-il anticiper et déposer la demande avant les 18 ans révolus ?        Les interventions intempestives de la Préfecture, auprès des employeurs en particulier, n’engagent pas à la confiance auprès de celle-ci qui, ainsi, est à l’origine de la rupture de nombreux contrats d’apprentissage ou de travail, ruinant tous les efforts mis en œuvre pour une bonne intégration, ruinant aussi tous les projets des employeurs.

**Fin de parcours (contrats jeunes majeurs)**

Face à la mainmise de la préfecture sur les autorisations de travail (l'avis de la direction du travail ayant été transférée au Ministère de l’Intérieur)), le CD assure la prise en charge des jeunes (contrat « jeune majeur ») jusqu'au terme de leur formation, le plus souvent en CAP, ce qui va fréquemment chaque année jusqu'à la fin août.

RESF souhaite que cette protection perdure lorsque des démarches juridiques devant le Tribunal Administratif s’imposent suite à l’avalanche d’OQTF que nous contestons désormais systématiquement.

Un échange nécessaire et fructueux face à une politique agressive et excluante du pouvoir en place…


Alain Lacoste  et Christian Morisse
