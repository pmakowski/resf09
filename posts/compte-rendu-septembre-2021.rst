.. title: Compte rendu septembre 2021
.. slug: compte-rendu-septembre-2021
.. date: 2021-09-01 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Septembre 2021
.. type: text

Réunion du 30 août 2021 (Mairie de Foix)

Yannick excusée

Animation : Christian Morisse

**Situation générale**

« Déblocage » en nombre d'OQTF (Obligation de Quitter le Territoire Français)... l'été aussi : 17 en ce moment pour les seuls mineurs isolés. Bien entendu, nous saisissons systématiquement le Tribunal Administratif pour tenter de casser ces décisions, obtenant des jugements assez souvent positifs. Il faut dire que la préfecture se contente fréquemment de « copier - coller » et non d’arguments personnalisés et étayés.

Ce caractère quasi mécanique des décisions préfectorales s'accompagne régulièrement d'éléments « inhumains » à caractère personnel ou familial.

De quoi nourrir un nouveau courrier à venir à la Préfète, sans illusion toutefois quant au résultat.

**Mineurs isolés**

A quelques rares exceptions près, tous les jeunes que nous suivons ont obtenu le CAP. Toutefois, quand ils arrivent à leur majorité, la Préfecture fait pression, y compris sur leur employeur, afin que ces derniers ne les conservent pas dans leur entreprise, ce qui ne leur permet pas de poursuivre leur insertion professionnelle. Cela se traduit là encore par de nouvelles OQTF (!) mais aussi par des articles dans la presse et surtout par des réactions croissantes et concertées d'employeurs responsables des diverses branches, qui écrivent à la préfète pour « regretter » cette situation. Courrier resté lettre morte pour l'instant mais qui nous donnera des arguments supplémentaires lors des audiences à venir.

Bien entendu, cette situation est semblable dans les autres départements.
S'y ajoute une nouvelle complication administrative avec la dématérialisation progressive des démarches concernant les autorisations de travail, nécessitant des inscriptions sur un site national (du Ministère de l’Intérieur !).

**Rencontre avec la vice Présidente du Conseil Départemental (chargée des affaires sociales) et la directrice de l'ASE (Aide Sociale à l'Enfance)**

Prévue le 1er septembre, cette nouvelle entrevue a pour but de faire un état des lieux concernant les jeunes (situation, papiers, démarches à effectuer) afin d'anticiper notamment les contacts avec la Juge Des Enfants.

Autre question : la fin des prises en charge, en particulier notre souhait de poursuite au cas par cas des Contrats Jeunes Majeurs, jusqu'à 21 ans si possible.

**Contact prochain avec les responsables du CISEL**

**Demande de l'ACARM (Couserans)** en direction de Christian pour animer une réunion de formation sur les diverses démarches administratives et juridiques opérées par RESF.

**Afghanistan**

Beaucoup de zones d'ombre et de questions en suspens. Le nombre de rapatriés afghans est et devrait rester faible du fait de choix personnels et familiaux orientés vers les pays voisins.

**SOS Méditerranée**

A noter la soirée initiée par l'Université Populaire du Pays de Foix le jeudi 23 septembre à 20 h. au centre universitaire avec projection et témoignages vécus.


Le 31 août 2021

Le Secrétaire , Alain LACOSTE

