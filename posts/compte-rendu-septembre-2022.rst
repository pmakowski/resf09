.. title: Compte rendu septembre 2022
.. slug: compte-rendu-septembre-2022
.. date: 2022-09-17 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 Septembre 2022
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse


Une fois n'est pas coutume, Yannick donne la parole aux participants pour évoquer tout d'abord des cas particuliers:

- une jeune Marocaine arrivant à Saint Girons et qui a été inscrite à Seix pour la rentrée. M. se charge d’une démarche pour qu'elle puisse reprendre le cours de ses études à Saint Girons
- une famille afghane installée dans le Couserans, en attendant la décision d’hébergement de l'OFII en tant que demandeurs d'asile, se trouvant isolée, a décidé de rejoindre un frère domicilié à Foix. Cette solution n'apparaît pas durable...
- D'autres familles vont être sans solution d’hébergement à court ou moyen terme.


**Nouveau projet de loi sur l'immigration**

Le texte est encore en préparation à l'échelon gouvernemental mais on peut s'attendre à un nouveau tour de vis du fait de la mainmise croissante du ministère de l'intérieur au détriment de la justice. RESF et la LDH doivent préparer un argumentaire sans tarder.

**Mineurs isolés**

Le Réseau reste très attentif à leur situation d'autant que la juge des enfants (rencontrée récemment) met un certain temps à statuer et s'appuie à nouveau sur les tests osseux dont on connait la fiabilité. Les décisions sont variables pour les jeunes en attente ; si elles sont positives, la prise en charge est assurée par l'aide sociale à l'enfance (service du Conseil Départemental) ; sinon, c'est RESF qui prend le relais.

A noter des résultats scolaires très positifs pour la plupart d'entre eux, mais l'administration préfectorale en tient peu compte.

En ce qui concerne le nombre de nouveaux arrivants sur le territoire, on constate une certaine décrue.

**Jeunes femmes avec enfants**

Nous en avons rencontré un certain nombre lors des permanences hebdomadaires (fréquemment issues de la prostitution) qui, lorsque le père déclarant est français, ont du mal à faire reconnaître la paternité pour leurs enfants. Pour ce qui est de leur hébergement, les lieux d'accueil semblent un peu plus accessibles que pour les hommes.

**Aide à la rentrée scolaire**

Les 50 euros par enfant octroyés par le réseau concerne une centaine d'enfants (40 familles) non bénéficiaires des aides officielles versées par la CAF. Reste à informer les familles pour que le maximum d'enfants en bénéficie.

**Finances de RESF**

Le trésorier constate des rentrées plus faibles que l'an dernier et lance un appel aux cotisants et donateurs afin de pouvoir assurer la continuité de nos actions.

Le Secrétaire, Alain Lacoste
