.. title: Convocation réunion octobre 2021
.. slug: convocation-reunion-octobre-2021
.. date: 2021-09-28 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion octobre 2021
.. type: text

Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF… Donc Octobre :

REUNION MENSUELLE Du Réseau Education Sans Frontière

Le LUNDI 4 OCTOBRE  2021 de 17H30 à 20H.

Salle Jean Jaurès – Mairie de FOIX

**La rentrée scolaire :**
 
Nous ferons le point sur les aides distribuées, éventuellement sur la prolongation de cette campagne. Il reste quelques moyens par rapport au budget prévu (ce qui n’est pas une raison pour gaspiller). Quelques étudiant-e-s n’ont pas accès aux bourses et auraient peut-être besoin d’un coup de pouce.  

Nous avons aussi pris en charge la scolarité de quelques « mineurs isolés » dont nous préciserons les situations administratives actuelles. Nous avons des difficultés avec les autorisations de travail pour les apprentissages. Dématérialisé, le service dépend désormais du Ministère de l’Intérieur… logique !

**La chasse aux sorcières :**

Les relents xénophobes traînent dans les discours des prétendants au trône. Ce qui se traduit par une espèce d’escalade où le Ministre de l’intérieur et « ses » administrations font du zèle… électoral. A nous de prendre les initiatives qui « les remettront à leur place. A nous de porter haut et fort les valeurs de la République de Fraternité et de solidarité, « je veux vivre, je veux mourir la porte ouverte disait Jean Ferrat »       

Ensuite nous pourrons faire un point sur la triste situation faite aux familles migrantes et les procédures disproportionnées commandées par le Sinistre Darmanin et ses complices.

**L’Afghanistan :**

Nous avons rencontré quelques ressortissants Afghans déjà en France avant les événements. Leurs inquiétudes pour la familles sont grandes, les moyens de venir en aide très limités. Les Ambassades européennes, hors Afghanistan, restent porte close. Le Ministère français des affaires étrangères reste lui, bien silencieux. « les bouffeurs d’étrangers hurlent à l’invasion inévitable. Nous essaierons de trouver quelques moyens d’exprimer notre solidarité au quotidien.

**Permanences et formation :**

Nous venons de recevoir (cadeau) le tout nouveau Code CESEDA - 2373 pages -  Rien d’évident pour le néophyte. Vous pouvez aussi le consulter sur :  legifrance.gouv.fr, encore faut-il en décrypter le jargon. Nous essaierons de vous familiariser lors de formations spécifiques avec les rubriques les plus usitées lors de nos interventions. Et nous avons l’aide précieuse d’avocat-e-s spécialisés.

**Questions diverses** ou autres, que nous prendrons le temps d’aborder. 

Bien amicalement à toutes et tous,

Montoulieu le 28 septembre 2021

Christian
