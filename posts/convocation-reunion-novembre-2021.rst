.. title: Convocation réunion novembre 2021
.. slug: convocation-reunion-novembre-2021
.. date: 2021-11-01 12:53:37 UTC+01:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion novembre 2021
.. type: text

Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF… 

Mais le premier lundi de Novembre est le 1er donc férié. Nous nous réunirons la semaine suivante.

REUNION MENSUELLE Du Réseau Education Sans Frontière

**Le LUNDI 8 novembre  2021 de 17H30 à 20H.**

Salle de Réunion Maison des Associations de FOIX (avenue de l’Ariège)

**Mineurs Isolés après la rentrée scolaire :**
 
Nous avons pris en charge la scolarité de quelques « mineurs isolés » dont nous assumons les charges d’internat et les frais de scolarité (plus un peu de menue monnaie pour les fantaisies). Nous aurions besoin de leur assurer un suivi plus rapproché, notamment pour les weekends et vacances. Nous avions parlé de « référents », où sont les volontaires ?

De nouveaux arrivants (sortie négative de l’évaluation au Conseil Départemental) font l’objet de nouvelles prises en charge. A leur demande, nous avons préparé, avec l’avocate, leurs requêtes auprès de la Juge des Enfants

**Quand demander une Admission exceptionnelle au séjour équivaut à demander sa propre expulsion** :

Qu’il s’agisse de personnes présentes depuis plusieurs années, de conjoints de Français ou de parents d’enfants français formulant une demande d’Admission exceptionnelle au Séjour (dont certaines sont « de Droit ») dans 90 % des cas la réponse préfectorale est mécanique : refus de séjour – obligation de quitter le territoire (OQTF), interdiction de retour de 12 mois (IRTF) quand le tout n’est pas agrémenté d’une assignation à résidence qui impose à l’ensemble de ces arrêtés un délai de recours de 48 h.. le vendredi de préférence. La France est un Etat de Droit… en bien mauvais état.       

Les voyages organisés vers les centres de rétention ont repris, Les Préfectures ont déjà oublié la pandémie Covid. Heureusement, nous avons le concours d’avocates sachant se rendre disponibles même le Dimanche et plusieurs de ces arrestations ne sont que des aller et retour et un gaspillage sans nom. Nous aborderons quelques exemples.

**Le point sur les cours de FLE (Pamiers et Foix)**

Il semblerait que la fréquentation soit très instable et que parfois nous ayons plus de formateurs que d’élèves. Faut-il envisager de varier les offres ? les Lieux ? à discuter.

**Permanences et formation** :
           
Nous venons de recevoir (cadeau) le tout nouveau Code CESEDA - 2373 pages -  Rien d’évident pour le néophyte. Vous pouvez aussi le consulter sur :  legifrance.gouv.fr, encore faut-il en décrypter le jargon. Nous essaierons de vous familiariser lors de formations spécifiques avec les rubriques les plus usitées lors de nos interventions :

A SAINT GIRONS  le Lundi 15 novembre de 9 h30 à 12h30 salle de l’ancienne gare (Av. A. Bergès)

A FOIX le 5 décembre de 15h à 17h30 (lieu à préciser) avant la réunion mensuelle de RESF.

**Questions diverses** ou autres, que nous prendrons le temps d’aborder.
 
Bien amicalement à toutes et tous,

Montoulieu le 1er novembre 2021

Christian
