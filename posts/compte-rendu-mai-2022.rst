.. title: Compte rendu mai 2022
.. slug: compte-rendu-mai-2022
.. date: 2022-05-31 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Mai 2022
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse

La période post Présidentielle ne nous permet pas de savoir quelle évolution attendre de la politique du gouvernement à venir en matière d'immigration et notamment de la loi Collomb). Reste que les OQTF continuent de « pleuvoir » voire de s'accélérer en lien le plus souvent avec les refus de renouvellement des cartes de séjour.

On constate aussi un traitement particulier vis à vis des réfugiés Ukrainiens alors que RESF ne fait aucune différence selon la provenance des déshérités !

**Mineurs isolés**

Nous en prenons sept en charge à l'heure actuelle (5 internes et 2 en demi-pension) pour leur scolarité, ce qui représente un montant non négligeable. Du fait de leur statut incertain, ile restent sous la menace de décisions Préfectorales et/ou judiciaires, la nouvelle juge des enfants s'appuyant de nouveau sur les tests osseux dont on connait pourtant la fiabilité...

Heureusement qu'ils sont scolarisés, sinon ils se retrouveraient certainement à la rue !

Pour autant, nous devons faire encore mieux pour leur insertion et envisager par exemple de leur faire découvrir nos modes de décision et d'organisation, en particulier sur le plan social.

Il convient aussi sans tarder de trouver des solutions d'hébergement pour les vacances scolaires. Christian propose à cette fin de constituer des groupes par secteur géographique : merci par avance aux bénévoles pouvant héberger un jeune (voire plusieurs) de le contacter.

**Difficultés particulières rencontrées par des femmes**

Il s'agit de mères d'enfants français éprouvant des difficultés pour faire renouveler leur carte de séjour, l'administration mettant en cause le manque de soutien des pères a priori français. Or les demandes administratives portent exclusivement sur les femmes et non leurs maris.

De plus, le passé de ces femmes et souvent trouble quand il n'est pas lié à la prostitution et elles se retrouvent souvent parent isolé, ce qui ne donne aucun droit notamment sur le plan social. Il convient donc de les informer sur leurs droits et devoirs élémentaires, à commencer par les jeunes (filles et garçons).

Mais il est très difficile de les inviter en réunion, leurs réticences culturelles étant nombreuses. Mieux vaut s'orienter vers des entretiens individuels dans le cadre de leurs structures habituelles (Institut protestant, CISEL, Emmaüs, centres sociaux...).

**Maison donnée à Pamiers (évoquée le mois dernier)**

Contact va être pris avec l'Apajh et les Pep.
