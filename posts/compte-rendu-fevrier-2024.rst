.. title: Compte rendu fevrier 2024
.. slug: compte-rendu-fevrier-2024
.. date: 2024-02-06 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 février 2024
.. type: text


**70ème  anniversaire  de  l’appel  de  l’Abbé  Pierre  :**

RDV  était  donné  pour  le  jeudi  1er  février  au  bric-à-brac  Emmaüs  de  Pamiers.  La  volonté  pour  les  bénévoles  de 
la  communauté  et  de  ses  responsables  était  de  marquer  l'anniversaire  de  l'appel  de  1954  de  l'Abbé  Pierre.  Ils 
ont  rendu  hommage  à  son  combat,  réinscrit  son  appel  dans  l'actualité  de  2024.  Ils  ont  également  rendu 
hommage  à  ceux  qui  poursuivent  son  engagement.  L'occasion  d'évoquer  les  morts  de  la  rue  et  des  routes 
migratoires.  L'occasion  également  de  présenter  l’action  de  SOS  Méditerranée.  Moment  dynamique,  émouvant 
et  chaleureux.  Plusieurs  d'entre  nous  étaient  présents.  Aucun  ne  l’a  regretté.  Le  samedi  une  vente  solidaire  au 
bric-à-brac  a  permis  de  recueillir  des  fonds  pour  soutenir  financièrement  l’action  de  SOS  Méditerranée  (pour 
info  1  journée  en  mer  =  30  000  €  !)  et  reverser  le  bénéfice  de  cette  vente  exceptionnelle  aux  associations 
caritatives  du  département  qui  s’attellent  quotidiennement  à  l’aide  aux  plus  démunis. 

**Secrétariat  :**

Changement  de  main  pour  le  secrétariat.  Alain,  souhaitant  prendre  du  recul,  la  fonction  devait  trouver  un 
remplaçant.  C'est  Francis  LAVERGNE  qui  prend  le  relai. 

**Loi  immigration  :**

Après  le  scandaleux  vote  à  l'Assemblée  Nationale  (accompagné  du  sourire  des  députés  RN)  une  étape 
importante  devenait  l'avis  du  Conseil  Constitutionnel.  Bilan :  si  celui-ci  a  censuré  pour  motif  de  forme  32 
articles  (ceux  déclarés  sans  lien  suffisant  avec  le  texte  initial)  et  3  articles  sur  le  fond  (en  partie  ou  en  entier), 
les  articles  «  qui  restent  »  accentuent  souvent  de  manière  «  sérieuse  »  les  textes  déjà  utilisés. 
Christian  M.  nous  a  présenté  dans  le  détail  les  nouvelles  dispositions.  (scandaleusement  nombreuses). 
Ce  compte-rendu  de  réunion  ne  détaille  pas,  volontairement, les  changements  et  leurs  conséquences  par  souci 
de  concision.

Suite  à  l'émotion  suscitée  par  cette  Loi  «  scélérate  »,  19  partis,  syndicats  et  associations  d'Ariège  ont  élaboré 
un  projet  de  communiqué  pour  présenter  pourquoi  ils  critiquent  cette  loi  et  pourquoi  ils  s'accordent  sur  une 
position  commune.  Ce  projet  de  texte  (dont  RESF09  est  signataire)  est  aujourd’hui  signé. 

Rappel  :  Pour  approfondir  ou  actualiser  nos  connaissances  sur  ce  vaste  et  complexe  sujet,  la  LDH  a  mis  sur  pied 
un  cycle  de  formation  de  6  séances  de  2  h  (18h  /  20  h)  entre  le  14/02  et  le  22/05.  Le  nombre  d'intention  de 
participation  est  (à  ce  jour)  de  20/25  personnes,  parmi  lesquelles  des  professionnels  missionnés  sur  le  sujet

**Hébergement  d'urgence  :**

Devant  les  difficultés  constatées,  il  est  envisagé  de  solliciter  une  audience  au  Préfet  sur  la  base  de  l’article  67 
de  la  Loi  immigration  (article  «  retoqué  »  par  le  Conseil  Constitutionnel) 

**Mineurs  isolés  :**

19  jeunes  viennent  d'arriver.  Si  10  ont  trouvé  un  accueil  aux  Pupilles  à  Reims,  les  9  autres  se  retrouvent  à 
l'hôtel. 5  sont  candidats  à  la  scolarité.  Une  démarche  vers  l’ÉRÉA  doit  être  menée.  Combien  pourraient  aller 
en  PPRP  (Parcours  Progressif  de  Réussite  Personnalisée)  …  mais  l’internat  est  couteux.  Combien  pourraient 
rejoindre  la  MLSD  (Mission  de  Lutte  contre  le  Décrochage  Scolaire)  solution  moins  couteuse,  mais  imposant 
d’autres  contraintes.  Une  démarche  est  à  engager  auprès  du  CIO  (Centre  d'Information  et  d'Orientation)  pour 
évaluer  les  niveaux  scolaires  et  évaluer  les  possibilités  de  scolarisation  dans  un  parcours  «  ordinaire  » 
Compte  tenu  du  nombre  et  de  la  complexité  de  ces  situations,  il  nous  faut  réfléchir  à  la  nécessité  d'ouvrir  un 
guichet  spécial  «  Jeunes  »  aux  permanences  du  vendredi. 


Le secrétaire, Francis  L. 



