.. title: Convocation réunion décembre 2022
.. slug: convocation-reunion-decembre-2022
.. date: 2022-11-28 16:00:37 UTC+02:00
.. tags: convocation
.. category: convocation
.. link:
.. description: RESF09 convocation réunion décembre 2022
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier lundi de chaque mois nous tenons une réunion de RESF.

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 5 décembre 2022 de 18H à 20H. - Salle F. Soulié – mairie de Foix - FOIX

**Darmanin : Le petit ministre aux méchants projets**

Nous joignons à ce courrier le texte de la circulaire que ce « Monsieur » a adressé aux Préfets en date du 17 
novembre dernier. Il donne les ordres et les instructions, il ordonne la méthode et rend personnellement responsables 
ses exécutants. On ne saurait être plus clair quant aux rapports hiérarchiques de l'apprenti Président ! 

Au-delà du bonhomme, le texte de cette circulaire énonce très clairement les intentions politiques et viennent 
éclairer de son vrai jour, les tergiversations et « baratins » autour d’un projet de Loi et d’un grand débat sur l'immigration 
dont on a déjà le mode d'emploi.

Nous prendrons le temps de décortiquer « ces ordres » pour mieux désobéir et préparer les ripostes en anticipant encore un peu plus. 

**Les mineurs isolés**

Toujours pas d'audience chez la Juge des enfants pour deux d'entre eux. Pourtant, à notre connaissance, le 
Ministre de l'Intérieur n'est pas encore Ministre de la Justice. Ce serait une histoire d'encombrement de l'institution. Les 
jeunes concernés ont plutôt tendance à souffrir d'encombrement des bronches. Dehors il fait de plus en plus froid.

Heureusement qu'ils ne sont pas complètement dehors et qu'ils sont scolarisés tous deux à Pamiers. 

**L'hébergement en période hivernale**

Nous reviendrons sur les mesures annoncées et les repositionnements de quelques familles. De nouvelles places 
« grand froid » ont été créées, suffiront-elles ? Oui, si la circulaire aux Préfets jette à la rue tous les pourchassés, victimes 
d'OQTF et autres « douceurs de Noël ». Pensez à recenser autour de vous les situations difficiles et faites remonter.

**Quelques situations alarmantes : au cas par cas.**

Tant pour les conséquences subies par ces familles ou ces personnes que pour l'exemple. Chacune de ces 
situations est un cas d'école dont les militantes et militants font l'expérience et renforcent leurs capacités à venir en aide.

**Bilan de l’exercice 2022**

Nous vous proposons de tenir l'AG statutaire de RESF 09 lors de la rencontre de Janvier (éventuellement le 9 
janvier sous réserve). Nous présenterons le rapport d'activité et le rapport financier et essaierons de proposer des 
améliorations toujours possibles à nos orientations et à notre fonctionnement. 

Les comptes seront clos le 31 décembre 2022... Vous avez encore quelques jours pour figurer sur la liste fort 
sympathique des adhérents/donateurs. Nous n'envoyons aucune relance individuelle. Chacune, chacun, fait ce qu'il peut.

**et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 28 novembre 2022

Christian
