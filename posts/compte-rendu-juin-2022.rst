.. title: Compte rendu juin 2022
.. slug: compte-rendu-juin-2022
.. date: 2022-06-09 10:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 Juin 2022
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse

**Questions diverses**

Film à l'Estive le 11/06 sur la question des réfugiés

Arrivée d'une nouvelle adhérente (prof. à l'EREA)

Le 16/06 à Lavelanet : projection-débat de « L'empire du silence » à 20 h 30

**Mineurs isolés**

L'acharnement de la Préfecture à leur égard ne se démentit pas, y compris pour ceux qui sont en apprentissage ou en stage et auxquels les employeurs sont très attachés compte tenu notamment de leurs difficultés à trouver des salariés formés et volontaires. Bien entendu, RESF intervient chaque fois auprès du Tribunal Administratif avec le concours précieux de nos partenaires avocat(e)s pour obtenir la suspension des mesures administratives.

Bonne nouvelle en contrepoint pour  Sahim (régularisation) et qui, CAP en poche trouve un emploi de cuisinier en CDI. Enfin, Mamadou, victime d’un très grave accident du travail, outre l'amélioration de son état de santé (quoique toujours handicapé) vient d'obtenir un titre de séjour.

En ce qui concerne la nouvelle Juge des enfants, on constate qu'elle fait traîner voire « oublie » les audiences demandées pour certains jeunes (4 sont en attente depuis 6 mois). Or la protection des mineurs est de son ressort et de sa responsabilité. Attend-elle qu'ils deviennent majeurs ? Qui plus est, elle remet à l'ordre du jour les « fameux » tests osseux dont on connait le grave manque de fiabilité.

**Affaire Gidéon :**

retenu à plusieurs reprises au CRA de Cornebarrieu suite à son refus d'embarquer (ce qui lui a valu la deuxième fois 3 mois de prison avec sursis) le juge des libertés a décidé d'annuler l'assignation à résidence, mais la Préfecture a « évidemment » fait appel et nous attendons donc un nouveau jugement. Un rendez-vous a eu lieu dernièrement à la Préfecture, il s'est avéré peu concluant. L'administration est restée sur sa position invoquant des résultats scolaires jugés insuffisants, des difficultés relationnelles dans son établissement ainsi qu'une mise en  accusation non suivie d'effets par la justice. Notons à ce propos (et bien plus largement) l'effet néfaste des réseaux sociaux.

**Hébergement des jeunes pendant les vacances**

Le récent courrier électronique envoyé aux bonnes volontés habituelles n'a pas permis de répondre à la demande (8 jeunes pendant 8 semaines). Un point d'ensemble va donc être refait pour reconstituer l'offre.

**Fin de la trêve hivernale**

Cinq familles en situation irrégulière sont concernées et risquent de se retrouver à la rue. La préfecture attendrait des éléments nouveaux avant de statuer. Mais on sait (cf. l'année dernière) que la même situation a abouti à des OQTF systématiques...

RESF s'est bien sûr mobilisé pour contester chaque décision défavorable et continuera de le faire.

**Mamans d'enfants français**

Sept dossiers sont en cours demandant un renouvellement de titres de séjour. La préfecture bloque et remet désormais en cause la véracité de la paternité. D'où enquête(s) judiciaire(s) et tests ADN.

En attendant, les mamans concernées sont en situation « irrégulière », perdent leur travail, n'ont plus aucune aide et se retrouvent dans la plus grande précarité.

**Accueil défaillant des étrangers en préfecture(s)**

Du fait de rendez-vous à géométrie variable et de la dématérialisation croissante des démarches, chacun comprend qu'il est de plus en plus difficile de se faire entendre pour les étrangers, a fortiori pour les non francophones.

Heureusement, le Conseil d'Etat vient de rendre une décision requérant une méthode de substitution devant garantir leur accueil dans des conditions correctes.

Même si le service étrangers des préfectures semble peu attractif côté employés...

Le Secrétaire, Alain Lacoste
