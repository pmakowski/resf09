.. title: Compte rendu décembre 2023
.. slug: compte-rendu-decembre-2023
.. date: 2023-12-15 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 décembre 2023
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse


**Questions diverses**

Information sur la marche aux flambeaux organisée à Foix le 18 décembre à partir de 18 h (rendez-vous sous la halle)

**Période hivernale**

Rappel : le code des familles précise que toute personne sans abri doit avoir accès à un hébergement d'urgence, a fortiori en hiver. Celui-ci existe bien au travers le 115... mais ne suffit pas à loger tous les nécessiteux.

De plus, leur santé physique et mentale n'est pas pleinement suivie ni garantie car la situation a évolué négativement avec un public majoritairement étranger sans papiers et maîtrisant mal notre langue.
Par ailleurs, au 115, le système de mise à l'abri est devenu tournant, instituant pour les demandeurs un rythme de trois jours dedans, puis dehors et retour éventuel si des places sont disponibles, l’accès incluant des critères de priorités.

Plus grave encore, la décision non écrite mais bien appliquée du ministère de l'intérieur : pas de papiers, pas d'hébergement ! Mesure destinée bien sûr à mettre la pression sur les intéressés pour les inciter à quitter le territoire.

D'où des personnes, dont des mineurs et des familles, parfois avec enfants, n'ayant pas d'hébergement pérenne ou pas d’hébergement du tout.

Heureusement, la solidarité existe encore pour assurer un accueil au coup par coup, mais elle a des limites, aussi bien du côté des bénévoles que des associations.

Ce climat conduit RESF -en lien avec Emmaüs- à demander une audience au Préfet, qui serait suivie d'un point presse afin d'informer nos concitoyens sur cette situation bien peu glorieuse …

**Projet de loi immigration**

Après le vote par le Sénat d'un texte très régressif, le débat a commencé à l'Assemblée Nationale de 11 décembre.

Deux articles revêtent une importance particulière : l'AME (Aide Médicale d'Etat) et les titres de séjour qui pourraient être accordés pour les métiers en tension.

**Aspects financiers**

Les six jeunes scolarisés à l'EREA que nous prenons en charge (en internat ou demi pension) pèsent sensiblement sur nos ressources, entre 500 et 600 € par trimestre chacun. S'y rajoute la difficulté récurrente de l'hébergement pendant les vacances scolaires et les weekends. Une fois encore, un appel est lancé aux bonnes volontés (contacter Christian).

**Droits des femmes, violences intra familiales, aspects culturels**

Un travail a été entrepris à Foix (Le Courbet) avec un groupe de femmes afin d'améliorer leurs connaissances du droit et de limiter les risques en la matière.

Objectif : leur permettre d' effectuer une nouvelle demande d'asile en France.

**Information du grand public**

Une large majorité de nos concitoyens ignore la réalité, le vécu en matière d'immigration (droit d'asile, mineurs isolés, regroupement familial, OQTF, etc...).

Christian va proposer et programmer une formation pour les personnes intéressées à partir de janvier, le vendredi à Foix de 18 à 20 heures.


Le Secrétaire, Alain Lacoste

