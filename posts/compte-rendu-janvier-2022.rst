.. title: Compte rendu janvier 2022
.. slug: compte-rendu-janvier-2022
.. date: 2022-01-06 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Janvier 2022
.. type: text

Réunion du 3 janvier 2022 (Mairie de Foix)

Présidence : Yannick Garcia

Animation : Christian Morisse

Yannick remercie les présents et leur souhaite ses voeux amicaux.

Christian revient sur la récente disparition de Gérard Bérail. Un hommage lui sera rendu prochainement à l'initiative des associations dans lesquelles il s'était impliqué souvent depuis longtemps. Les modalités seront précisées prochainement en fonction des conditions sanitaires.

**Période de campagne(s) électorale(s)**

RESF restera bien sûr dans le cadre de ses objectifs et actions permanents. Notre réponse aux sollicitations éventuelles sera donc très limitée et les membres de RESF ne s'exprimeront politiquement, s'ils le souhaitent, qu'à titre personnel.

**Journée des migrants (le 18 décembre dernier)**

La manifestation organisée à Saint Girons est apparue à la lecture de la presse comme « pilotée » par le député de la circonscription, ce qui pose question(s).

Un sérieux problème s'est également fait jour suite à la rédaction ubuesque d'un article dans La Dépêche consécutif à la conférence de presse du 17/12.

Le responsable de la rédaction départementale, contacté, s'est excusé, arguant du fait que les articles ne font plus l'objet d'une quelconque relecture.

On reste (presque) sans voix !

**« Urgences »**

Pas moins de 14 OQTF ont dégringolé de la préfecture en fin d'année, la plupart avec un délai de 15 jours que nous avons mis à profit pour saisir le Tribunal Administratif afin de tenter de les faire casser.

Cette multiplication n'a bien sûr rien d'hasardeux car elle complique nos démarches en cette période chargée pour beaucoup.

**Suite de l'affaire Bertrand**

Après l'épisode du CRA (Centre de rétention), son employeur est convoqué devant le tribunal pour l'avoir salarié en dehors des règles, Bertrand n’ayant de titre de séjour n’aurait pas le droit de travailler. Christian va prendre contact avec l'avocat pour préparer sa défense.

**A. G statutaire de RESF**

Après un rapide tour de table, la date est fixée au lundi 7 février 2022 (17 h 30) à Foix dans une salle qui reste à confirmer ; des précisions suivront dans un prochain message.


Le 6 janvier 2021

Le Secrétaire , Alain LACOSTE

