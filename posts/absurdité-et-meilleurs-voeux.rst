.. title: Absurdité et meilleurs vœux
.. slug: absurdité-et-meilleurs-vœux
.. date: 2020-12-26 08:55:00 UTC+01:00
.. tags: vœux
.. category: 
.. link: 
.. description: 
.. type: text


Dans la grisaille de la conjoncture sanitaire, humanitaire, politique… ce n’est pas difficile de formuler des vœux… A défaut de « meilleurs vœux », des vœux pour des jours meilleurs… Si le seul droit qu’ils nous laissent est celui de rêver… On peut toujours rêver.

* Rêver… Que les hommes (les femmes) naissent libres et égaux en droit.
* Rêver… qu’ils (elles) ont droit de manger à leur faim, d’être à l’abri, en bonne santé.
* Rêver… qu’ils (elles) ont droit de circuler et de vivre librement sur cette planète.
* Rêver… qu’ils (elles) prennent le plus grand soin de cette planète.

Poursuivez cette liste autant que vous le souhaiterez… Le rêve est encore gratuit ! Alors meilleurs vœux de rêves.

Nous bouclons l’année 2020 en beauté et décernons le prix **« des actes administratifs inqualifiables** (le mot est commode et évite les grossièretés) » à la Préfecture de l’Ariège pour cette d’année.

Aux expulsions programmées les 12, 26 et 30 décembre, sûrement dans le souci d’offrir à ces gens, en guise de cadeau, un voyage gratuit et des fêtes en famille au « pays natal » il convient d’ajouter des petits séjours en CRA (ne pas confondre les Centres de Rencontres Adorables - en rose sur la carte - avec les Centres de Rétention Administrative en gris sur la même carte de cette douce France qui vous flanque son pied aux fesses).

**Donc le premier prix d’acte administratif « inqualifiable » est décerné à celles et ceux qui ont proposé, décidé et exécuté l’envoi en rétention au CRA de Marseille d’un dénommé Abdoulaye. Que l’on juge de la virtuosité :**

Tout d’abord **le 23 décembre** la Gendarmerie est envoyée au domicile du couple afin de vérifier que le mariage n’est pas blanc (la mariée était en blanc !) donc procède aux inquisitions d’usage (humiliantes) puis force de constater que mariage et couple il y a, et faute… de faute… décide d’embarquer le Bonhomme et de le jeter en garde à vue.

**Le 24 décembre dès l’aube, embauche à 8h30**, interviennent nos valeureux gagnants du concours  qui à la question : *« que fait-on chef ? »* répondent d’une seule voix :  *« Nous avons réservé une place pour Abdoulaye au CRA de Marseille… Prenez la petite auto de la gendarmerie, passez-lui les menottes* (ils n’ont pas pensé à la muselière) *et en route pour Marseille »* où ils arrivent vers 16 h.  Un coup de fil a prévenu l’épouse qu’elle pouvait revendre les huîtres du réveillon sur le bon coin.

**Mais le 24 décembre, nous sommes aussi en fonction** et conjointement à Madame B. et à l’avocate, nous faisons suivre le dossier juridique au CRA de Marseille afin de permettre la saisine du Juge des Libertés et à l’avocat de permanence d’assurer la défense d’Abdoulaye.

**Le 25 Décembre, le Juge es Libertés,** estimant qu’Abdoulaye n’a rien d’un dangereux terroriste, considérant qu’il a une épouse et un domicile à Saverdun, qu’il n’a jamais eu l’intention de se soustraire aux petits plaisirs de la Préfecture, considérant… bref… décide que l’intéressé aurait pu rester chez lui et l’y renvoie.

Mais là, plus de taxi « bleu », la troupe s’est retirée et Abdoulaye se retrouve sans argent, sur le quai de la Gare St Charles… il est rentré à Saverdun.

**Conclusion :** un 1er prix bien mérité et vous pouvez remplacer le terme « inqualifiable » par un mot à votre convenance !

**Remarque subsidiaire :**  Au-delà de la violence intentionnelle gratuite, la plaisanterie coûtera fort cher au contribuable. Faites l’addition : les heures d’enquête et de garde à vue + 24 Heures/service de gendarme (charges comprises) + 1200 Kilomètres d’Auto et d’autoroute + les heures d’avocat et de tribunal = Vous ne vous en sortirez à moins de 2000 €… Faites vos comptes et passez à la caisse.

On n’arrête pas le progrès
Tous nos vœux pour des jours meilleurs

Le 26 décembre 2020
Christian Morisse



**Post scriptum :** *n’entendez-vous pas au-dessus de vos têtes le vol sourd et matinal d’un airbus A 320 emportant vers les cieux radieux de la Géorgie des gens qui ne souhaitaient pas y aller… Des vœux de Liberté !*
