.. title: Rapport d’activité  pour 2020 – Assemblée Générale statutaire
.. slug: rapport-activite-2020
.. date: 2020-12-31 08:33:49 UTC+01:00
.. tags: rapport d'activité
.. category: rapport d'activité 
.. link: 
.. description: Activités 2020
.. type: text

Une année bien difficile vient de s’écouler. Malgré la paralysie un peu générale de la société, cette dernière n’a pas oublié de profiter de la situation pour être encore un plus violente avec les plus vulnérables, les « crève la faim », les SDF sous leurs tentes et les étrangers en situation précaire.

Le Télétravail facilite la tâche de l‘administration. C’est une véritable aubaine où on peut prendre un maximum de décisions inhumaines sans devoir être confrontés aux visages défaits des victimes… bien plus confortable que derrière une vitre…  De Collomb à Castaner ou Darmanin, nous avons affaire à des furieux de l’expulsion et du mépris des pauvres gens n’ayant même plus besoin de s’embarrasser des parlementaires…  par décret et par circulaire c’est tellement plus simple d’imposer…

Donc les mauvais coups ont continué de pleuvoir et nous avons dû faire face. Certes c’était plus compliqué mais nous nous en sommes plutôt bien acquittés (voir tableau statistique ci-joint). 
           
**Toujours présents dans la durée**

Le Réseau vient d’achever sa septième année d’existence légale. Il est depuis plus de 20 ans, sous diverses formes, sur le terrain. Certes, pandémie oblige, nous avons dû adapter notre fonctionnement. Les rencontres mensuelles n’ont pas pu se tenir régulièrement. Il fut donc plus difficile de garder des liens étroits entre nous et avec « le terrain ». Heureusement le réseau est un vrai réseau et les travailleurs sociaux des différents secteurs ont constitué un précieux relai. Le centre décisionnel s’est de fait appuyé sur les permanences  que nous avons réussi à maintenir avec 5 ou 6 intervenants (à tour de rôle). Nos activités n’ont pas changé d’orientation même si le juridique occupe beaucoup plus de place.  .

Nous avons enregistré 142 contributions financières (cotisations et dons) pour un montant total de 20 925 € entre le 1er Janvier et le 31 décembre 2020 en progression sensible (7 634 € en 2016 - 12 460 € en 2017 – 15 470 € en 2018 - 17 656 en 2019). Nous comptons un peu plus d’adhérents, environ 170, les couples participants étant difficiles à comptabiliser (1 ou 2 ?), certain-e-s donnant à plusieurs reprises sur une même année. la participation associative et syndicale est en hausse. C’est rassurant. Nous verrons dans les commentaires du rapport financier que là aussi, l’année est spéciale. Globalement les comptes vont bien.

Les listes sympathisants fonctionnent avec environ 1000 destinataires (adhérents, hébergements et sympathisants). Le site du réseau «resf.ariège.eu.org » est régulièrement pourvu (merci à notre spécialiste) 

L’essentiel de nos interventions s’adresse aux personnes d’origine étrangère, surtout aux familles avec enfants, qu’elles soient demandeuses d’asile, réfugiées, primo-arrivantes, en cours ou en fin de procédures. La mobilisation en faveur des mineurs/majeurs Isolés s’est largement développée ces quatre  dernières années. Nous y reviendrons. 

Là aussi l’année est  exceptionnelle. Peu d’entrées nouvelles et pour cause, les frontières ont été fermées pendant près de six mois. Les personnes en situation précaire ou irrégulière ont été maintenues jusqu’à fin juin sur les hébergements de « période hivernale » et les associations humanitaires ont trouvé des moyens de fonctionner en permanence, merci à elles aussi.

Globalement, nous constatons dans le monde associatif un élan de solidarité assez général dont les populations « étrangères » ne sont pas tenues à l’écart, heureusement.

Nous avons surtout eu à gérer des « dossiers anciens » violemment réactivés par une Préfecture acharnée à faire « du nettoyage » n’ayant sans doute rien d’autre à faire, ignorante de la solidarité ambiante et saisissant l‘opportunité du confinement généralisé. Nous n’avions jamais enregistré autant de refus de séjour accompagnés d’OQTF (Obligation de Quitter le Territoire), d’IRTF (Interdiction de Retour sur le Territoire) et d’assignations à résidence. 

Les familles d’origines géorgiennes, Arméniennes et Albanaises restent les plus nombreuses en Ariège. D’Afrique subsaharienne nous accueillons surtout des migrants isolés, souvent mineurs et femmes seules avec enfants. C’est à dire généralement des familles ayant des jeunes enfants qu’il faut mettre à l’abri, souvent soigner, orienter vers de l’alphabétisation et une scolarisation urgente. 

Nous avons ouvert 68 nouveaux dossiers, soit environ 250 personnes nouvelles. De plus en plus de primo-arrivants sont malades, voire lourdement handicapés, ce qui implique un accompagnement spécifique. En fait, la file ouverte des situations a augmenté significativement par la réactivation préfectorale de dossiers anciens (321 dossiers en RDV individuels ou familles). Les démarches se compliquent du fait de l’origine de quelques cas isolés, Soudanais, Erythréens, Afghans ou Mongoles qui posent des problèmes d’interprétariat oral et écrit.

Sur les 20 dernières années d’activité LDH/RESF, nous avons enregistré un total de 1 301 dossiers classés dans les archives, ce qui correspond à  environ 4 500 personnes concernées dont une centaine de MIE/MNA… couvrant pratiquement tout le champ juridique du code CESEDA… Autant dire qu’il vaut mieux se tenir à jour. Une formation spécifique et permanente est nécessaire pour rester efficace et préparer la relève.
       
Réunions publiques impossibles, mobilisations de rue difficiles et très « minorisées » On a parfois l’impression d’être passés dans la clandestinité. Puisqu’il nous est inlassablement répété que nous vivons dans un Etat de Droit - au point de nous en faire douter - nous tentons de ne rien laisser passer et de recourir systématiquement à la Justice. Chaque requête gagnée n’est pas seulement gagnée pour les intéressés mais une victoire contre la politique xénophobe du pouvoir… donc une victoire aussi politique pour nous…

Les permanences hebdomadaires à Pamiers suffisent tout juste à répondre à la demande. En août, septembre et Octobre nous avons doublé les permanences en tenant une seconde le Lundi à Foix. L’équipe est rodée et de plus en plus efficace... Un grand merci à celles et ceux qui se déplacent chaque semaine, sachant qu’ils rentrent à la maison avec du « boulot » dans le sac… Nous savons aussi que nous pouvons nous appuyer  sur de bonnes relations et sur l’efficacité professionnelle de quelques avocates et avocats à qui nous demandons beaucoup. Nous avons rempli et déposé pas moins de 230 dossiers d’aide juridictionnelle cette année (150 en 2019).

Les séances de Français Langue étrangère ont été très sérieusement perturbées. Suspendues par obligation, elles se sont parfois transformées en « cours particuliers à domicile ».

Les capacités d’accueil à domicile sont maintenues (Familles ou Mineurs Isolés)… ça représente un investissement militant important qui nécessite de mettre en commun les charges et les difficultés.
 
Nous sommes en relation fréquente avec les lieux plus institutionnels assurant l’accueil (CADA, CAO, MECS, Accueils d’urgence, familles d’accueil, foyers, Emmaüs…) Nous avions à titre expérimental, mis en place nous-mêmes quelques lieux d’accueil… Il convient de s’interroger sur ces tentatives. En avons-nous les moyens militants et financiers ? RESF a-t-il vocation à devenir une association humanitaire polyvalente ?

**Quelques mots concernant notre fonctionnement… Quand les temps redeviendront normaux…**

Une réunion mensuelle du Réseau assure un minimum de prises collectives d’orientations et de décisions. D’abord destinée à informer, elle est devenue le lieu de rencontre des militants les plus actifs.  

Les permanences hebdomadaires fonctionnant à Pamiers constituent un lourd investissement militant. Une dizaine de camarades accueillent, sur rendez-vous, les personnes en demande. L’équipe, aujourd’hui pleinement fonctionnelle, semble bien stabilisée. Que faisons-nous ?

-	Nous assurons l’accueil des primo-arrivants et des demandes d’asile auprès de l’OFPRA, complétant les dossiers, assumant si besoin les traductions des récits d’exil, expliquant le déroulement des démarches futures, essayant d’orienter vers des hébergements, assurant la scolarisation des enfants s’il y a lieu. Le nouveau dispositif régional (SPADA) et l’absence de dispositif officiel de domiciliation compliquent la tâche. Le siège social de RESF et de la Ligue domicilient de plus en plus de dossiers (Asile, Justice ou encore CPAM). C’est une grosse responsabilité.

-	Nous assurons la rédaction des recours et des demandes d’aide juridictionnelle auprès de la Cour Nationale du Droit d’Asile (CNDA). En phase de recours, nous enregistrons un peu moins de demandes, les personnels CADA et les avocats commis d’office les assurant auprès des personnes hébergées (200 places). Nous sommes amenés à participer financièrement aux nombreux déplacements vers Toulouse et Paris. 

-	Nous organisons la défense juridique, le plus souvent auprès du Tribunal Administratif et des Cours d’Appel, des personnes ou familles menacées d’expulsion (OQTF ou arrêtés de transfert Dublin), assignées à résidence ou enfermées au Centre de Rétention Administratif (CRA). Ce qui semblerait redevenir « la mode » avec des expéditions ratées à Hendaye, Nîmes, Perpignan ou Marseille.

-	 Nous transmettons aux avocats, les dossiers préalablement constitués « à toutes fins utiles » ce qui implique de tenir à jour ces dossiers en «alerte», y compris les demandes d’aide juridictionnelle. Les délais de procédure sont désormais plus restreints. C’est un travail d’archivage, de correspondance écrite et téléphonique. C’est un travail d’anticipation. L’expérience nous a appris à envisager tous les possibles y compris les pires et les plus urgents.

-	Nous sommes confrontés aux besoins de traductions… Celles que nous pouvons assurer nous-mêmes (plusieurs langues), celles que nous pouvons transcrire à partir d’une traduction orale, aidés d’intervenants de même origine culturelle, discrètes et compétentes (le plus fréquent). Nous commandons et payons aussi les travaux des traducteurs assermentés quand c’est indispensable mais c’est cher. C’est un poste de dépenses important.

-	Nous sommes amenés de plus en plus souvent, confrontés aux refus de renouvellement des séjours  médicaux accompagnés d’Obligation de Quitter le Territoire (OQTF) à monter des dossiers juridico-médicaux pour les défendre auprès des tribunaux. L’OFII (Ministère de l’Intérieur) ayant remplacé l’ARS (Ministère de la santé) ses avis sont la plupart du temps négatifs auprès de la Préfecture qui applique sans plus d’examen approfondi de la situation des intéressés. La machine à expulser fonctionne sans états d’âme… Mais ont-ils une âme ? 

-	Nous avons constitué et rédigé de nombreux dossiers de demande de régularisation discrétionnaire. C’est long, compliqué et ça demande un travail relationnel avec les intéressés et plusieurs rencontres préalables. Ils sont transmis obligatoirement par recommandé (moyenne de 10 € l’envoi). Les refus sont quasi systématiques depuis un an donc retour devant le Tribunal Administratif…

-	Nous avons pris en charge quelques dossiers de demande de naturalisation. Ils sont complexes et exigeants, aux multiples documents à fournir. La concentration administrative actuelle sur la Région rend souvent les procédures inaccessibles…Les ratés se multiplient notamment à l’OFII  (ADA, retour au pays, santé, naturalisations...) 

-	Avec le souci de la plus grande efficacité possible, nous souhaitions entretenir des relations convenables avec les différentes institutions dont dépend ce public, mal nous en prit… la Préfecture devient progressivement « porte close » où il est de plus en plus difficile d’obtenir des Rendez-vous… Comment disait déjà ce nouveau Russe ? « la Maison commune » n’est plus commune… A nos âges ce n’est pas sérieux d’entretenir de telles illusions.

- Nous devons pour ce faire, prendre soin d’expliquer aux intéressés quelles sont les réglementations en vigueur et le fonctionnement des institutions. Ce qui s’avère difficile. Nous devons expliquer qui nous sommes… que nous sommes solidaires, que nous sommes bénévoles et que nous ne pouvons pas « tout faire »…

-	Les équipes interviennent sur l’apprentissage de la langue, l’une à Foix, l’autre à Pamiers. La fréquentation variable est composée prioritairement de personnes qui, du fait de leur absence de statut, n’accèdent pas au FLE institutionnalisé. Cette activité, surtout sur le plan relationnel, a l’air très appréciée.

-	Les mineurs isolés, tout au long de l’année ont occupé beaucoup de temps et d’argent. Nous accompagnons les « rejetés » afin qu’ils puissent avoir recours à la Justice (Juge des Enfants et Cour d’appel des mineurs). Les quelques requalifications obtenues sont venues rappeler que la législation existe et qu’il convient de la respecter. Nous avons, à notre demande, été reçus par la Présidente du Conseil Départemental, l’élue responsable des affaires sociales et les administratifs de l’ASE. Il semblerait que les choses évoluent dans le bon sens.

-	Avec la même discrétion et la même ténacité, nous avons accompagné presque tous les enfants vers une scolarité salvatrice, assurant des aides à la rentrée scolaire  (refusées par la CAF et attribuées au compte-gouttes par le Conseil Départemental). 

L’immigration est au cœur des débats sociaux et politiques. Faire connaître nos expériences ne peut qu’assainir ces débats     

Au-delà des « oublis » que vous nous pardonnerez… en quelques lignes une année d’activité bien remplie, forte d’une équipe consolidée, pleine d’espoir dans une période à venir qui s’annonce plutôt sombre…

Le 31 décembre 2020
