.. title: Manifestation contre la loi immigration
.. slug: manifestation-contre-la-loi-immigration
.. date: 2024-01-07 08:52:29 UTC+02:00
.. tags: rassemblement
.. category: rassemblement
.. link: 
.. description: RESF09 Loi immigration
.. type: text


**DIMANCHE 14 JANVIER L’ARIÈGE SE RASSEMBLE CONTRE LA LOI IMMIGRATION**

La Loi immigration a été adoptée au Parlement grâce aux voix de la droite et de
l’extrême droite. Son contenu reprend les principales propositions du
Rassemblement National et remet en cause nos principes républicains, comme
l’illustrent notamment ces mesures :

- Le principe de préférence nationale serait appliqué et conduirait à exclure les étrangers de l’accès aux allocations familiales et aux allocations au logement
- Le droit du sol serait remis en cause et l’acquisition de la nationalité française ne serait plus automatique pour les enfants nés en France mais dont les parents sont étrangers
- Les étudiants devraient fournir une caution pour avoir accès à un titre de séjour
- Pour faire adopter sa loi avec le soutien de la droite et de l’extrême droite, le gouvernement s’est également engagé auprès de ses alliés à remettre en cause l’aide médicale d’État dès le début de cette année

**FACE À UNE LOI SI RÉGRESSIVE, FAISONS VIVRE LES VALEURS D’ÉGALITÉ, DE SOLIDARITÉ ET DE FRATERNITÉ !**

RENDEZ-VOUS

DIMANCHE 14 JANVIER À 11H

AU MONUMENT DE LA RÉSISTANCE

ALLÉES DE VILLOTE FOIX

.. image:: /images/Tract-14-janvier.png

