.. title: Compte rendu avril 2022
.. slug: compte-rendu-avril-2022
.. date: 2022-04-08 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Avril 2022
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse

A noter pour ce rendez-vous mensuel une faible participation des adhérents déjà constatée début mars. Faute d’internet les invitations arrivent mal.

**Ukraine**

En fait, le pays est victime pour une bonne part du « jeu » géopolitique de l'OTAN qui a resserré son étreinte sur la Russie depuis la chute du mur de Berlin. Aujourd'hui, les pays proches craignent une extension du conflit sur leur territoire.

Reste une guerre absurde et son cortège d'atrocités injustifiables sur les civils. Dans quel but ?
Pour ce qui est des réfugiés venant en France, leur nombre restera assez limité. Pour autant, il convient de répondre le mieux possible à leurs besoins, individuels et familiaux (hébergement, alimentation, vêtements, soins, apprentissage de la langue, scolarisation des enfants...).
Administrativement, ils sont placés sous le régime de la protection provisoire sous couvert de la Préfecture. Les adultes peuvent travailler.

Les Municipalités et les élus locaux ont également été sollicités.

Pour ce qui nous concerne, que peut-on faire concrètement ? Essentiellement du FLE car les Pouvoirs Publics doivent s'impliquer directement.

Il conviendra quand même de contacter les CADA pour connaître leur analyse de la situation et leurs propositions.

Plus largement, on peut s'inquiéter de la fin de la trêve hivernale et des décisions d'expulsion que cela entraîne habituellement ; toutefois, la période électorale pourrait inciter l’administration à différer certaines décisions vis à vis de populations précaires.

**Nouvelles OQTF (ciblant particulièrement les Guinéens)**

Heureusement pour eux, la légalisation des papiers d'identité s'opère sans difficulté auprès de leur ambassade.
L'option indispensable pour les plus de 18 ans est la demande d'AES (Admission Exceptionnelle au Séjour), même si les dossiers sont assez lourds.

Par ailleurs, on note ces derniers temps une moindre pression de la Préfecture sur les employeurs de ces jeunes qui ne gênent personne et qui aspirent simplement à s'intégrer.

**Procédures en cours**

léger fléchissement du nombre d'expulsions constaté.

**Scolarisation**

 il faut rester attentifs à la prise en charge des jeunes pendant les vacances scolaires ; les volontaires sont toujours les bienvenus.

**Tests osseux**

ça recommence (!) à la demande de la JDE (juge des enfants) alors que, comme on le sait, ces tests n'ont qu'une très faible fiabilité. A noter que des demandes de protection vieilles de 6 mois n’ont toujours pas de réponse de la Juge. 

Alors peut-on parler de protection urgente ?

**Décision relative au don d'un bien immobilier**

Un couple d'adhérents à RESF souhaite donner une maison sise à Pamiers à une association qui la destinerait à l'hébergement de migrants, en particulier des jeunes. La proposition en a été faite à Christian qui met la décision au débat. Il nous semble que les contraintes qui y seraient liées (financières, techniques, humaines) apparaissent trop lourdes. En conséquence, Christian prendra contact avec d'autres associations en capacité de mieux répondre à cette opportunité et aux souhaits des donateurs. 

**Traitement des dossiers Couserannais**

Une possibilité de permanence vient de voir le jour à St Girons dans un local occupé jusqu'alors par les Restos du coeur.

D’accord sur le principe, reste à définir les modalités de cette permanence qui serait assurée par une adhérente volontaire.

Alain Lacoste, secrétaire
