.. title: Compte rendu février 2025
.. slug: compte-rendu-fevrier-2025
.. date: 2025-02-04 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 février 2025
.. type: text

Était prévu à l’ordre du jour de ce lundi 3 février 2025:

1. Journée mondiale des Droits des Étrangers - Coordination
2. Ministère de l’intérieur et autres avatars de la politique gouvernementale
3. Expulsions, OQTF, Assignations…
4. Mineurs isolés
5. Des personnes, des familles avec enfants dormiront dehors

Contrairement aux réunions habituelles il n’a pas été respecté le déroulé des 5 points prévus. 

Le bilan dressé de la journée de 25 janvier a finalement intégré tous les autres thèmes prévus

Le bilan chiffré de cette journée du 25 janvier : Satisfaisant. Pourquoi ?

- Les structures présentes : 14 sur 17 (malgré plusieurs courriels de relance)
- La fréquentation : +/- 120 personnes sur la journée… même si le matin le rassemblement n’a pas réuni autant de participants (82 exactement). Globalement c’est satisfaisant, mais si on divise 120 par 14 on doit relativiser la mobilisation dans chaque structure…
- Une dizaine de « stands » installés l’après-midi.

Qu’en tire-t-on ?

- Les échanges ont été denses, nombreux et riches…
- Quelques volontaires se sont déclarés pour rejoindre les permanences LDH/RESF du vendredi … nous devrions pouvoir nous organiser par pôles (Santé, Mineurs isolés, etc. …)
- Quelques nouveaux intervenants volontaires aux ateliers de français Centre Social du Courbet à Foix…

Quelle suite « politique » ?

- Changement de 1er ministre, mais Retailleau, Darmanin et autres sont toujours là !
- A l’Intérieur Retailleau confirme la pression sur les Préfectures : L’idéologie de la droite extrême se met en œuvre : c’est faire en sorte que pas une demande de régularisation ne débouche, fait nouveau en Ariège : une commission pour le renouvellement des titres de séjour de + de 1 an est constituée et s’est déjà réunie… Il faut faire mal et le faire savoir tant auprès des migrants qu’auprès des populations persuadées que la « vague migratoire » nous « submerge »… au mépris des chiffres qui sont à l’opposé de l’émotion soulevée par ces propos alarmistes. Désigner des boucs-émissaires permet de détourner le regard des vraies souffrances : accentuation des difficultés sociales, dégradation des conditions d’accès à la santé publique, difficultés de l’école publique, saturation des services de protection des biens et des personnes, anxiété face aux problématiques écologiques …
- La mobilisation de la coordination créée au lendemain du « 26 janvier » reste une nécessité. Quelle forme doit-elle prendre ?

**Alerte !**

Nos soucis majeurs du moment : Trouver des hébergements pérennes pour les mineurs isolés que nous avons réussi à scolariser faute d’hébergement pérenne, nous risquons de devoir renoncer à ces nouvelles prises en charge.

Si vous avez connaissance de quelques pistes (même incertaines…) joignez christian.morisse@nordnet.fr ou le même par téléphone 06 70 94 08 48 
