.. title: Convocation réunion juillet 2021
.. slug: convocation-reunion-juillet-2021
.. date: 2021-07-18 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion juillet 2021
.. type: text

FLASH du Jour:

1) D’abord nous vous rappelons la réunion de travail sur le theme des Mineurs Isolés (MIE — MNA)

Ce Lundi 19 Juillet 2021 de 15h à 17h a la Mairie de FOIX — salle Soulié

Nous y aborderons les diverses facettes de l’accompagnement et de la solidarité :

- accueil et la reconnaissance de minorité (hébergement et démarches juridiques)
- L’apprentissage du Francais Langue Étrangére
- la scolarisation et la formation professionnelle
- l'apprentissage
- les prises en charge financiéres et les hébergements
- les mobilisations et les procédures juridiques mises en ceuvre face aux menaces préfectorales d’expulsion
- un petit bilan du passé
- une amélioration possible de notre fonctionnement

Certes deux heures ne suffiront pas, mais il est nécessaire de faire ce point.

La réunion est proposée a toutes celles et tous ceux de RESF 09 impliqué-e-s dans ce boulot
ou, mieux, qui souhaitent s’y impliquer.

2) Nous venons d’apprendre qu’une initiative de soutien 4 Fodé KONDIANO apprenti boulanger,
recu au CAP, menacé d’expulsion et sous le coup d’une OQTF préfectorale, est prévue ce méme
Lundi 19 juillet à 9h30 devant la Boulangerie Evrard à Lavelanet où Fodé est apprenti.

NOUS Y SERONS SOLIDAIREMENT.

Un recours est déposé au Tribunal Administratif de Toulouse. Nous espérons que la Justice sera
favorable a Fodé, qu’elle reconnaitra les efforts d’intégration et de formation, qu’elle fera preuve
d’une humanité que la Préfecture ignore totalement.

Montoulieu le 17 juillet 2021

Christian

