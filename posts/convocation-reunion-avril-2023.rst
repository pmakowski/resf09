.. title: Convocation réunion avril 2023
.. slug: convocation-reunion-avril-2023
.. date: 2023-03-28 16:03:08 UTC+02:00
.. tags: convocation
.. category: convocation
.. link: 
.. description: RESF09 convocation réunion avril 2023
.. type: text

Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF. Mais, élections obligent, nous devrons attendre le Vendredi pour disposer d’une salle à FOIX.

REUNION MENSUELLE du Réseau Éducation Sans Frontière

Le VENDREDI 7 AVRIL 2023 de 17H30. à 20H.

Salle Jean Jaurès – Mairie de FOIX

***Projet de loi « Immigration »***

Nous avions bien travaillé, tout l’après-midi,  la semaine passée, pour essayer de comprendre ce que contenait le projet de Loi. Nous apprenions alors que le grand chef (Macron) avait décidé qu’il n’était plus question de Loi. Tant pis pour celui qui croyait pouvoir se faire « mousser ».

Mais ne soyons pas dupes : les mêmes petites saloperies peuvent très bien être mises en application sans passer par une Loi et sans un nouveau 49-3. Il semble bien que les Préfectures aient déjà reçu des consignes pour une mise en œuvre effective. Nous n’avons pas travaillé pour rien et nous y reviendrons lors de cette prochaine rencontre, notamment sur les demandes d’asile à l’OFPRA et sur la Cour Nationale du Droit d’Asile (CNDA).

***L’Europe des murs et des barbelés***

Après avoir renforcé Frontex, les instances européennes ont budgétisé les aides à apporter aux pays qui mettent en place des systèmes de « protection » de leurs frontières. La carte des murs existants devrait s’allonger et le nombre de noyés en Méditerranée ou en Manche s’allonger aussi.

***Au printemps on peut coucher sous les ponts…***

Beaucoup de familles sont menacées de devoir quitter les hébergements actuels dont de nombreuses places dans les campings, pour des destinations incertaines, voir la rue. Le 115, non seulement est saturé, mais reçoit l’ordre (autorité de tutelle préfectorale dite de protection des populations) de refuser certaines demandes pour les familles en situation irrégulière. Quid du suivi de scolarisation des enfants ? changement d’école ? ou déscolarisation ? 
            
***Cas particuliers dans l’urgence***

Quelques  bonnes nouvelles du côté du tribunal administratif, quelques mauvaises côté administration, de nouvelles exigences ou tracasseries visant plus particulièrement les ressortissants de Guinée Conakry. 

***Questions diverses*** : qui sont les vôtres.   

Bien amicalement à toutes et tous,

Montoulieu le 27 mars 2023

Christian 
