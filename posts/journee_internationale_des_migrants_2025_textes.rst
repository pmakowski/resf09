.. title: Journée Internationale des migrants - 25 janvier 2025 à FOIX
.. slug: journee_internationale_des_migrants_2025_textes
.. date: 2025-01-25 06:55:00 UTC+01:00
.. tags: actions
.. category: 
.. link: 
.. description:Conférence débat ateliers
.. type: text

Journée Internationale des migrants - 25 janvier 2025 à FOIX
-------------------------------------------------------------

Les interventions:

Prise de parole de Maryline Lambert au nom de la Coordination ariégeoise Asile Immigration, le 25 janvier 2025


Bonjour,

Le 14 janvier 2024 à Foix, une première mobilisation intersyndicale de 3 syndicats, la CGT, la FSU et Solidaires, avait répondu à un appel national à manifester contre la Loi Asile Immigration. Cette loi n’était pas encore promulguée (elle le sera le 26 janvier) que nous alertions sur ses dangers. Dangereuse, elle l’était non seulement pour les personnes étrangères présentes sur le territoire français, mais également pour la société elle-même, en ce que les principes fondamentaux des Droits de l’Homme qui fondent notre système y sont bafoués.

Il fallait faire grandir la protestation.  Et donc élargir son expression en faisant appel à des organisations syndicales, politiques, associatives, porteuses de valeurs qui les rassemblent. L’antiracisme, la solidarité, la fraternité, le respect de la dignité humaine, la lutte contre les idées d’extrême-droite, pourvoyeuse de haine, de rejet de l’autre et de reculs sociaux. Ainsi, la Coordination ariégeoise contre cette loi est-elle née le 15 janvier 2024, tenant sa première réunion à Pamiers sur une proposition de l’UD CGT, approuvée tout d’abord par la LDH. L’idée s’appuyait sur un appel de 20l personnalités, très diverses, décidées à faire partager leur rejet de cette loi.

C’est ainsi que la manifestation du 21 janvier à l’appel de la Coordination, munie de sa banderole, a rassemblé des manifestants issus notamment des 17 organisations signataires, qui réclamaient l’abrogation de cette loi.

Nous avons voulu informer des méfaits de cette loi, le plus largement possible, chacun avec les moyens de son organisation. Nous nous sommes appuyés sur un communiqué détaillant le texte rétrograde qui allait entrer en application. Même si 35 articles en avaient été censurés par le Conseil constitutionnel, partiellement ou totalement, il n’en restait pas moins que le gouvernement, avec cette loi, avait accepté de livrer sur un plateau une victoire idéologique à l’extrême-droite. Au devoir d’accueil, d’humanisme, le gouvernement et son ministre de l’Intérieur, Darmanin, avaient préféré la stigmatisation et la discrimination des étrangers.  
Autre action proposée par la Coordination, avec l’apport indéniable et précieux de la LDH : un cycle de formations; des séances sur le droit d’asile, la famille, le séjour, l’éloignement, la législation européenne notamment, ouvertes en priorité aux organisations membres de la Coordination

Le 19 avril 2024, se tenait à Foix, salle Jean Jaurès, une Conférence intitulée « Immigration, une chance pour l’Europe ». Madame Ekrame Boubtane, universitaire spécialisée dans les migrations internationales, a passionné l’auditoire, très nombreux,  avec les enjeux de l’immigration vus sous le prisme de l’économie. Loin de considérer les personnes migrantes comme une charge ou un « coût », elle a expliqué en quoi l’immigration est une chance. On sait que depuis les années 1950, 60 et décennies suivantes, l’arrivée de ces travailleurs a permis l’augmentation de la richesse nationale et du taux d’emploi. Elle a conclu son exposé en observant que l’Europe qui se barricade n’empêchera pas les mouvements migratoires, et c’est pourquoi il faut travailler sur l’accueil et l’intégration.

Un an après, quasiment jour pour jour, la promulgation de la loi du 26 janvier 2024, la Coordination réaffirme son opposition à cette loi, ainsi qu’aux velléités de certains d’en sortir une autre. Cette intervention était préparée en début de semaine, et vous avez vu que jeudi 23, est parue la circulaire Retailleau, actant un durcissement des régularisations ; remplaçant la circulaire Valls de 2012, elle porte la durée de résidence nécessaire à une régularisation à 7 ans ! Notamment. Nul doute que les préfets seront enclins à mettre en application ce serrage de vis.  
Le vent mauvais souffle toujours, attisé par des ministres du gouvernement proches, trop proches de l’extrême-droite. Sur les décombres du cyclone à Mayotte, cyclone qui n’a fait qu’aggraver une pauvreté endémique provoquée par l’inaction des gouvernements successifs, certains n’ont pas hésité à réclamer l’abrogation du droit du sol à Mayotte. Droit du sol hérité de la Révolution française dont certains, toujours les mêmes, ont tant de mal à accepter les principes.  
D’ailleurs, les idées d’exclusion et de discrimination démangent toujours les mêmes, tel Bruno Retailleau, encore et toujours lui, n’hésitant pas tout récemment à avancer qu’il faut toucher à l’AME (Aide médicale d’État). Mardi, il adoubait le collectif Némésis, proche de la famille Le Pen comme de groupes néo nazis, qui dénonce les faits de violences aux femmes uniquement sous le prisme du racisme ; ainsi produit-il des badges où l’on voit des hommes noirs poursuivre des jeunes femmes blondes pour les violer « Rape fugees », étrangers violeurs, formé sur refugees (source FR Inter, Libération). On a les amis qu’on peut….  
Aujourd’hui, nous nous retrouvons donc animés de la même énergie que l’an passé. Energie pour combattre, énergie pour débattre et proposer. C’est pourquoi, dans la continuité de la Journée internationale des Droits des migrants le 18 décembre, la Coordination a souhaité organiser cette journée. Christian Morisse va intervenir à présent, sur les enjeux de l’immigration, puis il présentera brièvement la seconde partie de la journée.


POURQUOI SOMMES-NOUS ICI ? AUJOURD’HUI ? (Christian )

Parce que nous habitons tous la même maison : la Planète  
Parce que nous appartenons tous à la même famille : l’Humanité  
Parce que si nous voulons aller mieux, aller bien, nous avons besoin d’une maison et d’une famille qui vont bien
Nous sommes ici…  
Parce la planète va mal… parce que l’humanité va mal…  
Regarder ces dégradations sans réagir serait suicidaire…  

Nous Sommes là pour agir, pour faire tourner la planète et l’humanité dans le bon sens.

Le constat est alarmant :

#. La planète s’embrase, les catastrophes…  pas si naturelles qu’on le dit…
   les catastrophes climatiques se multiplient, dévastatrices et mortelles  
   Catastrophes dont on connaît de mieux en mieux les causes et les responsabilités humaines  
   Catastrophes qui privent de nourriture , de soins et de maisons, des milliers, des millions de gens,  
   Catastrophes qui jettent loin de chez eux des milliers, des millions de gens.  
   Le réchauffement climatique est en passe de devenir la première cause des migrations humaines.
#. l’Humanité s’embrase  par cupidité, surtout par peur,  
   L’Humanité s’embrase,  multiplie les zones de guerre, détruit, tue…  
   Des conflits qui jettent loin de chez eux des milliers, des millions de gens  
   En 1939, on appelait ça : l’exode !  
   Lisez donc les derniers rapports du HCR :  
   Entre Juin 2023 et Juin 2024, les exilés, les déracinés ont augmenté de 8 %  
   Dans cette seule période, Ils sont 117 millions déplacés en raison de persécutions, de conflits, de violences, de violation des Droits de l’Homme (surtout des femmes)


Alors Les migrations : de tous temps, en tous lieux… 

Et ce au plus profond de la Préhistoire, de l’Histoire de l’Humanité, à la recherche de nourriture, de territoires de chasse, à la recherche de températures plus clémentes, à la recherche de terres cultivables … toutes les raisons basiques de survie de l’humanité

Mais nous ne sommes pas dans un cour d’histoire… Quand-même arrêtons-nous un instant. 

Arrêtons-nous sur les grandes périodes coloniales, le pillage des richesses naturelles, l’esclavage et les déportations, des époques où les colons américains du Nord comme du Sud s’arrangeaient très bien des migrations forcées, des déportations massives d’esclaves… 

Arrêtons-nous sur le recours aux mains d’œuvres étrangères pour reconstruire ce que la folie meurtrière des guerres avait détruit dans l’Europe des guerres mondiales.

Arrêtons-nous sur le recours aux travailleurs immigrés pour faire tourner l’industrie des années fastes et empocher les profits colossaux que ces mêmes immigrés avaient généré.  
En 2025  les pillages continuent, les profits aussi… mais… la robotique, l’informatique et bientôt l’intelligence artificielle ont remplacé les travailleurs qui devraient « débarrasser le plancher » pointer au chômage ou rentrer chez eux…  
L’Humanité doit faire face à une économie et des profiteurs qui n’ont plus besoin de tout le monde pour produire certes, 
Mais qui ont besoin de toujours plus de consommateurs…  
Mais qui gaspillent toujours plus les ressources de notre maison, la planète.  
Les cargos-containers qui sillonnent la planète, dégueulent de marchandises produites dans des conditions de travail et de pollution toujours plus dégradées.  
Des marchandises en quantités telles qu’il est difficile de les écouler.  
Des marchandises inaccessibles à ceux qui en auraient besoin et qui n’ont pas les moyens de payer, donc qui vont devoir s’en passer.  
La contradiction n’est pas nouvelle, ce n’est pas une découverte du 21èmè siècle…  
Mais des ressources dont on voit le bout… qui ne sont  pas inépuisables…  
Les peuples repus, entendez « les pays riches » (ou tout le monde n’est pas riche d’ailleurs) les peuples repus prennent peur, peur de manquer… peur de devoir partager… peur d’être envahis par tous les « crève la faim » de la planète…  
Alors ils se barricadent, posent des barbelés aux frontières, construisent des murs embauchent des flics et posent des caméras partout, votent des lois « immigration »…

Ils ont peur… et élisent des hommes politiques qui surfent sur les peurs fantasmées.

Ce qui est grave ce n’est pas l’élection de l’actuel Président Etatsunien 
mais les 65 millions d’électeurs qui sont derrière  et… qui ont peur.

De ce monde là ne sortira rien de bon  
Rien de bon pour notre maison : la planète,  
Rien de bon pour notre famille : l’Humanité

Maintenant que le décor est planté, Que pouvons-nous faire ?  
Non seulement par solidarité… mais aussi dans notre propre intérêt ?  
Nous sommes en France :  
La France, malgré les cris d’orfraie poussés par tout ce qu’elle compte de racistes et de xénophobes,  
La France est l’un des pays qui accueille le moins « la misère du Monde » de « feu Michel Rocard »… loin derrière de nombreux pays européens…  
Pourtant la démographie est en berne et la pyramide des âges habite chez les vieux …  
Pourtant des milliers d’emplois restent vacants…  
Pourtant le patronat cherche désespérément une main d’œuvre disponible  
Pourtant des secteurs entiers sont désertés « les fameux métiers en tension »…  
Pourtant, Maryline le rappelait à l’instant : Ekrame Boubtane l’a fort bien démontré l’an passé lors de la conférence « Immigrations : une chance pour l’Europe » Nos vieux pays ont un besoin urgent de « sang neuf » ont besoin de l’immigration pour survivre… pour revivre…

Donc, deux attitudes possibles :
La Peur et le rejet  ou  l’ouverture et l’accueil.  
Les politiques menées depuis les années 70 sont délibérément tournées vers le rejet !  
L’arsenal juridique déployé par les dénommés Pasqua, Sarkozy, Besson, Collomb ou plus près de nous, Darmanin ou Retailleau, un arsenal de lois toutes aussi violentes, aussi prétentieuses est un fiasco parce qu’il a pris la mauvaise option.

Ce n’est pas notre choix ! Nous pensons que la France peut faire beaucoup mieux :

#. en matière de Droit au travail…  
   Depuis 30 ans maintenant, les personnes qui entrent sur le territoire n’ont plus le droit « d’occuper un emploi salarié » : une invention du tandem Pasqua – Méhaignerie a supprimé ce droit aux demandeurs d’asile.   
   Les statistiques du chômage n’ont pourtant pas fléchi, le versement de l’allocation d’attente (ADA) coûte cher et ne rapporte rien  
   Les gens sont ainsi désoeuvrés pendant des mois, voire des années. Ceux qui pensaient pouvoir subvenir aux besoins de leur famille par leur savoir faire et leur courage sont réduits à la mendicité… Au désoeuvrement, s’ajoute l’humiliation…   
   C’est la porte ouverte au travail clandestin, une main d’œuvre nullement protégée par le code du travail, à la merci de toutes les exploitations, à la merci de toutes les sanctions  
   Cette politique est un échec total…  
   La France n’a-t-elle pas signé les déclarations et conventions des Droits de l’Homme ? dont le droit au travail.  
   Nous pensons que le Droit au travail doit être rétabli pour tout le monde sans discrimination aucune  
#. beaucoup mieux en matière de santé  
   La France dont le système de santé et de protection sociale prétendent  être performants, peut soigner, faire de la prévention immédiate, notamment infantile auprès de publics immigrés que la vie quotidienne a lourdement fragilisés. C’est une question de santé publique dont chacun d’entre nous dépend.  
   Nous pensons que l’AME doit être accessible à tous, sans délai, en limitation de soins.
#. beaucoup mieux  en matière de scolarité  
   La France dont le système scolaire est laïque, gratuit et obligatoire s’autorise à  discriminer depuis plusieurs années, les enfants des familles en situation irrégulière, dans l’attribution des bourses ou des aides à la rentrée scolaire versées par la CAF. Les moyens d’accès à la Langue peuvent être renforcés pout les primo-arrivants et à tous les niveaux de la scolarisation.  
   Nous pensons que sur les bancs de l’école un enfant est un enfant, est un élève sans distinction « de race de couleur ou de religion » dit la Convention des Droits de l’Enfant  
   Nous pensons que le Droit à l’éducation et à la Culture sont des besoins fondamentaux de tout individu vivant en société. 
#. beaucoup mieux en matière de protection des Familles  
   La France s’est dotée d’un code de la Famille.  
   La France qui s’est dotée de dispositifs et d’une administration dite « de protection des populations »  
   Elle ne saurait discriminer parmi ceux qui ont besoin d’être mis à l’abri, besoin d’être hébergés, ceux qui cherchent un logement décent.  
   Pourtant la France pose toutes les entraves possibles permettant la réunification familiale, respectant le droit de vivre en couple, le droit de vivre avec ses enfants, en situation régulière ou pas. Les projets de l’actuel ministère de l’intérieur entend durcir encore la législation.  
   Nous pensons que le Droit au Logement, les Droits de la Famille constituent l’une des conditions premières à l’accueil humanitaire et à l’intégration.

En résumé, en conclusion :

Nous pensons que la France peut faire beaucoup mieux et accueillir plus largement…

Tels sont les objectifs que nous fixons aux travaux des ateliers cette après-midi.

- Se mobiliser pour une législation décente, humaine et solidaire
- Se mobiliser pour que Justice soit rendue
- Se mobiliser pour une mise en œuvre immédiate des solidarités

Un arsenal militant dans lequel chacun peut trouver sa place, si modeste soit-elle.


