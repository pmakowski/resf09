.. title: Convocation réunion août 2022
.. slug: convocation-reunion-aout-2022
.. date: 2022-07-26 09:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link:
.. description: RESF09 convocation réunion août 2022
.. type: text


Bonjour,

Des vacances ? qui parle de vacances ? On peut « virer » les étrangers à toute heure… pas de vacances !

Nous avons le plaisir de vous rappeler que le premier lundi de chaque mois nous tenons une réunion  de RESF,

RÉUNION MENSUELLE

Du Réseau Éducation Sans Frontière

Le lundi 1er août 2022  de 17H30 à 20H.

Salle Soulié – Mairie de FOIX

**« Toute personne a droit au respect de sa vie privée et familiale, de son domicile et de sa correspondance »**

Article 8-1 de la Convention Européenne des Droits de l’Homme – inconnu au Ministère de l’Intérieur


**1 – Expulsez le Père et les autres suivront, peut-être. :**

Pour la troisième fois, la Préfecture de l’Ariège met en place des procédures (policières) afin de coffrer le Père de famille, laissant sur le côté Femme et enfants. Edmond, Garegin, aujourd’hui Artur, tous trois mariés et chargés d’enfants sont embarqués seuls vers l’avion.

Cette méthode est particulièrement honteuse émanant des chantres « de la famille », honteuse, émanant des pleurnichards sur les ravages en Ukraine, honteuse de la part des exécutants qui peuvent signer toutes les misères sans avoir à les regarder.

Il y a des jours où nous n’avons pas l’impression d’appartenir au « même monde », où nous ne sommes « pas fiers d’être citoyens de ce pays-là »

Nous y reviendrons dans le détail et tenterons d’organiser la solidarité autour des femmes et des enfants, notamment en vue de la rentrée scolaire.

**2 – les mineurs isolés travaillent plutôt bien à l’école. :**

Nous avons eu le plaisir d’entendre « nos protégés »  annoncer leurs succès, qui au CAP, qui au Bac Pro. Nous serons moins heureux quand la Préfecture leur refusera un titre de séjour et leur balancera une OQTF au mépris des besoins en personnel formé dans de nombreuses professions. Pas de souci, nous avons des « dirigeants » qui pensent !

Nous passerons en revue la liste de ces jeunes élèves ou apprentis et nous essaierons de définir nos engagements pour la prochaine rentrée. Nous ferons également le point sur les charges financières correspondantes et les moyens dont nous disposerons pour 2022/2023.

**3 – Les femmes « parents d’enfant français »**

à défaut de pouvoir expulser les pères français, l’administration (toujours respectueuse  de l’Article 8 de la CEDH) conteste les paternités et le font payer aux femmes et aux enfants. On expliquera.


**4 – et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 26 juillet 2022

Christian
