.. title: Convocation réunion septembre 2022
.. slug: convocation-reunion-septembre-2022
.. date: 2022-08-17 16:00:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link:
.. description: RESF09 convocation réunion septembre 2022
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF, mais le « premier Lundi de septembre sera cette année le Lundi 29 août » pour cause de rentrée scolaire.

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 29 Août 2022  de 18H à 20H. - Salle des fêtes – maison des associations - FOIX

**Un tour d’horizon sur les projets du Sinistre de l’Intérieur, une tape sur les doigts de ce petit prétentieux !**

Nous vous avions adressé un article du Monde concernant les projets de Darmanin, ses copains (enfin les crabes du même panier) lui ont fait savoir, non pas qu’il faisait fausse route, mais que peut-être ce n’était pas le bon moment de réveiller les morts. Nous en parlerons.


**Préparer la rentrée scolaire pour les mineurs isolés**

Toujours pas de nouvelle de la Juge des Enfants auprès de laquelle, les appels au secours des jeunes non reconnus mineurs avaient été lancés… Merci pour la réactivité de celle qui a la charge de la protection de l’enfance. Depuis 7 ou 8 mois ces jeunes sont à la rue, ce qui n’a pas l’air de l’empêcher de dormir.

En attendant une prise en charge effective, nous les avons tant bien que mal accueillis et scolarisés. Nous préparons leur rentrée scolaire et les orientations professionnelles souhaitées. 

Nous avons fait le point sur les charges financières correspondantes et les moyens dont nous disposons pour 2022/2023. Il faudra que vous soyez généreux si nous voulons tenir nos engagements sans trop écorner les réserves.

**Préparer la rentrée pour les enfants « laissés pour compte de la CAF »**

Ceux-là, bien qu’entrant dans le cadre de « l’école Laïque, gratuite et Obligatoire » pourrons s’y rendre pieds nus et écrire sur le bois de la table… Interdits d’Allocation de rentrée pour cause de « parents sans droits ». En voilà une bien belle discrimination chez les enfants, qui ne devrait pas être pour déplaire à l’extrême droite et pourtant émanant d’un organisme social et paritaire… parait-il !

Dans la limite de nos maigres moyens, nous essaierons cette année encore de les aider financièrement… Nous nous appelons « Réseau Education Sans Frontière » l’école n’est-elle la première planche de salut pour ces gamins ballotés au gré des turpitudes administratives.

Voyez autour de vous quels sont les mômes qui pourraient être concernés et faîtes leur part des permanences dites « rentrée scolaire »

FOIX : Jeudi 25 Août à partir de 17 h et Lundi 29 août à partir de 16 h – Maison des associations de Foix

PAMIERS : Vendredi 19 août matin et Vendredi 26 août matin – Maison des Associations de Pamiers

**et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 17 août 2022

Christian
