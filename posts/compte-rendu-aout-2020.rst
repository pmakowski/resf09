.. title: Compte rendu août 2020
.. slug: compte-rendu-aout-2020
.. date: 2020-09-02 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Août 2020
.. type: text

Réunion du 3 août 2020 (Mairie de Foix)

Présidence : Yannick Garcia Animation : Christian Morisse
e Mineurs

Depuis le début de notre action, près de la moitié des suivis ont abouti à une issue positive.

Restent en ce moment une vingtaine de dossiers en cours.

Malheureusement, on constate pour eux aussi une accélération des OQTF.

Sur le plan scolaire, après une évaluation avec le CIO (Centre d'Information et d'Orientation) la
plupart sont orientés avec succès vers les filières d'apprentissage et/ou d'études techniques. Les frais
de scolarité sont pris en charge pour tout ou partie par RESF (11 jeunes concernés à l'heure
actuelle).

A noter depuis quelques temps un ciblage de la Préfecture pour expulser y compris des jeunes ayant
obtenu un diplôme puis un travail - même en CDI ! - ainsi qu'un logement, autrement dit
parfaitement intégrés.

Autre cas : un jeune en provenance des Pyrénées Atlantiques ayant obtenu le bac avec mention Très
Bien et décroché un contrat jeune majeur, Inscrit en BTS à Toulouse, destinataire lui aussi d'une
OQTF !

Ces jeunes sont en règle générale très motivés : ils obtiennent souvent d'excellents résultats
scolaires puis sont très appréciés par leurs employeurs.

Après la période de blocage que nous venons de traverser, la réouverture des frontières entraîne de
nouvelles arrivées de jeunes pratiquant très peu le français et faiblement scolarisés.

A noter : pendant les vacances d'été l'implication d'Emmaüs qui, n'ayant pu organiser son chantier
habituel, a pris en charge de nombreux jeunes tant pour l'animation que l'hébergement.

**OQTF (pour les familles aussi !)**

Elles pleuvent au rythme de 3 ou 4 par semaine, ce qui nécessite pour le Réseau de solliciter
autant d'avocat(e)s. Bien entendu, il faut en amont préparer rigoureusement les dossiers parfois en
quelques jours.

De plus, nous n'avons que peu de possibilités d'hébergement, d'autant que les demandeurs sont peu
mobiles. Heureusement, et il convient de les remercier encore une fois, plusieurs personnes ou
familles volontaires sont à nos côtés pour les accueillir.

**Scolarisation des enfants à la rentrée**

Une question majeure se pose : que peut-on faire pour aider les familles sachant qu'elles n'ont pas
droit aux aides sociales notamment à l‘allocation de rentrée ? Après un tour de table, un soutien
forfaitaire individuel de 50 euros par enfant scolarisé est approuvé.

Un rendez-vous va être pris avec la Présidente du Conseil Départemental pour étudier ces situations
humainement prioritaires.

RESF poursuivra également autant que possible son appui en liaison avec les établissements
scolaires en complément des assurances scolaires (gratuites grâce à la MAE) et des dons des
associations caritatives.

Une fiche d'information va être créée par Yannick pour guider utilement dans leurs démarches les
familles concernées.

Le 12 août 2020 - Alain Lacoste, secrétaire

