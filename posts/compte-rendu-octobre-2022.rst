.. title: Compte rendu octobre 2022
.. slug: compte-rendu-octobre-2022
.. date: 2022-10-15 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 Octobre 2022
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse

Réunion mensuelle du 3 octobre 2022


**Fait notable**

On constate depuis quelques mois le recul du nombre d'arrivées de migrants en Ariège tant du côté des individus isolés que des familles (hormis les arrivées d’Ukraine dont la prise en charge est spécifique)

**Cours de FLE (Français Langue Etrangère)**

Sur Pamiers, les rendez-vous sont désormais programmés les mercredis et jeudis de 10 h. à 11h.30. En cette rentrée, la participation s'avère faible, ce qui entraîne un questionnement voire la démobilisation des bénévoles.

Sur Foix, une baisse sensible a également été enregistrée mais le noyau de participant(e)s habituels reste solide. Heureusement, d'autres structures travaillent aussi dans ce domaine.

**Rentrée scolaire**

Le partenariat avec les collèges et les lycées du département se poursuit même si les demandes sont moins nombreuses notamment en LEP.

L'EREA propose une gamme étendue de formations d'enseignement professionnel, incluant un renforcement scolaire (français, math, …).

RESF prend en charge les frais de scolarité de plusieurs mineurs isolés en pension complète ou demi-pension, ce qui représente un coût annuel important.

Statut et hébergement dépendent au cas par cas de la décision de la Juge Des Enfants vis à vis de leur minorité ou majorité.

**Admission Exceptionnelle au Séjour (AES)**

Les difficultés avec la Préfecture persistent même lorsque les demandeurs sont prêts à travailler et ont les formations adaptées. On est en revanche parfois surpris de voir aboutir des demandes « originales » sans que l'on puisse en connaître les raisons.

Les mamans isolées d'enfants français se heurtent à des problèmes spécifiques. L’administration  leur contestant le statut et mettant en cause la paternité, la Préfecture, via le Procureur n’hésite pas à exiger des tests ADN. L'autonomie de quelques-unes de ces femmes reste aléatoire car elles ont fait ou font encore l'objet de manipulations de réseaux de prostitution.

**Finances de l'association**

La situation se tend car les dons sont en retrait. Rappelons que le montant des cotisations dépend du « bon vouloir » ou du « bon pouvoir » de chacun.

L'aide aux plus démunis se traduit par des montants importants et répétés : lors des permanences hebdomadaires, pour les dossiers AES (timbres fiscaux), sans oublier le soutien aux jeunes pour leur scolarité.

Le Secrétaire, Alain Lacoste
