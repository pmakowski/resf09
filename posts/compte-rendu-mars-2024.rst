.. title: Compte rendu mars 2024
.. slug: compte-rendu-mars-2024
.. date: 2024-03-03 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 mars 2024
.. type: text


**Mineurs isolés**

* Les arrivées sont en forte hausse. (le nombre serait de 37 jeunes venus essentiellement de Guinée, Côte d’Ivoire et Burkina Faso). Les locaux habituellement dédiés (notamment des PEP 09) et autres foyers ne suffisent plus à couvrir les besoins, d’où une utilisation exponentielle par le Conseil Départemental de la solution (très coûteuse) hôtelière … Côté RESF, nous avons évoqué plusieurs pistes … toutes sans solutions réalistes et pérennes à court terme. 
* Cas des « jeunes pas jeunes » (évalués majeurs). 7 recours sont « en cours » Nous avons la crainte que la justice (en situation de lenteur alarmante) de traite les dossiers (estimés moins prioritaires que d’autres) dans des délais qui ne permettent pas de déboucher sur des avis adaptés à l’urgence. Malheureusement c’est la même situation dans les autres  départements de la régions (400 situations analogues actuellement sur Toulouse)

Devant cette situation se confirme la nécessité (déjà évoquée lors de la réunion de février) d’organiser une « cellule jeunes » à la permanence du vendredi à Pamiers
Est pointé l’utilité d’adresser un courrier :

* au Tribunal pour exposer la situation et demander qu’une solution adaptée soit trouvée
* au Conseil Départemental pour exposer la situation et demander qu’une solution adaptée soit trouvée

Reste à savoir qui et quand ces courriers seront rédigés et envoyés.

Une satisfaction : une deuxième avocate spécialiste du Droit des Étrangers vient de s’installer à Foix.
Suite aux résultats des évaluations des acquis scolaires (déjà engagées) pour 5 jeunes, se présente le problème de l’orientation de ces jeunes. Qui peut rejoindre la MLSD (Mission de Lutte contre le Décrochage Scolaire) , qui peut aller en PPRP (Parcours Progressif de Réussite Personnalisée). Dans quels établissements y a-t-il des places ? Quels arbitrages pour répondre aux coûts induits pas ces choix ? Quelles solutions d’accueil et d’accompagnement pour les jeunes hors temps de scolarisation et/ou d’internat ?

Pour information :

* Le Conseil Départemental accompagne financièrement actuellement  de nouveaux contrats « Jeunes majeurs » au regard des projets professionnels individuels et des bilans des foyers et/ou familles d’accueil concernés.
* A ce jour, ce sont 202 places en CADA ouvertes en Ariège. Mais un « turn-over » important s’est accéléré ces derniers mois puisque, à défaut d’obtenir le statut de « Réfugié » les demandeurs d’Asile sont dans l’obligation de quitter les lieux d’hébergement dans le mois qui suit le rejet (signifié par l’OFII) sortie « sans bagages et sans destination : le 115 est saturé…

**Coordination contre la loi « asile-immigration »**

* Diffusion dans la presse du « communiqué commun » : Voir ce qui est écrit dans l’invitation à cette réunion
* Cycle de formation proposée par la LDH : Christian M. rappelle que ce cycle de formation cible 3 publics :

1. les bénévoles engagés (ou qui pourraient s’engager) dans la tenue des permanences du vendredi,
2. des personnels (issus d’associations ou services publics) en charge des migrants et 
3. des « militants » des différentes structures signataires du communiqué commun pour constituer dans ces différentes structures un réseau de personnes informées sur les aides et conseils à apporter aux migrants (et surtout être informées sur les erreurs à ne pas commettre pour éviter les pièges nombreux dans lesquels quelques personnes « de bonne volonté » sont tombées sans le vouloir, dans le passé) Pour le reste, voir ce qui était écrit dans l’invitation à cette réunion 

* Conférence « L’immigration, une chance pour l’Europe » présentée par Mme Ekrame BOUBTANE : `ici <https://resf.ariege.eu.org/posts/migrationeurope2024/>`_

Le Secrétaire, Francis Lavergne

