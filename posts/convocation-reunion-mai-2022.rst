.. title: Convocation réunion mai 2022
.. slug: convocation-reunion-mai-2022
.. date: 2022-04-25 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion mai 2022
.. type: text

L’hibernation tire à sa fin semble-t-il… Après 59 jours de panne internet à laquelle jusqu’à ce jour, ni « Nordnet satellite », ni la tentative de mise en place d’un raccordement « fibre optique Orange » n’ont su répondre… nous retrouvons enfin internet. Pour combien de temps ? Alors profitons-en, tant que ça marche. 

Difficile néanmoins d’être réduit au silence pendant ces deux derniers mois, mois de guerre en Ukraine et ailleurs, mois de « campagne électorale » mais surtout deux mois de maltraitance des étrangers même si les autorités sollicitent des « larmes de crocodile » sur les réfugiées Ukrainiennes et leurs enfants. Faut-il leur rappeler que des milliers « d’autres » étrangers sont aussi en perdition au Moyen-Orient, en Afrique sub-saharienne, en Erythrée, en Afghanistan ou au Yémen, étrangers qu’elles tentent par tous moyens de rejeter, d’expulser ou d’affamer. Nous avons choisi de ne pas « faire deux poids – deux mesures » et de poursuivre dans le cadre d’une solidarité universelle.

Quant à la campagne électorale, la caravane passe… le résultat tombera dans quelques heures. Quel qu’il soit, il sera mauvais pour les immigrés, pour les étrangers.

Nous pouvons d’ores et déjà remercier les « Gauches de toute boutique » d’avoir raté une belle occasion de virer l’extrême droite dès le premier tour. Tout ça pour une « jolie traversée du désert »… le glas des petites ambitions personnelles ?!

Cette punition infligée par les opérateurs du Net aura eu quelques avantages :

- Le constat que les réseaux solidaires fonctionnent bien et que les informations arrivent et circulent quand-même. Un grand merci à tous les travailleurs sociaux en Ariège.
- Les stratégies de remplacement sont possibles parmi les équipes, notamment celle des permanences ;
- De nouvelles liaisons plus horizontales ont vu le jour et pourront désormais être intégrées à notre mode de fonctionnement. Elles ont été possibles grâce aux compétences acquises par l’équipe ces dernières années, une bonne occasion de faire ses expériences « grandeur nature ».
- Enfin de constater que, même dans ces conditions, nous avons réussi à répondre aux nombreuses sollicitations surtout juridiques, merci bien sûr aux avocat-e-s si souvent mis à contribution avec lesquels nous avons aussi mis en œuvre de nouveaux modes d’échange.

Bref, après les confinements et autres avatars de la période, nous sommes toujours présents et prêts à dénoncer toutes les vilenies administratives et politiques, prêts  à relever les défis, prêts à affirmer les solidarités élémentaires et surtout à les mettre en œuvre.

Pour ce faire, nous avons besoin de tous, nous avons besoin de vous… A bientôt donc.

Le premier lundi du mois en Mai

RDV à la Mairie de Foix, salle Jean Jaurès le LUNDI 2 Mai à partir de17h30

Bien amicalement

Christian
