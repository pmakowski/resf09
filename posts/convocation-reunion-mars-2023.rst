.. title: Convocation réunion mars 2023
.. slug: convocation-reunion-mars-2023
.. date: 2023-02-28 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion mars 2023
.. type: text

Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion de RESF

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 6 mars 2023  de 17H30 à 20H.

**Maison des Associations  - Salle 2 – 7bis rue St Vincent - PAMIERS**

**À l’Intérieur, petit ministre deviendra grand pourvu que… (bis)**

Une de plus / si  l’on en croit le « Canard Enchainé », le même aurait quelque prétention à devenir Premier Ministre… et comme un malheur ne va jamais seul : la Xème Loi sur l’immigration revient sur le devant de la scène (de tous ces bons apôtres, la Scène). Nous donnerons un avant-goût de la mouture actuelle et en discuterons.


Nous avons prévu une demi-journée de travail juridique sur ce projet, ouverte à toutes celles et ceux que ça intéresse, le Vendredi 17 mars 2023 de 14h à 18 h à la Maison des Associations de Pamiers (7bis rue St Vincent).

**L’Europe des murs et des barbelés**

Après avoir renforcé Frontex, les instances européennes ont budgétisé les aides à apporter aux pays qui mettent en place des systèmes de « protection » de leurs frontières. La carte des murs existants devrait s’allonger et le nombre de noyés en Méditerranée et en Manche s’allonger aussi.

**Fin de la période hivernale**

Au 31 mars, tout le monde dehors. Beaucoup de familles sont ainsi menacées de devoir quitter les hébergements actuels dont de nombreuses places dans les campings, pour des destinations incertaines, voir la rue. Quid du suivi de scolarisation des enfants ? changement d’école ? ou déscolarisation ?

**Quelques cas d’école.**
 
A partir de deux ou trois dossiers en cours, nous regarderons les parcours possibles, les impasses et les démarches administratives, y compris « combien ça coûte ?»

**Questions diverses :** qui sont les vôtres

Bien amicalement à toutes et tous.

Montoulieu le 28 février 2023

Christian

