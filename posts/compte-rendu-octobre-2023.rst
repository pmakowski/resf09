.. title: Compte rendu octobre 2023
.. slug: compte-rendu-octobre-2023
.. date: 2023-10-27 16:46:49 UTC+02:00
.. tags: compte-rendu
.. category: compte-rendu
.. link:
.. description:  RESF09 Octobre 2023
.. type: text

Présidence : Yannick Poirier

Animation : Christian Morisse


**OQTF  et  autres  interdictions  (IRTF,  assignations  à  résidence)**

La machine préfectorale à expulsion(s) semble s'emballer, incluant pour partie 
un «nettoyage de tiroirs ». En effet, plusieurs familles arrivées depuis plusieurs 
années et bien insérées en ont fait les frais.  

Au total : pas moins de 13 cas en trois semaines.

**Métiers en tension**

On en reparle dans le cadre du projet de loi prochainement en débat au 
Parlement. On constate d'ores et déjà quelques nouvelles régularisations alors qu'il n'y a pas
si longtemps les intéressés étaient sur liste rouge. Pour l'instant cela concerne une dizaine de cas en Ariège.

Plus largement en matière de régularisations et examens de demandes diverses 
(OFII, OFPRA, CNDA) les tensions internationales compliquent bien sûr la situation 
et. les résultats. 

**Rentrée scolaire**

Pour des raisons diverses, quelques familles concernées par notre aide ne sont 
pas venues aux rendez-vous proposés par RESF. Certaines ont été régularisées au 
cours des derniers mois et peuvent désormais bénéficier des aides publiques. 

L'objectif majeur est bien sûr d'assurer l'inscription de tous les enfants de la 
maternelle à l'université. 

**Mineurs/majeurs que nous accompagnons**

Cinq internes et deux demi-pensionnaires sont concernés pour l'instant. Pour 
les mineurs, la prise en charge en principe revient au Conseil Départemental sauf rejet 
du Procureur pour « majorité ». Le juge des enfants est alors saisi qui tranchera en 
leur faveur ou qui les considèrera majeurs. Rappelons que son avis pose parfois 
question car il est arrivé de statuer sans méme avoir rencontré les intéressés. 

Subsiste un problème de place dans les établissements scolaires, notamment à 
à l'EREA qui s'est vu refuser à la rentrée (temporairement ?) un poste de surveillant, 
entraînant la fermeture d'un dortoir, touchant 20 jeunes ! RESF est intervenu pour 
plaider la cause de cet établissement et donc des jeunes eux-mêmes. 

S'y ajoute encore et toujours l'éternelle difficulté de leur hébergement lors des 
vacances et des weekends. Si vous êtes en mesure d'en accueillir, contactez bien vite 
le réseau. 

**Aggravation en Méditerranée (entre autres)**

On constate une accélération des départs en provenance particulièrement de 
Tunisie, entraînant l'aggravation des situations aussi bien pour les sauveteurs que 
dans les camps dits de transit (en Italie, Grèce, Turquie...). 

Sans doute va t'on devoir prendre l'initiative de conférences-débats pour mieux 
informer nos concitoyens sur la situation de ces miséreux qui font si peur à certains et 
qui sont montrés du doigt... 

Le Secrétaire, Alain Lacoste

