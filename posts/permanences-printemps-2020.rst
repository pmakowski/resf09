.. title: Permanences printemps 2020
.. slug: permanences-printemps-2020
.. date: 2020-03-20 08:33:49 UTC+01:00
.. tags: 
.. category: permanences 
.. link: 
.. description: Permanences durant l'état d'urgence sanitaire
.. type: text

Bonjour,

Rassurez-vous : aujourd'hui c'est le printemps, il n'est pas confiné,
même si les perspectives ne sont pas réjouissantes.

Nous sommes désolés d'avoir dû suspendre les permanences le vendredi à Pamiers.

Si besoin urgent, vous pouvez nous contacter par courriel (christian.morisse@nordnet.fr)
et laisser d'éventuels documents en PDF.

Vous pouvez aussi nous contacter par téléphone (05.61.65.65.98 ou 06.70.94.08.48)
et laisser vos coordonnées en cas d'absence.

L'association reste en "ordre de marche".


