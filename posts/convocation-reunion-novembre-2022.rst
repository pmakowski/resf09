.. title: Convocation réunion novembre 2022
.. slug: convocation-reunion-novembre-2022
.. date: 2022-10-31 16:00:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link:
.. description: RESF09 convocation réunion novembre 2022
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier lundi de chaque mois nous tenons une réunion  de RESF.

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 7 novembre 2022  de 18H à 20H. - Salle Jean Jaurès – mairie de Foix - FOIX

**Les mineurs isolés**

Toujours pas d’audience chez la Juge des enfants pour deux d’entre eux. Heureusement qu’ils ne sont pas  complètement dehors et qu’ils sont scolarisés tous deux à Pamiers. Ceci dit, il nous manque au moins un « accueil famille » pour les weekends et vacances scolaires.

Les arrivées semblent s’être taries pour l’instant. Peut-être que lorsqu’on arrive d’Afrique ou du Bangladesh, la perspective des hivers montagnards en « refroidit » quelques-uns…

**Les admissions exceptionnelles au séjour… ou des expulsions systématiques ?**

De nombreux jeunes (anciens MIE devenus majeurs) ont demandé un titre de séjour, les textes les y obligent. Répétons-le : La plupart ont une formation professionnelle et des diplômes, la plupart ont un emploi. Darmanin, du haut de ses talons, peut se réjouir et féliciter une Préfecture qui « fait du chiffre » en matière d’OQTF.

**Des cibles « test »**

Deux ou trois familles sont l’objet d’un acharnement particulier de  l’Administration comme si elle avait besoin de se prouver ou de prouver qu’elle pouvait « réussir une expulsion » même si pour se faire, il faut affamer tout le monde, renvoyer le père en laissant les autres (femme et enfants) se débrouiller sur place avec rien… espérant que de guerre lasse ils dégageront surement. Nous reviendrons en détail sur ces procédés et des dossiers en cours.

**L’hébergement en période hivernale**

Evidemment, octobre, le plus chaud de tous les temps, n’appelle pas l’urgence. Néanmoins la période hivernale commence le 1er novembre et il y a obligation  de mettre tout le monde à l’abri. Qu’en est-il dans le département ? des gens seront ils dehors ? Dans la limite de nos moyens, cette année encore, ponctuellement, nous aidons financièrement quelques familles concernées… Mais le bénévolat ne saurait suppléer les insuffisances des institutions.

**Distribuer, c’est bien mais il faut aussi alimenter la caisse.**

Nous avons fait le point financier précis pour l’année en cours. Il semblerait que l’inflation refroidisse un peu les dons et cotisations… Nous essaierons de boucler l’année sans trop de déficit, mais…

**et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 31 octobre 2022

Christian
