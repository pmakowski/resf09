.. title: Convocation réunion septembre 2024
.. slug: convocation-reunion-septembre-2024
.. date: 2024-08-20 09:00:37 UTC+02:00
.. tags: convocation
.. category: convocation
.. link:
.. description: RESF09 convocation réunion septembre 2024
.. type: text


Bonjour, 

Afin de préparer la rentrée scolaire au mieux, nous avons prévu d’avancer la réunion de RESF fin Août :. 

**REUNION  MENSUELLE  du  Réseau  Education  Sans  Frontière** 

**Le Lundi 26 août 2024  de  17h30  à  20H.** 

Salle Soulié - Mairie de FOIX

**Mineurs isolés**

 Les arrivées se poursuivent. Nous sommes alertés des cas « rejetés » lors de l’évaluation afin de mettre en œuvre un éventuel recours… force est de constater que les intéressés, dans la plupart des cas reprennent « la route » vers les grandes villes et que nous ne les voyons pas… dommage. Mais chacun fait sa vie comme il l’entend.

Pour l’heure, 6 requêtes sont pendantes à la Cour d’appel et 3 recours sont en partance vers la Juge des Enfants, si Juge des Enfants il y a ? La mutation était prévue pour le 1er septembre.

Nous ferons le point sur la scolarisation éventuelle en Septembre 2024. Les orientations ont bien avancé et les inscriptions aussi. Merci à la MAE et aux restos du cœur pour la prise en charge des assurances scolaires. Nous sommes toujours à la recherche de capacités de prise en charge pour les weekends et les vacances. Pour l’instant c’est la pierre d’achoppement.


**Rentrée scolaire**

Nous reconduisons les aides mises en place les années précédentes, faites-le savoir autour de vous… rappel des critères : enfants scolarisés de la maternelle à la terminale et familles sans ressources propres hébergées dans le département. Nous aiderons aussi quelques étudiant(e)s suivant les mêmes conditions. Les aides seront remises en main propre, nous allons programmer des « permanences spéciales rentrée ». 

**Les premiers ravages de la Loi « Asile-Immigration »**

Rappel : Contre tout respect du principe de non rétroactivité de la Loi, principe fondamental en Droit français, l’administration l’applique rétroactivement concernant des OQTF anciennes, portant leur « validité » à 3 ans contre une seule année lors de leur édition. Donc des OQTF qui étaient caduques sont ressorties du placard comme étant toujours applicables. Un recours est déposé en Conseil d’Etat.

Sans vergogne aucune quant à leur présente légitimité, quatre Ministres se sont arrogés le droit de signer les décrets d’application de la Loi du 26 Janvier…. C’est ce que Macron appelle « gérer les affaires courantes ». Nous ferons le point sur les premières applications.

**Formation pour les intervenants F.L.E.**

Bilan de la demi-journée de rencontre/ formation le Samedi 15 Juin de 9 h à 12h à la MDA et remise en route du programme 2024/2025.

**Questions  diverses  :**  qui  sont  les  vôtres. 


Bien amicalement à toutes et tous,

Christian
