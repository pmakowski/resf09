.. title: Convocation réunion juin 2022
.. slug: convocation-reunion-juin-2022
.. date: 2022-06-01 09:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion juin 2022
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF,
mais que le premier lundi de Juin est férié et qu’aucune salle n’est disponible. Aussi, la prochaine se tiendra  :

RÉUNION MENSUELLE

Du Réseau Éducation Sans Frontière

Le mercredi  8 juin 2022  de 17H30 à 20H.

Salle Soulié – Mairie de FOIX

L’ordre du jour sera chargé :

**1 – les mineurs isolés :**  

Nous avons abordé le problème des accueils pendant les congés scolaires sans pour l’instant y apporter de réponse, nous sommes le 8 juin et il reste 3 semaines. Si vous arrivez avec des propositions ce sera un bon début.

Nous ferons le point sur les trop nombreuses atteintes à leurs droits au séjour et sur les OQTF/IRTF qui pleuvent avec une constance préfectorale maladive… quelque-part entre « faire du chiffre » et «  faire du zèle ».

Nous reviendrons tout particulièrement sur la situation de Gidéon Ekomo, aujourd’hui toujours en rétention .

**2 – Fin de trêve hivernale et nouvelles menaces sur les familles :**

Plusieurs d’entre-elles ont reçu, comme l’an passé, un courrier d’injonction leur demandant, sous quinzaine, de nouveaux  éléments concernant leur situation. L’an passé, nous avions obtempéré pour 11 cas qui se sont, sans exception,  soldés par 11 OQTF nouvelles. De qui se moque-t-on ? Quelle attitude pouvons-nous tenir ? 

**3 – Les femmes « parents d’enfant français »**

Elles sont tout particulièrement ciblées par la Préfecture lors de leur demande de renouvellement de titre de séjour. Les refus reposent sur l’argument récurrent : «le père est-il bien le père ? » avec des poursuites allant jusqu’aux recherches ADN, les tentatives de retrait de la nationalité pour les enfants et bien sûr la suspension des droits sociaux ouverts. Malgré quelques « bonnes » décisions prises en référé par les Tribunaux, la Préfecture persiste et signe.

**4 – et les autres problèmes :**

De la dématérialisation infernale et des rendez-vous impossibles et des « accueils » au lance-pierre. La liste est longue des dysfonctionnements où les citoyens, où les étrangers sont méprisés, à priori des suspects.

**5 – et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 31 mai 2022

Christian
