.. title: Rapport d’activité  pour 2023 – Assemblée Générale statutaire
.. slug: rapport-activite-2023
.. date: 2024-01-04 08:33:49 UTC+01:00
.. tags: rapport d'activité
.. category: rapport d'activité 
.. link: 
.. description: Activités 2023
.. type: text

**Rapport d’activité  pour 2023 – Assemblée Générale statutaire**

Nous avions conclu le rapport de l’an passé par ces mots : « L’immigration est plus que jamais au cœur des débats sociaux et politiques. Faire connaître au grand public nos expériences ne  peut qu’assainir ces débats et remettre de l’humain face au déferlement de haine auquel nous assistons »

Si nous avons en tête les lamentables cuisines politicardes auxquelles la rédaction et le vote de la nouvelle Loi dite « immigration » vient de donner lieu, force est de constater que nous avons apparemment raté nos objectifs. L’immigration est plus que jamais l’objet de toutes les ambitions, le fond de commerce électoral des plus moches sur l’échiquier… Si loin des difficultés réelles de la vie quotidienne des populations concernées.

La tache d’encre noire envahit l’Europe et les surenchères d’extrême droite semblent payer électoralement. Ne nous faisons pas d’illusions, derrière cet affligeant constat, il y a des électrices et des électeurs… des gens à qui nous n’avons pas su « faire connaître notre travail »  On ne saurait tout faire !

**Sur la brèche 365 jours par an**

Le rythme de fonctionnement normal de RESF09, c’est la tenue de 51 permanences, une par semaine le Vendredi matin à Pamiers et deux spéciales à Foix, où nous sommes généralement entre 7 et 8 intervenants,  nous avons essayé de répondre à toutes les situations qui nous ont été soumises. Le tableau statistique joint est un bon baromètre. 

Mais les ordres tombant du Palais princier et de ses annexes ministérielles se succèdent : circulaires, injonctions, mutations, pratiques toujours plus violentes et acharnées à l’égard des plus vulnérables, les « crève la faim », les SDF sous leurs tentes et les étrangers en situation précaire. Bref les tâches quotidiennes d’administrations et d’institutions où les ordres et les contre-ordres se succèdent rendent les procédures de plus en plus illisibles et inaccessibles au citoyen ordinaire, alors que dire de l’étranger qui ne maîtrise pas la langue ?

Nous recevons souvent ces laissés pour compte  des méandres administratifs, à qui nous tentons de donner des repères dans les démarches (et les déconvenues) qui les attendent. Après les galères de la Méditerranée et le débarquement, commence la navigation à vue dans les couloirs préfectoraux ou judiciaires.

Nous nous sommes appliqués à ce que les compétences de nos équipes sachent répondre, expliquer, guider, permettre de choisir les possibles, dans le respect des publics concernés. Ce n’est pas toujours évident, tentés que nous pouvons être de « faire à  la place de » pour gagner du temps. Il s’agit de permettre à chacun  de rester l’acteur principal de sa vie personnelle et le garant de ses secrets intimes.

**Toujours présents dans la durée**

Le Réseau vient d’achever sa dixième année d’existence légale. Il est sur le terrain depuis plus de 20 ans, sous diverses formes. Le réseau est un vrai réseau où les militants et les travailleurs sociaux des différents secteurs constituent un précieux relais. 

Les permanences  que nous tenons avec suffisamment d’intervenants sont un peu la plaque tournante des décisions à prendre. Nos activités n’ont pas changé d’orientation même si le juridique occupe beaucoup plus de place. La future nomenclature du code CESEDA (Code de l’Entrée et du Séjour des Etrangers et des Demandeurs d’Asile) version 2024, nous contraindra à quelques révisions, réadaptations et formations. 

Nous proposons en annexe un plan mensuel de formation par thèmes que nous ouvrirons à tout public.

En 2023, Nous avons enregistré 108 contributions financières (cotisations et dons) pour un montant total de 19 791 € en légère baisse (les temps sont difficiles) en comparaison : 7 634 € en 2016 - 12 460 € en 2017 – 15 470 € en 2018 - 17 656 en 2019 – 20 925 en 2020 – 22 255 € en 2021 – 22 737 en 2022. Le nombre d’adhérents reste stable, environ 150. La participation associative et syndicale est, elle aussi, stable. C’est rassurant. Globalement les comptes vont bien. Nous vous présenterons le bilan financier et les principaux postes de dépense. Le Réseau ne vit que de ses fonds propres, vos dons et cotisations, ce qui lui assure une totale indépendance, salutaire par les temps qui courent… 

Les listes « sympathisants » sont difficiles à tenir à jour. Elles fonctionnent avec environ 1000 destinataires (adhérents, hébergements et sympathisants) mais nous avons trop de retours pour changements d’adresse ou de nouvelles situations non signalées. Il faudra en revoir l’utilité, l’élaboration et la gestion. 

Le site du réseau «resf.ariège.eu.org » est régulièrement pourvu (merci à notre spécialiste). Le site  mériterait lui aussi d’être mieux alimenté avec des rubriques et des infos plus larges. Il nous appartient à toutes et tous d’en faire la promotion, peut-être de l’utiliser plus dans « l’interactif ». A  voir avec les spécialistes.

**Un public variant peu**

L’essentiel de nos interventions s’adresse aux personnes d’origine étrangère, surtout aux familles avec enfants, qu’elles soient demandeuses d’asile, réfugiées, primo-arrivantes, en cours ou en fin de procédures. La mobilisation en faveur des mineurs/majeurs Isolés s’est largement développée ces dernières années et constitue une part importante de nos travaux.  

Le nombre d’entrées nouvelles dans le département semble relativement stable, voire en baisse.

Les familles d’origines géorgiennes, Arméniennes et Albanaises restent les plus nombreuses en Ariège. D’Afrique subsaharienne, nous accueillons surtout des migrants isolés, souvent mineurs et femmes seules avec enfants. C’est-à-dire généralement des familles ayant des jeunes enfants qu’il faut mettre à l’abri, souvent soigner, orienter vers de l’alphabétisation et une scolarisation urgente. 

Les lourdes menaces que fait planer la Loi en chantier quant aux prises en charge sociales et médicales va mettre à mal la mise en œuvre des solidarités élémentaires. Les familles que nous accompagnons actuellement seront touchées de plein fouet.

**Ne pas confondre « Eloignements » et « Expulsions »**

Qu’en termes pudiques, ces choses-là sont dites ! Les consignes ministérielles de décembre 2022 ont été suivies à la lettre, ou presque… dans 90 % des cas, les OQTF (Obligation de Quitter le Territoire) sont accompagnées d’une IRTF (Interdiction de retour sur le Territoire Schengen) et d’une assignation à résidence de 2 fois 45 jours en attendant de pouvoir « éloigner » par avion.

Ces trois arrêtés étant accompagnés d’une assignation, les possibilités de contestation devant le Tribunal administratif sont réduites à un délai de 48 h. Nous devons donc anticiper la préparation d’une requête en défense.  Les méthodes d’interpellation privilégient les weekends, les vacances scolaires et les jours de fête, l’administration espérant bien que notre vigilance soit en sommeil : RATE ! et les militants et les avocates sont là ! Même le Dimanche. 

Contrairement aux allégations des xénophobes, globalement, nous constatons dans le monde associatif un élan de solidarité assez général y compris à l’égard les populations « étrangères ».

Nous n’avions jamais été amenés à compléter autant de  dossiers d’AES (Admission Exceptionnelle au séjour)  procédure parfaitement discrétionnaire qui ouvre la porte à toutes les inégalités de traitement. La réponse, quand réponse il y a, est souvent négative et suivie de la cohorte d’arrêtés « d’expulsion » Tous les prétextes sont bons : Compléments de dossier, papiers d’Etat civil, passeports, non intégration, remise en cause des « paternités » et tests ADN imposés… Bref un arsenal qui pourrait se résumer à dire : Comment emmerder le monde ? Pour 72 demandes d’AES, nous avons enregistré 22 régularisations toutes catégories confondues, néanmoins un léger progrès par rapport à 2022..

Nous avons ouvert 136 nouveaux dossiers (en hausse sensible), soit environ 350 personnes nouvelles. . En fait, la file ouverte des situations a encore augmenté cette année essentiellement suite aux refus d’asile et aux sorties de CADA suivies d’OQTF (à quinze jours) - 533 dossiers en RDV individuels ou familles. Les origines géographiques de plus en plus diversifiées posent des problèmes d’interprétariat oral et écrit mais nous sommes riches de nos différences et quand on ne sait pas faire, on fait faire et on paie….

Sur les 20 dernières années d’activité LDH/RESF, nous avons enregistré un total de 1 646 dossiers classés dans les archives, ce qui correspond à  environ 5 000 à 6000 personnes concernées dont près de 200 mineurs isolés (MIE/MNA)

Ces diverses procédures couvrent pratiquement tout le champ juridique du code CESEDA… Autant dire qu’il vaut mieux se tenir à jour. Une formation spécifique et permanente est nécessaire pour rester efficace et préparer la relève.
       
Nous tentons de ne rien laisser passer et de recourir systématiquement à la Justice. Les avocates sont là, disponibles et pertinentes. Chaque requête gagnée n’est pas seulement gagnée pour les intéressés mais constitue une victoire contre la politique xénophobe du pouvoir… donc une victoire politique pour nous aussi…

Les permanences hebdomadaires à Pamiers suffisent tout juste à répondre à la demande. L’équipe est rodée et de plus en plus efficace... Un grand merci à celles et ceux qui se déplacent chaque semaine, sachant qu’elles,  qu’ils rentrent à la maison avec du « boulot » dans le sac… 

Nous savons aussi que nous pouvons nous appuyer  sur de bonnes relations et sur l’efficacité professionnelle de quelques avocates et avocats à qui nous demandons beaucoup. Nous avons rempli et déposé une bonne centaine de dossiers d’aide juridictionnelle cette année.

Les capacités d’accueil à domicile sont maintenues (Familles ou Mineurs Isolés) mais nous manquons cruellement de propositions… La concentration de la demande sur l’axe Tarascon – Foix – Pamiers nous complique un peu les choses. Ces accueils  représentent un investissement militant important qui nécessite de mettre en commun les charges et les difficultés. Nous sommes en relation fréquente avec les lieux plus institutionnels assurant l’accueil (CADA, SAO, MECS, Accueils d’urgence, familles d’accueil, foyers, Emmaüs…)

**Quelques mots concernant notre fonctionnement…**

Une réunion mensuelle du Réseau assure un minimum de prises collectives d’orientations et de décisions. D’abord destinée à informer, elle est devenue le lieu de rencontre des militants les plus actifs.  

Les permanences hebdomadaires fonctionnant à Pamiers constituent un lourd investissement militant. Une dizaine de camarades accueillent, sur rendez-vous, les personnes en demande. L’équipe, aujourd’hui pleinement fonctionnelle, semble bien stabilisée. De nouvelles venues « apprennent le métier », laissant espérer une relève possible.

**Que faisons-nous ?**

* Le dispositif régional (SPADA) devant lequel tout demandeur d’asile doit se faire enregistrer assure un premier accueil et un accompagnement aux premières démarches. Cette année, l’essentiel des nouveaux venus sont arrivés dans le département parce qu’affectés sur les CADA (200 places). Nous n’avons eu que fort peu d’entrées directes. Il nous revient souvent de déposer les demandes de recours à la CNDA (Cour nationale du Droit d’Asile) pour les ressortissants des « Pays sûrs »
* Lorsque, déboutés, les demandeurs d’Asile doivent quitter le CADA, nous assurons la suite inéluctable  sous forme d’OQTF facile à prévoir. Nous complétons les dossiers, assumons si besoin est, les traductions des récits d’exil ; Nous expliquons le déroulement des démarches futures, essayons d’orienter vers des hébergements, nous assurons la scolarisation des enfants s’il y a lieu. 
* L’absence de dispositif officiel de domiciliation complique la tâche. Le siège social de RESF et de la Ligue domicilie de plus en plus de dossiers (Asile, Justice ou encore CPAM). C’est une grosse responsabilité qui consiste à réceptionner, redistribuer et souvent expliquer les courriers reçus.
* Nous organisons la défense juridique, le plus souvent auprès du Tribunal Administratif et de la Cour Administrative d’Appel, des personnes ou familles menacées d’expulsion (Refus de séjour, OQTF, arrêtés de transfert Dublin et IRTF). Idem pour celles qui sont assignées à résidence ou enfermées au Centre de Rétention Administratif (CRA). 
* Nous avons en 2023, dû faire face à plusieurs tentatives d’expulsion. Nous avons déjà parlé ici de la violence institutionnelle dont l’administration préfectorale fait preuve sans aucun respect de la famille, sans aucune considération humanitaire. La Justice elle-même a sanctionné de telles pratiques (Juge des Libertés) et cassé les arrêtés préfectoraux. 
* Nous transmettons aux avocats, souvent dans l’urgence (procédures à 48h. de préférence pendant les weekends), les dossiers préalablement constitués « à toutes fins utiles » ce qui implique de tenir à jour ces dossiers en «alerte», y compris les demandes d’aide juridictionnelle. 
* Les délais de procédure seront  désormais encore plus restreints suite à l’application de la dernière Loi, l’objectif du pouvoir étant, faute de pouvoir supprimer le droit à un recours, de le rendre impossible par les délais imposés..
* C’est un travail d’archivage, de correspondance écrite et téléphonique. C’est un travail d’anticipation. L’expérience nous a appris à envisager tous les possibles y compris les pires et les plus urgents. Des procédures dont nous devons alerter les intéressés si possible sans faire « peur » mais comme possibles.
* Nous sommes confrontés aux besoins de traductions… Celles que nous pouvons assurer nous-mêmes (plusieurs langues), celles que nous pouvons transcrire à partir d’une traduction orale, aidés d’intervenants de même origine culturelle, discrètes et compétentes (le plus fréquent). Nous commandons et payons aussi les travaux des traducteurs assermentés quand c’est indispensable mais c’est cher. 
* Nous avons constitué et rédigé 72 dossiers d’AES (Admission Exceptionnelle au Séjour) dits discrétionnaires donc au bon vouloir du Prince. C’est long, compliqué, ça demande un travail relationnel avec les intéressés, plusieurs rencontres préalables et des centaines de photocopies de documents déjà connus des services préfectoraux mais néanmoins exigés. 
* Nous avons pris en charge quelques dossiers de demande de naturalisation. Ils sont complexes et exigeants, aux multiples documents à fournir. Depuis deux ans, la concentration administrative  entièrement dématérialisée (malgré la Loi) rend souvent les procédures inaccessibles… Les RDV, eux aussi dématérialisés, sont quasi impossibles à obtenir ou portés à plusieurs mois. Les ratés se multiplient tout comme les refus de naturalisation. Eux aussi seront rendus plus difficiles avec la Loi promise. 
* Nous devons prendre soin d’expliquer aux intéressés quelles sont les réglementations en vigueur et le fonctionnement des institutions. Nous devons expliquer qui nous sommes… que nous sommes solidaires, que nous sommes bénévoles et que nous ne pouvons pas, ne savons pas  « tout faire »…
* Les équipes interviennent sur l’apprentissage de la langue, l’une à Foix, l’autre à Pamiers. La fréquentation variable est composée prioritairement de personnes qui, du fait de leur absence de statut, n’accèdent pas au FLE institutionnalisé. Cette activité, tant sur le plan de l’apprentissage que sur le plan relationnel, a l’air très appréciée.
* Les mineurs isolés, cette année encore, ont occupé beaucoup de temps et d’argent. Nous accompagnons les « rejetés du Conseil Départemental» afin qu’ils puissent avoir recours à la Justice, Juge des Enfants et Cour d’appel des mineurs. Certes des  procédures de recours existent mais, malgré nos requêtes, les réponses du Juge des enfants se fait attendre des mois, des mois pendant lesquels les mineurs sont laissés à la dérive, sans hébergement, sans argent et sans occupation. Nous devons et tentons de pallier tous ces abandons sur lesquels les institutionnels, administratifs et judiciaires, ferment les yeux. Les quelques requalifications obtenues sont venues très tard… trop tard… 
* Avec la même discrétion et la même ténacité, nous avons accompagné presque tous les enfants des familles étrangères en situation précaire vers une scolarité salvatrice, assurant des aides à la rentrée scolaire  (refusées par la CAF et attribuées au compte-gouttes par le Conseil Départemental).

Nous  le disions en préambule de ce rapport, « l’immigration est plus que jamais au cœur des débats sociaux et politiques. Faire connaître au grand public nos expériences ne peut qu’assainir ces débats et remettre de l’humain face au déferlement de haine auquel nous assistons. » Nous ajoutons : Oui mais Comment ?     

Au-delà des « oublis » que vous nous pardonnerez… en quelques lignes une année d’activité bien remplie, forte d’une équipe consolidée, pleine d’espoir dans une période à venir qui s’annonce plutôt sombre…

Proposé à l’AG statutaire du 8 janvier 2024 à Foix.

**LDH et RESF 09 Données statistiques des permanences 2023**

L’année 2023 avec quelques repères statistiques – Assemblée Générale statutaire
merci aux travailleurs sociaux qui ont servi de relai indispensable.

* Nous avons tenu 12 réunions mensuelles du Réseau à Foix
* Nous avons tenu 49 permanences à Pamiers (avec une moyenne de 11 RDV par séance)
* Nous sommes en moyenne 7 ou 8 militant-e-s à chaque permanence.
* Nous avons tenu 4 permanences « spéciales rentrée scolaire » 2 à Pamiers, 2 à Foix, quelques visites à domicile (exceptionnelles), au cours desquelles nous avons reçu 533 RDV (« dossiers » individuels ou familles), dont 136 nouveaux dossiers pour 2023 (stable). Soit environ 250 dossiers « actifs » (certains sont venus deux ou trois fois). Soit un total en archives de 1646 dossiers. 
* Nous avons contribué à 24 dossiers d’asile (OFPRA – CNDA - réexamen et traductions). La SPADA de Toulouse, passage obligé par la Plateforme pour toutes les demandes d’asile, les quatre structures CADA du département et les avocats commis de la CNDA ont pris en charge l’essentiel de ces dossiers.
* Nous avons été confrontés à  8  classements Dublin 
* Nous avons complété 8 dossiers de naturalisation 
* Nous avons contribué à établir 6 dossiers pour des titres de séjour « santé » 
* Nous avons introduit 82 requêtes contre les OQTF – IRTF et refus de séjour au T.A. suivies si nécessaire en Cour d’Appel administrative aujourd’hui  à Toulouse
* Nous avons reçu 25 RDV pour des raisons plus diverses (souvent sociales ou administratives)
* Nous avons eu connaissance de 2 retours volontaires et 1 expulsion effective
* Nous avons complété environ 100 demandes d’Aide Juridictionnelle (dont quelques préventives)
* Nous avons introduit des requêtes sur  assignations à résidence et 7 en CRA  (à 48 h. de délai)
* Ces requêtes exigent la constitution de  dossiers juridiques en amont 
* Nous avons établi 15 dossiers de regroupement familial (mariages, divorces, rapprochements)
* Nous avons accompagné 38 Mineurs isolés (régularisations – sorties du CD -Juge des enfants)
* Nous les avons « dépannés » financièrement (souvent en complément d’Emmaüs)
* Nous avons soutenu, hébergé et financé la scolarisation de 9 Mineurs Isolés 
* Nous avons déposé 2 demandes d’adoption simple
* Nous avons accompagné financièrement  une cinquantaine d’enfants et 4 étudiants pour la rentrée scolaire.
* Nous avons (sur les finances de RESF) aidé ponctuellement de nombreuses familles  
* Nous avons accompagné  une dizaine de mineurs isolés devant la Juge des enfants
* Nous avons formulé 72 nouvelles demandes d’Admission Exceptionnelle au Séjour
* Nous avons rédigé plusieurs courriers. La Préfecture n’a que peu reçu au guichet
* Nous enregistrons 22 Régularisations adultes (toutes procédures confondues) et 9 réfugiés statutaires (hors accueil des Ukrainiens)
* Nous avons rédigé de nombreux courriers à l’extérieur (services sociaux, scolarité, justice)
* Nous avons fait procéder à plusieurs traductions officielles (souvent plusieurs documents)
* Nous avons assuré de nombreuses traductions orales et écrites (permanences et dossiers)
* Les cours de FLE hebdomadaires à Pamiers et à Foix se poursuivent 
* Enfin nous avons enregistré sur le cahier une moyenne de 10 à 12 communications téléphoniques journalières (il n’y a ni fêtes, ni dimanches…) et de très nombreux documents par mail, nous permettant ainsi de faire face aux précipitations administratives
* Et des kilomètres et des kilomètres… Merci à toutes celles et ceux qui roulent…

Il peut y avoir quelques oublis. Les relevés sont faits à partir des dossiers en cours et des cahiers.
