.. title: Convocation réunion octobre 2023
.. slug: convocation-reunion-octobre-2023
.. date: 2023-09-28 16:00:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link:
.. description: RESF09 convocation réunion octobre 2023
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF.

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 2 octobre 2023  de 17h30 à 20h. - Salle F Soulié – mairie de Foix - FOIX

**Les bonnes et les mauvaises nouvelles du mois… plutôt mauvaises…**

Le distributeur est débridé : Ces deux dernières semaines, nous avons été saisis d’une dizaine d’OQTF/IRTF,  d’assignations à résidence et d’arrêtés de transfert Dublin. Il semblerait que la machine à expulser s’emballe. Pas étonnant vu les discours du flic en chef sur les écrans. Nous reviendrons sur quelques exemples récents.

**Rentrée scolaire et orientation**

Nous avons tenu 4 permanences spécifiquement sur  le sujet dont nous avions arrêté les modalités concrètes  en Août. Il est encore possible de recevoir quelques demandes correspondant aux critères retenus.  

**Des mineurs/majeurs pas si « isolés »**

Nous venons d’accompagner « les petits » à l’école. Pour l’instant nous prenons en charge 7 scolaritées essentiellement en lycée professionnel avec internat. Nous avons quelques soucis avec le « déversement » qu’une asso toulousaine tente par une mise « sous le fait accompli ». Nous expliquerons, nous discuterons et nous devrons arrêter  une conduite à tenir. Nous renouvelons l’appel aux hébergements (weekend et vacances scolaires).

**Les « étrangers ne savent pas nager »**

Mais il semble bien ces temps-ci que beaucoup aient envie de les pousser à l’eau, voire de leur proposer des vacances au Maghreb, en Tunisie en particulier, chez ces mêmes tunisiens qui préfèrent les orienter vers « les châteaux de sable en plein désert »… Un vrai désastre humanitaire s’organise, par des européens repus « rotant dans leurs mangeoires (Léo Ferré).

**et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 28 septembre 2022

Christian
