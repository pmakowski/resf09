.. title: Convocation réunion décembre 2023
.. slug: convocation-reunion-decembre-2023
.. date: 2023-11-28 13:00:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link:
.. description: RESF09 convocation réunion décembre 2023
.. type: text


Bonjour,

Nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF.

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 4 décembre 2023  de 17h30 à 20h. - Salle Jean Jaurès – mairie de Foix - FOIX

**La période hivernale est là depuis le 1er Novembre : les étrangers ne craignent ni la pluie, ni la bise...**

*«Toute personne sans abri en situation de détresse médicale, psychique ou sociale a accès, à tout moment, à un dispositif d’hébergement d’urgence...*

*Cet hébergement d’urgence doit lui permettre, dans des conditions d’accueil conformes à la dignité de la personne humaine (...) de bénéficier de prestations assurant le gîte, le couvert et l’hygiène (...)»*  
Article L 345-2-2 du Code de l’action sociale et des familles

Discrimination oblige : le Ministère de l’intérieur et ses filiales locales ont donc décidé d’exclure de l’application de cet article de Loi les familles *«sans papiers»* et de leur refuser l’hébergement et la protection d’urgence.

Et comme le Ridicule ne tue pas : à une demande formulée, l’administration propose en guise d’hébergement *«une assignation à Résidence»* généralement mise en œuvre pour organiser les expulsions, avec signature à la Police tous les jours. Ridicule, du ridicule, l’administration nous demande de servir *«d’intermédiaire»* pour signifier à la famille la mésaventure qui l’attend… L’administration : c’est parfois un monde bizarre ! 

**Nouvelle Loi sur l’immigration ? non N ième loi sur l’immigration...**

Plus xénophobe que moi, tu meurs… du Sénat à l’Assemblée la surenchère aux relents d’extrême droite va bon train. Les protagonistes, vraisemblablement boustés par les réussites électorales en Europe de toute la gente raciste, se voient déjà en posture présidentiable. Nous tenterons de faire un point d’étape dans un terrain bourbeux… Mais nous en connaissons déjà les victimes… enfin celles qui sont passées à travers les barbelés érigés à toutes les frontières de l’Europe.

**Des mineurs/majeurs pas si «isolés».**

Nous continuons d’accompagner « les petits » à l’école. Nous prenons en charge 8 scolarités essentiellement en lycée professionnel avec internat. Pour l’instant tout le monde est casé mais nous devrons trouver des solutions dès Noël pour deux d’entre eux. Nous renouvelons l’appel aux hébergements (weekend et vacances scolaires).

**et des questions diverses :**

qui sont les vôtres.

Bien amicalement à toutes et tous,  Montoulieu le 28 novembre 2022

Christian
