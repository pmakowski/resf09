.. title: Compte rendu mars 2022
.. slug: compte-rendu-mars-2022
.. date: 2022-03-08 10:46:49 UTC+02:00
.. tags: compte-rendu 
.. category: compte-rendu
.. link: 
.. description:  RESF09 Mars 2022
.. type: text

Présidence : Yannick Garcia

Animation : Christian Morisse

**Cyber attaque**

Christian vient d'en faire les frais indirectement suite à l'attaque informatique qui a ciblé le satellite hébergeant son prestataire de services.
Résultat : des « trous dans la raquette » pour les invitations à notre réunion mensuelle et... une moindre participation.

**OQTF**

Six nouvelles sont tombées au cours de cette première semaine de mars. Bien entendu, le réseau a saisi les avocat(e)s et leur a fourni les éléments nécessaires pour tenter de casser ces décisions préfectorales.

**Ukraine**

Comment se préparer concrètement à l'arrivée de réfugié(e)s ? Même si on peut penser que l'Ariège sera éloignée des priorités géographiques exprimées par les familles concernées. De plus les autorités vont être directement saisies par le gouvernement tout comme les élus locaux.

Affaire à suivre... de près car on pourrait assister à une concurrence en matière d'hébergements.

**Fin de la période hivernale**

RESF va envoyer un courrier à la préfecture pour demander de proroger la date habituelle de la fin mars, ce qui permettrait de ne pas réduire la capacité d'hébergements actuelle, déjà limite.

**Evolution de nos interventions**

Nos actions sont essentiellement tournées vers l'aide administrative et juridique, ainsi que l'apprentissage de la langue (FLE). Or on constate très régulièrement à l'occasion des permanences de gros besoins d'information sur le fonctionnement de l'Etat (au sens large) aussi bien son organisation que le fonctionnement dans divers domaines : éducation, justice, santé...

Il faudra y réfléchir pour tâcher d'y répondre au mieux, peut-être en lien avec d'autres associations.


Alain Lacoste  secrétaire
