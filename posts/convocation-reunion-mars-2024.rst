.. title: Convocation réunion mars 2024
.. slug: convocation-reunion-mars-2024
.. date: 2024-02-27 10:53:37 UTC+02:00
.. tags: convocation
.. category:  convocation
.. link: 
.. description: RESF09 convocation réunion mars 2024
.. type: text

Bonjour, nous avons le plaisir de vous rappeler que le premier Lundi de chaque mois nous tenons une réunion  de RESF.

REUNION MENSUELLE du Réseau Education Sans Frontière

Le Lundi 4 Mars 2024  de 17h30 à 20H.

Maison des Assos de FOIX (petite salle de réunion)

**Mineurs isolés**

Les arrivées en hausse, les évaluations et l’hébergement. Nous envisageons la scolarisation de quelques nouveaux candidats. Nous attendons le résultat des évaluations pour faire des demandes au CIO. Reste à savoir si des places sont disponibles dans les établissements scolaires. Nous sommes toujours à la recherche de capacités de prise en charge pour les weekends et les vacances.

Nous reviendrons également sur les audiences chez la Juge des enfants et les 7 nouvelles demandes pour lesquelles nous avons été sollicités suite aux sorties/refus du Conseil Départemental.

**La coordination contre la Loi «Asile-Immigration»**

**Le communiqué commun** que nous vous avions transmis est passé « caviardé » dans la Dépêche, ne mentionnant pas la liste des signataires. La Gazette semble avoir préféré donner la parole au Préfet qui se réjouit de ses « réussites » en matière d’expulsion (qui restent à vérifier par ailleurs). On a la presse qu’on peut !

**Le planning de formation** proposé par la LDH se remplit – la première séance s’est tenue le 14 février à la Maison des associations de Foix sur le thème de l’Asile. Nous poursuivons donc ce cycle : ci-joints les docs pour s’inscrire au fil du calendrier, n’hésitez pas, il y aura de la place pour tout le monde même si nous devons dédoubler les séances.

**La conférence** envisagée par la coordination sur le thème **«l’immigration, une chance pour l’Europe»** prend forme. Elle aura lieu le Vendredi 19 Avril à 20h30  à la Salle Jean Jaurès – Mairie de Foix et sera présentée par Mme **Ekrame BOUBTANE** (Université Clermont / Auvergne)… retenez la date dès maintenant.

**La Journée internationale Des Droits des Femmes**

Plusieurs initiatives sont annoncées que nous vous avons transmises. Privilégiant notre expérience de terrain, nous avons retenu pour ce qui nous concerne le thème **«le quotidien des femmes dans les migrations»** sous forme de Conférence/débat le 8 mars à 20h30, la salle Jean Jaurès à FOIX animée par Yannick, Présidente de RESF.

**Questions diverses :** 

qui sont les vôtres.  

Bien amicalement à toutes et tous,   Montoulieu le 27 février 2024  
Christian   
