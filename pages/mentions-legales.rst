.. title: Mentions légales
.. slug: mentions-legales
.. date: 2020-03-15 16:38:51 UTC+01:00
.. tags: 
.. category: 
.. link: 
.. description: 
.. type: text

Le site resf.ariege.eu.org est édité par Réseau éducation sans frontière 09.

Association loi 1901 - déclaration en préfecture n° W091001962 (JO du 13/01/2015)

Siret 81769738600016

5 rue de Carrié - Seignaux 09000 Montoulieu

