.. title: Contact et permanences
.. slug: contact-et-permanences
.. date: 2020-03-15 17:00:55 UTC+01:00
.. tags: 
.. category: permanences
.. link: 
.. description: 
.. type: text

Réseau éducation sans frontière 09
5 rue de Carrié - Seignaux 09000 Montoulieu 

Téléphone : 06.70.94.08.48 

Courriel : christian.morisse@nordnet.fr 

Permanence : accueil et accompagnement juridique 

sur rendez-vous chaque vendredi matin de 8h30 à 12h. 

Maison des Associations - 7 Bis Rue Saint Vincent - 09100 Pamiers 
